<div class="main-col add">
    <div class="row"><h1 class="h2">Создать статью</h1></div>
    <div class="thumbs">
        <!--
        <form class="form" action="" method="post">
            <fieldset>
                <label for="title">Заголовок (от 10 символов)</label>
                <input type="text" name="title" id="title" class="field" placeholder="Заголовок">
            </fieldset>
            <fieldset class="file">
                <label for="image" class="button gray">Выберите файл</label>
                <input type="file" name="image" id="image">
            </fieldset>
            <fieldset>
                <label for="description">Описание (до 100 символов)</label>
                <textarea name="description" id="description" class="field" rows="5"></textarea>
            </fieldset>
            <fieldset>
                <label for="text">Содержание</label>
                <textarea name="text" id="text" class="field" rows="20"></textarea>
            </fieldset>
            <fieldset>
                <label for="category">Категория</label>
                <select name="category_id" id="category" class="field">
                    <option value="1">Новости</option>
                    <option value="2">Статьи</option>
                </select>
            </fieldset>
            <fieldset>
                <button type="submit" name="submit" value="submit" class="button">Опубликовать</button>
            </fieldset>
        </form>
        -->
        {include file="default/default_form.tpl" form=$form}
    </div>
    <div class="sidebar">
        <div class="thumb padding"><br></div>
        {include file="blocks/teaser.tpl"}
        {include file="blocks/counter.tpl"}
    </div>
    <div class="clearfix"></div>
</div>