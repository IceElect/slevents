<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Проверка</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style>
        body{
            font-size: 19px;
            font-family: sans-serif;
        }
        .button {
            background: #d2383b;
            padding: 7px 16px 8px;
            margin: 0;
            display: inline-block;
            zoom: 1;
            cursor: pointer;
            white-space: nowrap;
            outline: none;
            vertical-align: top;
            line-height: 19px;
            text-align: center;
            text-decoration: none;
            color: #fff;
            border: 0;
            border-radius: 2px;
            box-sizing: border-box;
        }
        .button:hover {
            background: #c03336;
        }

        table th,
        table td{
            padding: 3px 8px;
            text-align: left;
        }
    </style>
</head>
<body>
    <table>
        {foreach from=$ticket key=k item=value}
            <tr>
                <th>{$k}</th>
                <td>{$value}</td>
            </tr>
        {/foreach}
    </table>
    <hr>
    {if $oUser->group == 2}
        <a href="/ticket/success_go/{$ticket->hash}" class="button">Прошел, билет использован</a>
    {/if}
</body>
</html>