<div class="main-col">
	<div class="user-page">
		<div class="col-sidebar">
			<div class="avatar thumb big">
				<div>{get_avatar u=$u}</div>
				{*if $u->id == $oUser->id}
				<div class="buttons-group">
					<button class="md-icon" onclick="photo.upload_popup(photo.avatar_upload, {$u->avatar_album}, event)">camera_alt</button>
					<button class="md-icon" onclick="photo.crop_popup(photo.avatar_crop, {$u->avatar}, event)">photo_size_select_small</button>
				</div>
				{/if*}
			</div>
			<div class="actions">
				{if $u->id == $oUser->id}
				<a href="/users/logout" class="button full_width gray">Выход</a>
				{/if}
			</div>
		</div>
		<div class="col-content">
			<div class="user-page-header {if ($u->last_action >= ($time - 900))}online{/if} big">
				<div class="user-page-info thumb flr">
					<div class="row">
						<h1 href="#" class="name">{$u->fname} {$u->lname}</h1>
						<div class="spacer"></div>
						{*if ($u->last_action >= ($time - 900))}<span>В сети</span>{else}<span><abbr title="{$u->last_action|date_format:"%Y-%m-%d %H:%M:%S"}" class="time"></abbr></span>{/if*}
						<span>{$u->id}</span>
					</div>
					<div class="row user-actions">
						<div class="spacer"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
				<div class="thumb user-tabs">
			    	<ul>
			    		<li><a href="javascript:void(0)" class="user-tab user-tab-sel">Ваши билеты</a></li>
			    	</ul>
			    </div>
				<div class="users-list tickets-list">
					{foreach from=$tickets item=ticket}
					<li class="user-item thumb">
					    <a href="javascript:void(0)" class="user" data-type="load">
					        <span class="name">{if $ticket->type == 'personal'}Пропуск персонала{else}{$ticket->name}{/if}</span>
					    </a>
					    {*<div class="user-button">
		                	<button class="md-icon">more_horiz</button>
					    </div>*}
					</li>
					<li class="user-item thumb file-upload">
						<div class="ticket-info">
							<div><b>{if $ticket->type == 'personal'}{$ticket->event_name}{else}{$ticket->name} - {$ticket->price}р{/if}</b></div>
							<div>Дата: {$ticket->wd} {$ticket->day} {$ticket->month} {$ticket->year}</div>
							<div>{$ticket->location}</div>
							<hr>
							<div>{$ticket->description}</div>
						</div>
						<div class="qr">
			            	<img src="/{$ticket->qr}">
			            	<p>
			            		<b>{if $ticket->payment_status || $ticket->type == 'personal'}Оплачен{else}Не оплачен{/if}</b>
			            	</p>
			            </div>
					</li>
					{/foreach}
				</div>
			</div>
		</div>
	</div>
</div>