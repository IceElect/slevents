<div class="photos-choose-rows">
    {foreach from=$photos item=photo}
    <a class="photo-choose-row" onclick="return photo.photo({$photo->id}, 1, event);">
        <div class="photo-row-img" style="background-image: url('{$hosted_url}ajax/photo/view/{$photo->id}/145/photo.jpg');"></div>
    </a>
    {/foreach}
</div>