<div class="main-col posts">
    <div class="row">
        <h1 itemprop="headline" class="h2">{$pageTextTitle}</h1>
    </div>
    <div class="row">
        <ul class="categories">
            <li class="{if !isset($category_id)}selected{/if}"><a href="/">Все</a></li>
            {foreach from=$categories key=$i item=$category}
                <li class="{if isset($category_id)}{if $category->id == $category_id}selected{/if}{/if}">
                    <a href="/blog/{$category->id}">{$category->title}</a>
                </li>
            {/foreach}
        </ul>
    </div>
    <div class="thumbs" itemscope itemtype="http://schema.org/Blog">
        <meta itemprop="description" content="Современный блог о новостях в мире игр и IT">
        {foreach from=$posts key=$i item=$post}
            {include file="blog/post.tpl" post=$post}
        {/foreach}
    </div>
    <div class="sidebar">
        {include file="blocks/howto.tpl"}
        <div class="thumb padding">
            <h3>Горячие новости</h3>
            {foreach from=$last_news item=$new}
                <div class="line_new">
                    <a href="/post/{$new->id}-{$new->url}" class="image-holder">
                        <img src="{$new->image}" alt="{$new->title}">
                    </a>
                    <div class="content">
                        <a href="/post/{$new->id}-{$new->url}" title="{$new->title}"><h3 class="h4">{$new->title}</h3></a>
                    </div>
                </div>
            {/foreach}
        </div>
        {include file="blocks/teaser.tpl"}
        {include file="blocks/counter.tpl"}
    </div>
    <div class="clearfix"></div>
</div>