<div class="comment">
    <div class="avatar middle">
        {get_avatar u_id=$comment->author_id u_av=$comment->avatar}
    </div>
    <div class="content">
        <div class="info">
            <a href="#" class="name">{$comment->fname} {$comment->lname}</a>
            <span><abbr title="{$comment->date|date_format:"%Y-%m-%d %H:%I:%S"}" class="time"></abbr></span>
            <span class="spacer"></span>
            <div class="actions">
                <button class="icon fa icon-dot-3"></button>
            </div>
        </div>
        <div class="text">
            {$comment->text}
        </div>
    </div>
</div>