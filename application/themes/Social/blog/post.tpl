<article itemprop="blogPosts" itemscope itemtype="http://schema.org/BlogPosting">
    <div class="thumb post">
        <abbr title='{$post->date|date_format:"%Y-%m-%d %H:%M:%S"}' class="time"></abbr>
        <a href="/post/{$post->id}-{$post->url}" class="image-holder">
            <img itemprop="image" src="http://news.slto.ru{$post->image}" alt="{$post->title|escape}" title="{$post->description|escape}">
            <div class="clearfix"></div>
        </a>
        <div class="content-holder">
            <a href="/post/{$post->id}-{$post->url}" class="title"><h2 class="h3" itemprop="name">{$post->title}</h2></a>
            <p class="description" itemprop="description">{$post->description}</p>

            <div class="info">
                <span><i class="fa fa-eye"></i> {$post->views_count}</span>
                <span class="dot"></span>
                <span><i class="fa fa-comment"></i> {$post->comments_count}</span>
            </div>
        </div>
    </div>
</article>