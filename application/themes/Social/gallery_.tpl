<div class="main-col">
    <h1 class="h2">Галерея</h1>
    <div class="sidebar">
        <div class="thumb tabs" style="margin-top: 0px">
            {foreach from=$events item=event}
            <div role="tab" id="event-{$event->id}" data-gallery="{$event->album_id}" href="#event-{$event->id}" class="body-nest tab">{$event->name}</div>
            {/foreach}
        </div>
    </div>
    <div class="content">
        <div class="thumbs">
            <div class="thumb padding" id="gallery-wrap" style="margin-top: 0px">
                
            </div>
        </div>
    </div>
</div>
<script>
    {literal}
    $(document).on("click", "[data-gallery]", function(e){
        e.preventDefault();

        $("[data-gallery]").removeClass('current');
        $(this).addClass('current');

        var gallery = $(this).attr('data-gallery');
        $.post("/welcome/album/", {id: gallery}, function(data){
            var data = eval('('+data+')');

            if(data.response){
                $("#gallery-wrap").html(data.data);
            }
        })
    })

    $(".tabs .tab:first-child").click();
    {/literal}
</script>