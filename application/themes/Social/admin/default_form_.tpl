{strip}
    {assign var="required_star" value="<span class='required'>*</span>"}
    {assign var="i" value='0'}
    {nocache}
        <div class="content-wrap">
            {include file="sys/messages.tpl"}
            <form {foreach from=$form_attributes key=var item=value}{$var}="{$value}"{/foreach}>
                {csrf}
                <div class="col-lg-9 col-md-7">
                    <div class="nest">
                        <div class="title-alt">
                            <h6>{$pageTextTitle}</h6>
                            <div class="titleClose">
                                <a class="gone" href="#elementClose">
                                    <span class="entypo-cancel"></span>
                                </a>
                            </div>
                            <div class="titleToggle">
                                <a class="nav-toggle-alt" href="#element">
                                    <span class="entypo-up-open"></span>
                                </a>
                            </div>
                        </div>
                        <div class="body-nest">
                            {if $action ne 'add'}
                                <input id="item_id" type="hidden" name="id" value="{$nId}"/>
                            {else}
                                <input type="hidden" name="change_pwd" value="1"/>
                            {/if}
                            <!--
                            {if $action ne 'add'}
                                <div class="form-group">
                                    <div class="col-lg-2 col-sm-2 control-label">
                                        ID :
                                    </div>
                                    <div class="col-sm-6 infofield">
                                        <p class="form-control-static">{$nId}</p>
                                    </div>
                                </div>
                            {/if}
                            -->
                            {$num = 0}
                            {foreach from=$aFields key="num" item="fields" }
                                {if (!isset($tabs.$num))}{continue}{/if}
                                <section class="content tab-content {if $num == 0}current{/if}" id="{$tabs.$num.id}">
                                    <div class="form-group">
                                        <div class="col-lg-3 col-sm-3 control-label"></div>
                                        <div class="col-sm-6 infofield">
                                            <p class="form-control-static">{$required_star} {translate code="form_required_fields" text="Обязательные поля"}</p>
                                        </div>
                                    </div>
                                    {foreach from=$fields key="field_key" item="field" }
                                        {$i = $i + 1}
                                        {if !empty($field.custom_field_template)}
                                            {assign var=path value=$field.custom_field_template}
                                            {include file="custom/$path.tpl" field=$field aData=$aData i=$i}
                                        {else}
                                            {include file='admin/default_form_row.tpl' field=$field aData=$aData i=$i}
                                        {/if}
                                    {/foreach}
                                    {if isset($hooks.{$tabs.$num.id})}
                                        {foreach from=$hooks.{$tabs.$num.id} key=key item=hook}
                                            {include file="default/{$hook}.tpl"}
                                        {/foreach}
                                    {/if}
                                </section>
                            {/foreach}
                            {if $tabs}
                                </div>
                            {/if}
                    </div>

                    {if isset($hooks.after_form)}
                        {foreach from=$hooks.after_form key=key item=hook}
                            {include file="default/{$hook}.tpl" aFields=$aFields aData=$aData}
                        {/foreach}
                    {/if}
                </div>

                <div class="col-lg-3 col-md-5">
                    {if $tabs}
                        <div class="nest tabs" style="margin-top: 30px">
                            {foreach from=$tabs key=i item=tab}
                                <div role="tab" id="{$tab.id}" href="#{$tab.id}" aria-controls="{$tab.id}" class="body-nest tab {if $i == 0}current{/if}">{$tab.title}</div>
                            {/foreach}
                        </div>
                    {/if}
                    
                    <div class="nest">
                        <div class="title-alt">
                            <h6>Действия</h6>
                            <div class="titleClose">
                                <a class="gone" href="#elementClose">
                                    <span class="entypo-cancel"></span>
                                </a>
                            </div>
                            <div class="titleToggle">
                                <a class="nav-toggle-alt" href="#element">
                                    <span class="entypo-up-open"></span>
                                </a>
                            </div>
                        </div>
                        <div class="body-nest">
                            <section class="col-xs-12">
                                {if isset($aFields.sidebar)}
                                    {foreach from=$aFields.sidebar key="field_key" item="field" }
                                        {$i = $i + 1}
                                        {include file='admin/default_form_row.tpl' field=$field aData=$aData i=$i}
                                    {/foreach}
                                {/if}
                                <div class="row box-btn form-actions btn-group pull-left">
                                    <div class="col-sm-12">
                                        
                                        <button type="submit" name="submit" value="1" class="btn btn-success">
                                            <i class="fa fa-floppy-o" style="margin-right: 0"></i>{*<span>{translate code="form_save" text="Сохранить"}</span>*}
                                        </button>
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a><button name="submit_exit" value="1" type="submit">{translate code="form_save_exit" text="Сохранить и выйти"}</button></a></li>
                                            <li><a><button name="submit_next" value="1" type="submit">{translate code="form_save_next" text="Сохранить и далее"}</button></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row box-btn form-actions btn-group pull-right">
                                    <a class="btn btn-danger" href="{$aConf.base_url}{$aConf.active_module}">
                                        <span>{translate code="form_cancel" text="Отмена"}</span>
                                    </a>    
                                </div>
                            </section>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    {if isset($hooks.after_sidebar)}
                        {foreach from=$hooks.after_sidebar key=key item=hook}
                            {include file="default/{$hook}.tpl"}
                        {/foreach}
                    {/if}
                </div>
            </form>
        </div>
    {/nocache}
    <script type="text/javascript">
        {literal}

        function fields_remove_image(el, id, field_name) {
            $.ajax({
                type: 'POST',
                url: '{/literal}{$aConf.base_url}{$aConf.active_module}/remove_file/{literal}' + id + '/' + field_name,
                dataType: 'json',
                success: function (data) {
                    $(el).parent('.image_holder').hide();
                },
                data: {'ajax': 1},
                async: false
            });
        }
        $(document).on('click', '.tabs .tab', function(e){
            e.preventDefault();

            var id = $(this).attr('aria-controls'),
                tabs = $(this).parents('.tabs');
            
            tabs.find('.current').removeClass('current');
            $(this).addClass('current');

            $(".content.tab-content.current").removeClass('current');
            $(".content.tab-content#"+id).addClass('current');
        })
        {/literal}
    </script>
{/strip}