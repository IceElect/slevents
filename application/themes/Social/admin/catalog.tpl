<div class="content-wrap">
    <div class="main-col">
        <div class="admin-category-list">
        {foreach from=$categories key=i item=category}
            <div class="thumb admin-category-tile">
                <div class="thumb-title">
                    <span class="thumb-title-inner">{$category->title}</span>
                    <div class="spacer"></div>
                    <div class="actions">
                        <a href="{$aConf.base_url}admin/catalog{$editAction}/{$category->id}" class="icon with-bg md-icon">create</a>
                        <a href="{$aConf.base_url}admin/catalog{$delAction}/{$category->id}" class="icon with-bg md-icon">clear</a>
                    </div>
                </div>
                <a href="{$aConf.base_url}admin/catalog/{$category->name}" class="admin-category-cover">
                    {assign var="cover" value="{$project_path}uploads/category/{$category->cover}"}
                    {if file_exists($cover) && !empty($category->cover)}
                        <img src="{$aConf.base_url}uploads/category/{$category->cover}" alt="Нет фото" width="100%">
                    {else}
                        <img src="https://cms.skeeks.com/img/no-image.png" alt="Нет фото" width="100%">
                    {/if}
                </a>
            </div>
        {/foreach}
        </div>

        <div class="col-md-12">
            {include file="admin/default_grid.tpl" aFields=$aFields aData=$aData pagination=$pagination get_table_only=true}
        </div>
    </div>
</div>