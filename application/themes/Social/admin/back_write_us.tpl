﻿{include file="back_header.tpl"}
{strip}
<div id="wraper" class="wraper">
    <div class="box" style="width:80%;">
        <div style="text-align:center;padding:10px 0 10px 0;">
            <span class="caption">Язык: </span>
            <select name="coupon_category_id" id="coupon_category_id" onchange="getLangList(this.value)">
                <option value="">Все</option>
                {foreach from=$language_list key="lang_key" item="lang" }
                    <option value="{$lang->id}">{$lang->code}</option>
                {/foreach}
            </select>
        </div>
    </div>
    <div class="box" style="width:600px;">
        <table id="list" class="scroll" cellpadding="0" cellspacing="0"></table>
        <div id="pager" class="scroll"></div>

    </div>

    <script type="text/javascript">
        {literal}
        jQuery().ready(function () {

            var gridimgpath = '{/literal}{$aConf.base_url}themes/images/backend/{literal}';

            var setEditFrm = function () {
            }

            function customFormatImg(cellvalue, options, rowObject) {
                var img = '<div class="flags my-icon sprite-' + options.rowId.toUpperCase() + '">&nbsp;</div>';
                return img;
            }

            jQuery("#list").jqGrid({
                url: '{/literal}{$aConf.base_url}{$active_module}{literal}/getlist/',
                datatype: "json",
                colNames: [
                    'ID',
                    'Дата',
                    'Текст',
                    'Язык'
                ],
                colModel: [
                    {
                        name: 'ID',
                        index: 'id',
                        width: 30,
                        align: "center",
                        sorttype: 'int',
                        editable: false,
                        editoptions: {size: 30},
                        formoptions: {elmprefix: "required_star"},
                        editrules: {required: true}
                    },
                    {
                        name: 'Дата',
                        index: 'date',
                        width: 60,
                        sorttype: 'text',
                        editable: true,
                        editoptions: {size: 100},
                        formoptions: {elmprefix: "required_star"},
                        editrules: {required: true}
                    },
                    {
                        name: 'Текст',
                        index: 'text',
                        width: 100,
                        sorttype: 'text',
                        editable: true,
                        editoptions: {size: 100},
                        formoptions: {elmprefix: "required_star"},
                        editrules: {required: true}
                    },
                    {
                        name: 'Язык',
                        index: 'lang_id',
                        width: 30,
                        align: "center",
                        sorttype: 'int',
                        editable: false,
                        editoptions: {size: 30},
                        formoptions: {elmprefix: "required_star"},
                        editrules: {required: true}
                    },
                ],
                rowNum: 20,
                autowidth: true,
                mtype: "GET",
                rowList: [20, 30, 40],
                imgpath: gridimgpath,
                pager: '#pager',
                viewrecords: true,
                multiselect: true,
                rownumbers: true,
                editurl: "{/literal}{$aConf.base_url}{$active_module}{literal}/edit/",
                sortname: 'id',
                sortorder: "asc",
                ondblClickRow: function (id) {
                    editRow(id);
                },
                caption: '{/literal}{$module_title}{literal}',
                height: 200,
                loadui: "block"

            }).navGrid('#pager', {view: true, edit: false, add: false, search: false, del: true, refresh: true},
                    {
                        jqModal: false,
                        checkOnUpdate: false,
                        savekey: [true, 13],
                        navkeys: [true, 38, 40],
                        width: '400',
                        checkOnSubmit: false,
                        reloadAfterSubmit: true,
                        closeOnEscape: true,
                        bottominfo: "required_str",
                        closeAfterEdit: true,
                        beforeInitData: setEditFrm
                    }, // edit button parameters
                    {
                        jqModal: true,
                        checkOnUpdate: false,
                        savekey: [true, 13],
                        navkeys: [true, 38, 40],
                        width: '400',
                        checkOnSubmit: false,
                        reloadAfterSubmit: true,
                        closeOnEscape: true,
                        bottominfo: "required_str",
                        closeAfterAdd: true,
                        beforeInitData: setEditFrm
                    }, // add button parameters
                    {
                        url: '{/literal}{$aConf.base_url}{$active_module}{literal}/del/',
                        reloadAfterSubmit: true,
                        jqModal: false,
                        closeOnEscape: true
                    }, // del button parameters
                    {multipleSearch: false, closeOnEscape: true}, // enable the advanced searching
                    {
                        navkeys: [true, 38, 40],
                        height: 250,
                        jqModal: false,
                        closeOnEscape: true
                    } /* allow the view dialog to be closed when user press ESC key*/
            ).navButtonAdd("#pager", {
                        caption: "", title: "edit_admin", buttonicon: 'ui-icon-pencil', position: 'first',
                        onClickButton: editRow
                    })

                /*
                 .navButtonAdd("#pager",{caption:"",title:"Поиск",buttonicon:'ui-icon-search',position:'first',
                 onClickButton:function(){
                 if(jQuery("#filter").css("display")=="none") {
                 jQuery("#filter").show();
                 $('#sButton, #cButton').hover(
                 function() { $(this).addClass('ui-state-hover'); },
                 function() { $(this).removeClass('ui-state-hover'); }
                 );
                 }
                 }
                 })*/;
            /*
             jQuery("#filter").jqGrid('filterGrid',"list",
             {
             gridModel:false,
             filterModel:[
             {label:'123', name: 'id', stype: 'text', defval: ''},
             {label:'222', name: 'name', stype: 'text', defval: ''},
             {label:'333', name: 'sef_url', stype: 'text', defval: ''},
             ],
             gridNames:true,
             formtype:"vertical",
             enableSearch:true,
             enableClear:true,
             autosearch: false,
             buttonclass:'ui-state-default ui-corner-all typebtn',
             tableclass:'ui-accordion-content-active filtertbl'
             }
             );
             */
            $(".btn_close").click(function () {
                jQuery("#filter").hide();
                return false;
            });

            function editRow(id) {
                if (!id || typeof id == 'object') {
                    var id = jQuery("#list").getGridParam('selrow');
                }
                if (id) {
                    var sURL = '{/literal}{$aConf.base_url}{$active_module}{literal}/edit/' + id + '?action=edit&width=965&height=490&jqmRefresh=true';
                    var sTitle = 'Изменение данных - ' + id;
                    $('#anchorModalWindow').attr('href', sURL);
                    $('#anchorModalWindow').attr('title', sTitle);
                    $('#anchorModalWindow').click();
                } else {
                    alert("Сначала выберите меню");
                }
            }

            /*
             function addRow(id){
             var sURL = '
            {/literal}{$aConf.base_url}{$active_module}{literal}/edit/?action=add&width=965&height=490&jqmRefresh=true';
             var sTitle = 'Добавление меню';
             $('#anchorModalWindow').attr('href', sURL);
             $('#anchorModalWindow').attr('title', sTitle);
             $('#anchorModalWindow').click();
             }
             */

            jQuery("#list").jqGrid('gridResize');

            $(function () {
                $("form input").keypress(function (e) {
                    if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                        $('#sButton').click();
                        return false;
                    } else {
                        return true;
                    }
                });
            });

        }); // end rdocument.eady

        function getLangList(lang_id) {
            jQuery("#list").jqGrid('setGridParam', {
                url: "{/literal}{$aConf.base_url}{$active_module}{literal}/getlist/?search=1&lang_id=" + lang_id,
                page: 1
            }).trigger("reloadGrid");
        }

        {/literal}
    </script>
    {/strip}
{include "back_footer.tpl"}