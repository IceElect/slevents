{strip}
    <div id="dialog-confirm" title="{translate code="dialog_delete_title" text="Удаление"}">
        <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>

        <div style="padding-left:20px;">
            {translate code="dialog_delete_menu_link_confirm" text="Удалить ссылку со всеми под ссылками из этого меню ?"}
        </div>
    </div>
    <div class="menu_add_link_block">
        <label>{translate code="add_link" text="Добавить ссылку"}</label>
        <select class="new_link margin-right-6">
            {nocache}
                {foreach $link_list as $link}
                    <option value="{$link.id}">{if (empty($link.tr_text))}{$link.name}{else}{$link.tr_text}{/if}</option>
                {/foreach}
            {/nocache}
        </select>
        <a class="add_link_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" href="#">
            <span class="ui-button-text">
                {translate code="form_add" text="Добавить"}
            </span>
        </a>
    </div>
    <div>
        {nocache}
        <ul class="sortable ui-sortable">
            {$deep = 0}
            {foreach $menu_links as $key => $link}
                {if ($deep > $link->deep)}
                    {section name=close_ul loop=($deep-$link->deep)}
                            </li>
                        </ul>
                    {/section}
                    {$deep = $link->deep}
                {/if}
                {if ($deep == $link->deep && $key != 0)}
                    </li>
                {/if}
                {if ($deep < $link->deep)}
                        {$deep = $link->deep}
                    <ul>
                {/if}

                <li data-linkid="l_{$link->id}" class="ui-state-default ui-sortable-handle">
                <div>
                    <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                    {$link->name}
                    <a href="javascript:void(0)" onclick="deleteLink(this)"
                       class="fright ui-state-hover buttons-edit">{translate code="form_delete" text="Удалить"}</a>
                </div>
            {/foreach}
        {section name=close_ul loop=($deep+1)}
                </li>
            </ul>
        {/section}
    </div>
    <div class="row box-btn form_actions">  
        <button type="submit" name="submit" value="1" class="btn btn_save ui-widget ui-state-default">
            <i class="fa fa-floppy-o"></i>{translate code="form_save" text="Сохранить"}
        </button>
        <a class="btn btn_cancel" href="{$aConf.base_url}{$aConf.active_module}">
            <i class="fa fa-times"></i>{translate code="form_cancel" text="Отмена"}
        </a>
    </div>
    {/nocache}
    <script type="text/javascript">
        {literal}
        function hide_messages(){}
        $(function () {
            $('.sortable').nestedSortable({
                handle: 'div',
                items: 'li',
                toleranceElement: '> div',
                listType: 'ul',
                cursor: 'move',
                attribute: 'data-linkid'
            });
            $('.btn_save').click(function () {
                hide_messages();
                var menu_order = $('ul.sortable').nestedSortable('toArray');
                $.ajax({
                    type: 'POST',
                    url: '{/literal}{$aConf.base_url}{$aConf.active_module}{literal}/edit_menu_order/{/literal}{$menu_id}{literal}',
                    dataType: 'json',
                    success: function (data) {
                        if (data.success) {
                            show_message('{/literal}{translate code="form_msg_menu_order_save" text="Меню сохранено"}{literal}', 'success');
                        }
                    },
                    data: {'ajax': 1, 'menu_order': menu_order},
                    async: false
                });
            });

            /*
             arraied = $('.sortable').nestedSortable('toArray');
             */
            $('select.new_link').chosen();
            $('.add_link_button').click(addLink);

            function addLink(e) {
                e.preventDefault();
                hide_messages();
                var arraied = $('.sortable').nestedSortable('toArray');
                var checked = true;
                var link_id = $('.new_link').val();
                if (arraied.length) {
                    for (i = 0; i < arraied.length; i++) {
                        if (arraied[i].item_id == link_id) {
                            checked = false;
                            break;
                        }
                    }
                }
                if (checked) {
                    var text = $('.new_link :selected').html();
                    var html = '<li class="ui-state-default" data-linkid="l_' + link_id + '">';
                    html = html + '<div class="ui-sortable-handle">';
                    html = html + '<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>' + text;
                    html = html + '<a class="fright ui-state-hover buttons-edit" onclick="deleteLink(this)" href="javascript:void(0)">Удалить</a></div></li>';
                    $('.ui-sortable').prepend(html);
                } else {
                    show_message('{/literal}{translate code="form_msg_error_link_exist" text="Ссылка уже есть в этим меню"}{literal}', 'error');
                }
            }
        });


        function deleteLink(elem) {
            $("#dialog-confirm").dialog({
                resizable: false,
                modal: true,
                buttons: {
                    "{/literal}{translate code="form_delete" text="Удалить"}{literal}": function () {
                        $(elem).parent().parent().remove();
                        $(this).dialog("close");
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }

        {/literal}
    </script>
{/strip}