{if (!isset($field.form_disable))}
    {$i = $i +1}
    {if ($field.type ne 'id') && ($field.type ne 'no_edit')}
        <div class="form-group">
            <div class="col-lg-3 col-sm-3 control-label">
                <label for="def_{$i}">
                    {if (strpos($field.rules,'required')!== false)}{$required_star}{/if} {$field.title}
                    :
                </label>
            </div>
            <div class="col-sm-6 field_wrap_{$field.type}">
                {if ($field.type == 'select') }
                    <select class="form-control"
                            name="{$field_key}"
                            id="def_{$i}" {if (!empty($field.readonly)) }{$readonly_attr}{/if} {if (!empty($field.disabled)) }{$disabled_attr}{/if} >
                        {if (!empty($field.options))}
                            {foreach from=$field.options key="option_key" item="option" }
                                <option value="{$option_key}"  {if (!empty($aData.$field_key) && $aData.$field_key == $option_key) } selected {/if} >{$option}</option>
                            {/foreach}
                        {/if}
                    </select>
                {/if}
                {if ($field.type == 'textarea') }
                    <textarea rows="4" {if (!empty($field.readonly))}{$readonly_attr} class="readonly"{/if} {if (!empty($field.disabled))}{$disabled_attr}{/if}  name="{$field_key}" id="def_{$i}" />{if isset($aData.$field_key) }{$aData.$field_key}{else}{$field.value}{/if}</textarea>
                {/if}
                {if ($field.type == 'text') }
                    {if (!empty($field.prefix) || !empty($field.suffix))}
                        <div class="input-group">
                    {/if}
                    {if (!empty($field.prefix))}
                          <span class="input-group-addon" id="basic-addon1">{$field.prefix}</span>
                    {/if}
                        <input type="text" {if (!empty($field.readonly))}{$readonly_attr}{/if} {if (!empty($field.disabled))}{$disabled_attr}{/if}
                               name="{$field_key}" id="def_{$i}"
                               class="form-control text {if (!empty($field.readonly))}readonly{/if}"
                               value="{if isset($aData.$field_key) }{$aData.$field_key}{/if}"/>
                    {if (!empty($field.suffix))}
                          <span class="input-group-addon" id="basic-addon1">{$field.suffix}</span>
                    {/if}
                    {if (!empty($field.prefix) || !empty($field.suffix))}
                        </div>
                    {/if}
                    {if (!empty($field.description))}
                    <p class="form-text text-muted">
                        {$field.description}
                    </p>
                    {/if}
                {/if}
                {if ($field.type == 'checkbox') }
                    <input type="checkbox" {if (!empty($field.readonly))}{$readonly_attr}{/if} {if (!empty($field.disabled))}{$disabled_attr}{/if}
                           name="{$field_key}" id="def_{$i}"
                           class="form-check checkbox {if (!empty($field.readonly))}readonly{/if}"
                           value="1" {if !empty($aData.$field_key) } checked="checked" {/if}
                           autocomplete="off"/>
                    <label for="def_{$i}">&nbsp</label>
                {/if}
                {if ($field.type == 'image') }
                    {if (empty($field.readonly) && empty($field.disabled)) }
                        <input type="file" name="{$field_key}" id="def_{$i}" class="form-control-file image"/>
                    {/if}
                    <div class="image_holder">
                        {if (!empty($aData.$field_key)) }
                            <img class="image_preview"
                                 src="/{$field.image_src}{$aData.$field_key}">
                            <button value="" type="button" class="btn_cancel image_remove"
                                    onclick="fields_remove_image(this,{$nId},'{$field_key}')">
                                {translate code="form_delete" text="Удалить"}
                            </button>
                        {/if}
                    </div>
                {/if}
                {if ($field.type == 'static') }
                    <p class="form-control-static">{if isset($aData.$field_key) }{$aData.$field_key}{else}{$field.value}{/if}</p>
                {/if}
                {if ($field.type == 'wysiwyg') }
                    <textarea id="wysiwyg_{$field_key}" id="def_{$i}"
                              name="{$field_key}">{if isset($aData.$field_key) }{$aData.$field_key}{else}{$field.value}{/if}</textarea>
                {literal}
                    <script type="text/javascript">
                    /*
                        tinymce.init({
                            selector: "#wysiwyg_{/literal}{$field_key}{literal}"
                        });*/
                        var ckeditor_{/literal}{$field_key}{literal} = CKEDITOR.replace('wysiwyg_{/literal}{$field_key}{literal}');
                        //CKFinder.setupCKEditor( editor );
                        //editor.setData( '<p>Just click the <b>Image</b> or <b>Link</b> button, and then <b>&quot;Browse Server&quot;</b>.</p>' );
                        CKFinder.setupCKEditor(ckeditor_{/literal}{$field_key}{literal}, '/ckfinder/');
                    </script>
                {/literal}
                {/if}
            </div>
        </div>
    {/if}
{/if}