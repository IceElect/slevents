{strip}
    <div class="jqgrid_wrapper">
        <table id="list" class="scroll" cellpadding="0" cellspacing="0"></table>
        <div id="pager" class="scroll"></div>


        <script type="text/javascript">
            {literal}
            jQuery().ready(function () {

                var gridimgpath = '{/literal}{$aConf.base_url}themes/images/backend/{literal}';


                var setEditFrm = function () {
                }

                function customFormatImg(cellvalue, options, rowObject) {
                    var img = '<div class="flags my-icon sprite-' + options.rowId.toUpperCase() + '">&nbsp;</div>';
                    return img;
                }

                jQuery("#list").jqGrid({
                    url: '{/literal}{$aConf.base_url}{$aConf.active_module}/{$viewAction}{literal}/',
                    datatype: "json",
                    colNames: [
                        {/literal}
                        {foreach from=$aFields key="field_key" item="field" }
                            {if ($field.table_show)}
                                {if (!$field@first)}, {/if}'{$field.title}'
                            {/if}
                        {/foreach}
                        {literal}
                    ],
                    colModel: [
                        {/literal}
                        {foreach  from=$aFields key="field_key" item="field" }
                            {if ($field.table_show)}
                                {if (!$field@first)}, {/if}
                                {ldelim}name: '{$field.title}',
                                index: '{$field.field}'
                                {if (!empty($field.table_width))}, width:{$field.table_width}{/if}
                                {if (!empty($field.table_align))}, align: "{$field.table_align}" {/if}
                                {if (!empty($field.sorttype))}, sorttype: "{$field.sorttype}" {/if}
                                {if (!empty($field.sortable))}, sortable: {$field.sortable} {/if}
                                {rdelim}
                            {/if}
                        {/foreach}
                        {literal}

                    ],
                    
                    hidegrid: false,
                    rowNum: 50,
                    autowidth: true,
                    mtype: "GET",
                    rowList: [50, 100, 200],
                    imgpath: gridimgpath,
                    pager: '#pager',
                    viewrecords: true,
                    multiselect: true,
                    rownumbers: true,
                    editurl: "{/literal}{$aConf.base_url}{$aConf.active_module}/{$editAction}{literal}/",
                    sortname: 'id',
                    sortorder: "asc",
                    sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge', 'bw', 'bn', 'in', 'ni', 'ew', 'en', 'cn', 'nc', 'nu', 'in', 'ni'],
                    ondblClickRow: function (id) {
                        editRow(id);
                    },
                    caption: '{/literal}{$moduleTitle}{literal}',
                    height: 400,
                    loadui: "block"

                }).navGrid('#pager', {
                            view: true,
                            edit: false,
                            add: false,
                            search: true,
                            del: {/literal}{if ({permission action='delete'})}true{else}false{/if}{literal},
                            refresh: true
                        },
                        {
                            jqModal: false,
                            checkOnUpdate: false,
                            savekey: [true, 13],
                            navkeys: [true, 38, 40],
                            width: '400',
                            checkOnSubmit: false,
                            reloadAfterSubmit: true,
                            closeOnEscape: true,
                            bottominfo: "required_str",
                            closeAfterEdit: true,
                            beforeInitData: setEditFrm
                        }, // edit button parameters
                        {
                            jqModal: true,
                            checkOnUpdate: false,
                            savekey: [true, 13],
                            navkeys: [true, 38, 40],
                            width: '400',
                            checkOnSubmit: false,
                            reloadAfterSubmit: true,
                            closeOnEscape: true,
                            bottominfo: "required_str",
                            closeAfterAdd: true,
                            beforeInitData: setEditFrm
                        }, // add button parameters
                        {
                            url: '{/literal}{$aConf.base_url}{$aConf.active_module}/{$deleteAction}{literal}/',
                            reloadAfterSubmit: true,
                            jqModal: false,
                            closeOnEscape: true
                        }, // del button parameters
                        {multipleSearch: true, closeOnEscape: true}, // enable the advanced searching
                        {
                            navkeys: [true, 38, 40],
                            height: 250,
                            jqModal: false,
                            closeOnEscape: true
                        } /* allow the view dialog to be closed when user press ESC key*/
                ){/literal}{if ({permission action='edit'})}{literal}.navButtonAdd("#pager", {
                            caption: "",
                            title: "{/literal}{translate text="Редактировать" code="jqgrid_edit"}{literal}",
                            buttonicon: 'ui-icon-pencil',
                            position: 'first',
                            onClickButton: editRow
                        }){/literal}{if (!isset($disable_add))}{literal}
                        .navButtonAdd("#pager", {
                            caption: "",
                            title: "{/literal}{translate text="Добавить" code="jqgrid_add"}{literal}",
                            buttonicon: 'ui-icon-plus',
                            position: 'first',
                            onClickButton: addRow
                        })
                {/literal}
                {/if}
                {/if}
                {literal}

                $(".btn_close").click(function () {
                    jQuery("#filter").hide();
                    return false;
                });

                function editRow(id) {
                    if (!id || typeof id == 'object') {
                        var id = jQuery("#list").getGridParam('selrow');
                    }
                    if (id) {
                        edited_id = id;
                        var url = {/literal}'{eval var=$editLink}';
                        {if (empty($use_one_page)) }
                        window.open(url, '_blank');
                        {else}
                        document.location = url;
                        {/if}
                        {literal}
                    } else {
                        alert("{/literal}{translate text="Такой id не существует" code="jqgrid_error_chose"}{literal}");
                    }
                }


                function addRow(id) {
                    var url = {/literal}'{$aConf.base_url}{$aConf.active_module}/{$editAction}/';
                    {if (empty($use_one_page)) }
                    window.open(url, '_blank');
                    {else}
                    document.location = url;
                    {/if}
                    {literal}
                }

                jQuery("#list").jqGrid('gridResize');

                $(function () {
                    $("form input").keypress(function (e) {
                        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                            $('#sButton').click();
                            return false;
                        } else {
                            return true;
                        }
                    });
                });

            }); // end document.ready


            {/literal}
        </script>
    </div>
{/strip}