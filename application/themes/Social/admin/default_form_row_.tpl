{strip}
{if isset($field.additional_field)}
    {assign var=v value='custom_'|cat:$field.additional_field}
    {if isset($aData.$v)}
        {$field.value = $aData.$v}
    {/if}
{/if}
{if (!$field.form_disable)}
    {if $field.type == 'hidden'}
        <input type="{$field.type}" name="{$field.field}" value="{$field.value}">
    {else}
        <div class="form-group {if $field.error}has-error{/if}">
            <div class="{if (!isset($field.full_size))}col-lg-3 col-sm-3{else}col-sm-12{/if} control-label">
                <label for="def_{$i}">
                    {if (strpos($field.rules,'required')!== false)}{$required_star}{/if} 
                    {$field.title}
                </label>
            </div>
            <div class="{if (!isset($field.full_size))}{if $field.tab !== 'sidebar'}col-sm-9{else}col-sm-9{/if}{else}col-sm-12{/if} field_wrap_{$field.type}">
            	{if (!empty($field.prefix) || !empty($field.suffix))}
                    <div class="input-group">
                {/if}
                {if (!empty($field.prefix))}
                      <span class="input-group-addon" id="basic-addon1">{$field.prefix}</span>
                {/if}
                {/strip}
        		<{$field.tag_name}
        			id="def_{$i}"
        			{foreach from=$field.attributes key=key item=value}
                        {if $key !== 'value'}
        				    {if $value !== false}{$key}="{$value}"{/if}
                        {else}
                            {$key}="{if isset($aData.$field_key)}{$aData.$field_key}{else}{$field.value}{/if}"
                        {/if}
        			{/foreach}
                    {if $field.type == 'checkbox' && $field.value == '1'}checked{/if}
        		>{strip}
                
                {if $field.field == 'id'}
                        {$nId}
                    {elseif $field.type == 'select'}
                        {if (!empty($field.options))}
                            {foreach from=$field.options key="option_key" item="option" }
                                <option value="{$option_key}" {if !empty($aData.$field_key)}
                                                                {if $aData.$field_key == $option_key} selected {/if}
                                                              {else}
                                                                {if $field.value == $option_key} selected {/if}
                                                              {/if}>{$option}</option>
                            {/foreach}
                        {/if}
                    {elseif $field.type == 'radio'}
                        {if (!empty($field.options))}
                            {foreach from=$field.options key="option_key" item="option" }
                                <input type="radio" name="{$field.field}" value="{$option_key}" id="{$field.field}_{$option_key}" {if !empty($aData.$field_key)}
                                                                                                            {if $aData.$field_key == $option_key} checked {/if}
                                                                                                        {else}
                                                                                                            {if $field.value == $option_key} checked {/if}
                                                                                                        {/if}>
                                <label for="{$field.field}_{$option_key}">{$option}</label>
                            {/foreach}
                        {/if}
                    {elseif $field.type == 'checkbox'}
                        <label for="def_{$i}">&nbsp;</label>
                    {else}
                        {if $field.tag_name == 'textarea'}
                            {if isset($aData.$field_key)}
                                {$aData.$field_key}
                            {else}
                                {$field.value}
                            {/if}
                        {/if}
                    {/if}
                </{$field.tag_name}>

                {if ($field.type == 'image' || $field.type == 'file') }
                    <div class="image_holder">
                        {if (!empty($aData.$field_key)) }
                            <img class="image_preview"
                                 src="/{$field.image_src}{$aData.$field_key}" width="100%">
                            <button value="" type="button" class="btn_cancel image_remove"
                                    onclick="fields_remove_image(this,{$nId},'{$field_key}')">
                                {translate code="form_delete" text="Удалить"}
                            </button>
                        {/if}
                    </div>
                {/if}

    			{if (!empty($field.suffix))}
                      <span class="input-group-addon" id="basic-addon1">{$field.suffix}</span>
                {/if}
                {if (!empty($field.prefix) || !empty($field.suffix))}
                    </div>
                {/if}
                {if (!empty($field.description))}
                <p class="form-text text-muted">
                    {$field.description}
                </p>
                {/if}
        	</div>
            {if $field.multiple}
                {if isset($aData.screenshots)}
                <script>
                    $("input#def_{$i}").fileinput({literal}{{/literal}
                        initialPreview: {$aData.screenshots.preview|@json_encode},
                        initialPreviewAsData: true,
                        overwriteInitial: false,
                        deleteUrl: '{$aConf.base_url}{$aConf.active_module}/remove_file/{$nId}',
                        initialPreviewConfig: {$aData.screenshots.config|@json_encode},
                        initialCaption: "{$field.title}",
                        maxFilePreviewSize: 10240
                    {literal}}{/literal});
                    $('input#def_{$i}').on('filesorted', function(event, params) {literal}{{/literal}
                        var sort = [];
                        $( '.file-preview-thumbnails > .file-preview-frame' ).each(function( index ) {literal}{{/literal}
                            sort[index] = $(this).attr('data-uri');
                        {literal}}{/literal});
                        $.post('', {literal}{{/literal}sort: sort{literal}}{/literal}, function(data){literal}{{/literal}console.log(data);{literal}}{/literal})
                    {literal}}{/literal});
                </script>
                {/if}
            {/if}
            {if $field.type == 'wysiwyg'}
                <script>tinymce.init({ selector:'textarea#def_{$i}', plugins : "paste", inline_styles : false });</script>
            {/if}
        </div>
    {/if}
{/if}
{/strip}