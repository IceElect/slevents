{* include file="back_header.tpl"*}
{strip}

    {$disabled = ''}
    {if (!$perm_edit)}
        {$disabled = 'disabled="disabled"'}
    {/if}
        
        <table class="permissions table" cellpadding="0" cellspacing="0" >
        <tr class="table_head tr">
            <th class="th">{translate text="Модули\Группы" code="modules\groups"}</th>            
            {foreach $aUserGroups as $key => $group}
    <th class="th">{$group->translate}</th>
{/foreach}
        </tr>
        {$cur_module = ''}

    {foreach $aPermEntites as $key => $entity}
        {if $cur_module != $entity->module}
            {$cur_module = $entity->module}
            <tr class="tr divide_row">
                <td class="td head_2">{$entity->module}</td>
                {foreach $aUserGroups as $key => $group}
                    <td class="td">
                        <label for="check_all_{$entity->id}_{$group->id}">Check All</label>
                        <input id="check_all_{$entity->id}_{$group->id}"
                               name="check_all"
                               type="checkbox" {$disabled}
                               autocomplete="off"
                               onclick="check_all_perm(this, '{$cur_module}',{$group->id})"/>
                    </td>
                {/foreach}
            </tr>
        {/if}
        <tr class="tr">
            <th class="th">
                {$entity->translate}
            </th>
            {foreach $aUserGroups as $key => $group}
                <td class="td ca_{$cur_module}_{$group->id}">
                    {$checked = ''}
                    {if (!empty($aPermissions[$entity->id][$group->id]))}
                        {$checked = 'checked="checked"'}
                    {/if}
                    <input id="{$entity->id}_{$group->id}"
                           name="perm[{$entity->id}][{$group->id}]"
                           type="checkbox" {$checked} value="1" {$disabled}
                           autocomplete="off"
                           data-entity="{$entity->id}"
                           data-group="{$group->id}"
                           onclick="save_permission(this)"/>
                </td>
            {/foreach}
        </tr>
    {/foreach}
        
		</table>
        <div id="pager" class="scroll"></div>       
    </div>    

{literal}
    <script>
        function save_permission(elem){
        //prop('checked')
        //console.log($(elem).attr('mod_id'));
        var checked = $(elem).prop('checked');
        var group_id = jQuery(elem).data('group');
        var entity_id = jQuery(elem).data('entity');
        jQuery(elem).prop("disabled", true);
        $.ajax({
        type: 'POST',
        url: '{/literal}{$aConf.base_url}{$aConf.active_module}{literal}'+'/save/',
        dataType: 'json',
        success: function (data) {
            if (!data.perm) {
                jQuery(elem).prop('checked', !checked);
            } else {
                jQuery(elem).prop("disabled", false);
            }

        },
        data: {'ajax': 1, entity_id: entity_id, group_id: group_id, val: checked},
        async: true
        });
        return false;
        }

        function check_all_perm(elem, module, group_id) {
            var checked = jQuery(elem).prop("checked");
            jQuery('.ca_' + module + '_' + group_id + ' input').prop("checked", checked);
            jQuery('.ca_' + module + '_' + group_id + ' input').each(function () {
                save_permission(this);
            });
        }
    </script>
{/literal}


{/strip}
{*include "back_footer.tpl"*}
