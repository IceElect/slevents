{strip}
    {nocache}
        <ul>
            {foreach $list as $key => $branch}
                <li data-id="ele-{$branch->id}" class="ui-state-default ui-sortable-handle">
                    <div>
                        <span class="ui-icon"></span>
                        {$branch->name}
                        <a href="javascript:void(0)" onclick="deleteLink(this)"
                           class="fright ui-state-hover buttons-edit">{translate code="form_delete" text="Удалить"}</a>
                        <a href="{$aConf.base_url}{$aConf.active_module}/edit/{$branch->id}"
                           class="fright ui-state-hover buttons-edit">{translate code="form_edit" text="Изменить"}</a>
                    </div>
                    {if (!empty($branch->children))}
                        {include file="admin/reyting_category_tree.tpl" list=$branch->children}
                    {/if}
                </li>
            {/foreach}
        </ul>
    {/nocache}
{/strip}