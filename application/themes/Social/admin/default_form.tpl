{strip}
    {assign var="required_star" value=""}
    {assign var="i" value='0'}
    {nocache}
        <div class="main-col">
            {include file="sys/messages.tpl"}
            <div class="user-page">
                <form {foreach from=$form_attributes key=var item=value}{$var}="{$value}"{/foreach}>
                <div class="col-content form-horizontal left-content fll">
                        {csrf}
                        <div class="thumb">
                            <div class="pedit-form">
                                {if $action ne 'add'}
                                    <input id="item_id" type="hidden" name="id" value="{$nId}"/>
                                {else}
                                    <input type="hidden" name="change_pwd" value="1"/>
                                {/if}
                                <!--
                                {if $action ne 'add'}
                                    <div class="form-group">
                                        <div class="col-lg-2 col-sm-2 control-label">
                                            ID :
                                        </div>
                                        <div class="col-sm-6 infofield">
                                            <p class="form-control-static">{$nId}</p>
                                        </div>
                                    </div>
                                {/if}
                                -->
                                {$num = 0}
                                {foreach from=$aFields key="num" item="fields" }
                                    {if (!isset($tabs.$num))}{continue}{/if}
                                    <section class="content tab-content {if $num == 0}current{/if}" id="{$tabs.$num.id}">
                                        {foreach from=$fields key="field_key" item="field" }
                                            {$i = $i + 1}
                                            {if !empty($field.custom_field_template)}
                                                {assign var=path value=$field.custom_field_template}
                                                {include file="custom/$path.tpl" field=$field aData=$aData i=$i}
                                            {else}
                                                {include file='admin/default_form_row.tpl' field=$field aData=$aData i=$i}
                                            {/if}
                                        {/foreach}
                                        {if isset($hooks.{$tabs.$num.id})}
                                            {foreach from=$hooks.{$tabs.$num.id} key=key item=hook}
                                                {include file="default/{$hook}.tpl"}
                                            {/foreach}
                                        {/if}
                                        {*
                                        <div class="form-row clearfix">
                                            <div class="form-label"></div>
                                            <div class="form-input-wrap">
                                                <button type="submit" name="submit" value="1" class="button flr">Сохранить</button>
                                            </div>
                                        </div>
                                        *}
                                    </section>
                                {/foreach}
                                {if $tabs}
                                    </div>
                                {/if}

                            {if isset($hooks.after_form)}
                                {foreach from=$hooks.after_form key=key item=hook}
                                    {include file="default/{$hook}.tpl"}
                                {/foreach}
                            {/if}
                        </div>
                </div>
                <div class="col-sidebar flr">
                    {if $tabs}
                        <div class="thumb tabs">
                            {foreach from=$tabs key=i item=tab}
                                <div role="tab" id="{$tab.id}" href="#{$tab.id}" aria-controls="{$tab.id}" class="body-nest tab {if $i == 0}current{/if}">{$tab.title}</div>
                            {/foreach}
                        </div>
                    {/if}
                    
                    <div class="thumb">
                        <div class="thumb-title">
                            <span class="thumb-title-inner">Действия</span>
                        </div>
                        <div class="body-nest">
                            {if isset($aFields.sidebar)}
                                {foreach from=$aFields.sidebar key="field_key" item="field" }
                                    {$i = $i + 1}
                                    {include file='admin/default_form_row.tpl' field=$field aData=$aData i=$i}
                                {/foreach}
                            {/if}

                            <div class="buttons-group">
                                <button type="submit" name="submit" value="1" class="button full_width">
                                    Сохранить{*<span>{translate code="form_save" text="Сохранить"}</span>*}
                                </button>
                                <a class="button gray" href="{$aConf.base_url}{$aConf.active_module}">{translate code="form_cancel" text="Отмена"}</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    

                    {if isset($hooks.after_sidebar)}
                        {foreach from=$hooks.after_sidebar key=key item=hook}
                            {include file="default/{$hook}.tpl"}
                        {/foreach}
                    {/if}
                </div>

                </form>

            </div>
        </div>
    {/nocache}
    <script type="text/javascript">
        {literal}

        function fields_remove_image(el, id, field_name) {
            $.ajax({
                type: 'POST',
                url: '{/literal}{$aConf.base_url}{$aConf.active_module}/remove_file/{literal}' + id + '/' + field_name,
                dataType: 'json',
                success: function (data) {
                    $(el).parent('.image_holder').hide();
                },
                data: {'ajax': 1},
                async: false
            });
        }
        $(document).on('click', '.tabs .tab', function(e){
            e.preventDefault();

            var id = $(this).attr('aria-controls'),
                tabs = $(this).parents('.tabs');
            
            tabs.find('.current').removeClass('current');
            $(this).addClass('current');

            $(".content.tab-content.current").removeClass('current');
            $(".content.tab-content#"+id).addClass('current');
        })
        {/literal}
    </script>
{/strip}