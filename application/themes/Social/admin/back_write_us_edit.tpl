﻿{include file="back_header_light.tpl"}
{assign var="required_star" value="<span class='required'>*</span>"}
<div id="content">
    {nocache}
        <div id="icm_box">
            <form action="{$aConf.base_url}pages/edit/{if !empty($nId) }{$nId}{/if}" method="post" id="frm">
                <input type="hidden" name="action" value="{$action}"/>
                {if $action ne 'add'}
                    <input type="hidden" name="id" value="{$nId}"/>
                {else}
                    <input type="hidden" name="change_pwd" value="1"/>
                {/if}
                <div class="boxedit boxedit_pages">
                    <div class="table" id="editbox_pages">
                        {if $action ne 'add'}
                            <div class="row">
                                <div class="cell30 caption">
                                    {$required_star} ID:
                                </div>
                                <div class="cell30 aleft" style="padding-top:5px;">
                                    {$nId}
                                </div>
                            </div>
                        {/if}
                        <div class="row">
                            <div class="cell30 caption">
                                {$required_star} Титул:
                            </div>
                            <div class="cell30">
                                {input_box class="inp_group"}
                                    <input type="text" name="meta_title" class="inp"
                                           value="{if isset($aData.meta_title) }{$aData.meta_title}{/if}"/>
                                {/input_box}
                            </div>
                        </div>
                        <div class="row">
                            <div class="cell30 caption">
                                Меню:
                            </div>
                            <div class="cell30">
                                {input_box class="inp_group" }
                                    <select name="menu_id" class="inp" style="width:100%;">
                                        <option value=""></option>
                                        {foreach from=$menu_list key="menu_key" item="menu" }
                                            <option value="{$menu->id}"
                                                    {if isset($aData.menu_id) && $aData.menu_id eq $menu->id}selected{/if}>{$menu->name}</option>
                                        {/foreach}
                                    </select>
                                {/input_box}
                            </div>
                        </div>
                        <div class="row">
                            <div class="cell30 caption">
                                {$required_star} Ключевые описание:
                            </div>
                            <div class="cell30">
                                {input_box class="inp_group"}
                                    <input type="text" name="meta_description" class="inp"
                                           value="{if isset($aData.meta_description) }{$aData.meta_description}{/if}"/>
                                {/input_box}
                            </div>
                        </div>
                        <div class="row">
                            <div class="cell30 caption">
                                {$required_star} Ключевые слова:
                            </div>
                            <div class="cell30">
                                {input_box class="inp_group" }
                                    <input type="text" name="meta_keywords" class="inp"
                                           value="{if isset($aData.meta_keywords) }{$aData.meta_keywords}{/if}"/>
                                {/input_box}
                            </div>
                        </div>

                        <div class="row">
                            <div class="cell30 caption">
                                {$required_star} Язык:
                            </div>
                            <div class="cell30">
                                {input_box class="inp_group" }
                                    <select name="lang_id" class="inp" style="width:100%;">
                                        {foreach from=$language_list key="lang_key" item="lang" }
                                            <option value="{$lang->id}"
                                                    {if isset($aData.lang_id) && $aData.lang_id eq $lang->id}selected{/if}>{$lang->code}</option>
                                        {/foreach}
                                    </select>
                                {/input_box}
                            </div>
                        </div>
                        <div class="row">
                            <div class="cell30">
                                {$required_star} Обязательные поля
                            </div>
                        </div>

                    </div>
                </div>

                <div class="box_btn">

                    <button type="submit" name="submit" value="1"
                            class="fg-button-pos fg-button  fg-button-icon-right ui-widget ui-state-default ui-corner-all">
                        Сохранить
                        <span class="ui-icon ui-icon-disk fg-btn-ico"></span>
                    </button>
                    <button onclick="closeParentWin();callbackClose();"
                            class="fg-button-pos fg-button  fg-button-icon-right ui-widget ui-state-default ui-corner-all">
                        Закрыть
                        <span class="ui-icon ui-icon-close fg-btn-ico"></span>
                    </button>
                </div>
            </form>
        </div>
    {/nocache}
</div>
<script type="text/javascript">
    /* <![CDATA[ */
    {literal}
    jQuery().ready(function () {
        $('#submit').click(function () {
            $('#frm').submit();
        });

    }); // end rdocument.eady

    function showHidePwd(obj) {
        if (obj.checked) {
            $('#pwd_box').show();
        } else $('#pwd_box').hide();
    }
    function callbackClose() {
        parent.jQuery("#list").jqGrid().trigger("reloadGrid");
        top.clearDialogWin();
    }

    top.closeModalCustom = function () {
        callbackClose();
    }


    {/literal}
    /* ]]> */
</script>
{/strip}
{include file='footer.light.tpl'}