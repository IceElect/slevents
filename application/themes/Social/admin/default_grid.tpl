{if (!isset($get_table_only))}
<div class="main-col">
    <div class="nest" id="tableClose">
        <div class="title-alt">
            <h6>{$moduleTitle}</h6>
            <i>{$aData->records}</i>
            <div class="titleClose">
                <a class="gone" href="#tableClose">
                    <span class="entypo-cancel"></span>
                </a>
            </div>
            <div class="titleToggle">
                <a class="nav-toggle-alt" href="#table">
                    <span class="entypo-up-open"></span>
                </a>
            </div>

        </div>

        <div class="body-nest" id="table">
            <section id="flip-scroll">
{/if}
                <form action="/{$aConf.active_module}{$delAction}" method="post" data-type="ajax">
                    <input type="hidden" name="oper" value="del">
                    <div class="list_header">
                        <div class="row">
                            <div class="fll">
                                {if isset($left.buttons)}
                                    {foreach from=$left.buttons item=$button}
                                        <a href="{$button.url}" class="button btn-info button-list" {if isset($button.onclick)}onclick="{$button.onclick}"{/if}>{if isset($button.icon)}<i class="{$button.icon}"></i> {/if}{$button.text}</a>
                                    {/foreach}
                                {/if}
                                <button type="success" name="oper" value="del" style="margin-right:10px" type="button" data-color="#39B3D7" data-opacity="0.95" class="btn btn-default button">
                                    <i class="icon icon-trash"></i>
                                </button>

                                <button type="button" data-color="#39B3D7" data-opacity="0.95" class="btn btn-default button test pull-left">
                                    <span class="entypo-arrows-ccw"></span><span class="hidden-xs hidden-md hidden-sm">&nbsp;&nbsp;Обновить</span>
                                </button>
                            </div>


                            <div class="flr">
                                <div class="btn-group flr">
                                    {$pagination}
                                </div>
                                <div class="btn-group flr" style="margin-right:10px;">
                                    <button type="button" class="button">{$aData->offset+1}-{$aData->offset+($aData->rows|@count)} из {$aData->records}</button>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div id="content-list">
                        <table class="table table-bordered table-striped cf">
                            <thead>
                                {foreach from=$aFields key="field_key" item="field"}
                                    {if $field.table_show}
                                    <th class="{if (!empty($field.table_align))} text-{$field.table_align} {/if}"
                                        data-id="{$field.field}"
                                        {if (!empty($field.table_width))} width="{$field.table_width}px" {/if}>
                                        <a  {if ($field.sortable == true)}
                                            href="?sort={$field.field}&sort_type={if $aData->sort == {$field.field}}{if $aData->sort_type == 'desc'}asc{else}desc{/if}{else}desc{/if}"
                                            {/if}>
                                            {$field.title} 
                                            {if ($field.sortable == true)}
                                                <i class="fa {if $aData->sort == {$field.field}}{if $aData->sort_type == 'desc'}fa-sort-down{else}fa-sort-up{/if}{else}fa-sort{/if}
                                                "></i>
                                            {/if}
                                        </a>
                                    </th>
                                    {/if}
                                {/foreach}
                            </thead>
                            <tbody>
                                {foreach from=$aData->rows key="row_key" item="row"}
                                    <tr data-href="{$aConf.base_url}{$aConf.active_module}{$editAction}/{$row.id}">
                                        {foreach from=$row.cell key="id" item="field"}
                                            {if $aFields.$id.table_show}
                                                <td align="{$aFields.$id.table_align}" {if !$aFields.$id.view_callback}data-name="{$id}"{/if}>{$field}</td>
                                            {/if}
                                        {/foreach}
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div>
                </form>
{if (!isset($get_table_only))}
                {$pagination}
            </section>

        </div>

    </div>
</div>
{/if}