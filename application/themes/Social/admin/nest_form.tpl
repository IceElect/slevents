{strip}
    {assign var="required_star" value="<span class='required'>*</span>"}
    {assign var="i" value='0'}
<li class="nest" data-linkid="{$block_id}">
    <div class="title-alt">
        <h6>{$title}</h6>
        <div class="titleClose">
            <a class="gone" href="#elementClose">
                <span class="entypo-cancel"></span>
            </a>
        </div>
        <div class="titleToggle">
            <a class="nav-toggle-alt" href="#element">
                <span class="entypo-up-open"></span>
            </a>
        </div>
    </div>
    <div class="body-nest">
        <!--
        {if $action ne 'add'}
            <div class="form-group">
                <div class="col-lg-2 col-sm-2 control-label">
                    ID :
                </div>
                <div class="col-sm-6 infofield">
                    <p class="form-control-static">{$nId}</p>
                </div>
            </div>
        {/if}
        -->
        {$num = 0}
        {foreach from=$aFields key="num" item="fields" }
            <section class="content tab-content {if $num == 0}current{/if}" id="{$tabs.$num.id}">
                <div class="form-group">
                    <div class="col-lg-3 col-sm-3 control-label"></div>
                    <div class="col-sm-6 infofield">
                        <p class="form-control-static">{$required_star} {translate code="form_required_fields" text="Обязательные поля"}</p>
                    </div>
                </div>
                {foreach from=$fields key="field_key" item="field" }
                    {$i = $i + 1}
                    {if !empty($field.custom_field_template)}
                        {assign var=path value=$field.custom_field_template}
                        {include file="custom/$path.tpl" field=$field aData=$aData i=$i}
                    {else}
                        {include file='admin/default_form_row.tpl' field=$field aData=$aData i=$i}
                    {/if}
                {/foreach}
                {if isset($hooks.{$tabs.$num.id})}
                    {foreach from=$hooks.{$tabs.$num.id} key=key item=hook}
                        {include file="default/{$hook}.tpl"}
                    {/foreach}
                {/if}
                <div class="clearfix"></div>
            </section>
        {/foreach}
        {if $tabs}
            </div>
        {/if}
</li>
{/strip}