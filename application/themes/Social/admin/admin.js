function add_event(e){
	e.preventDefault();

	popup.show('add_event', {}, function(a){
		console.log(a);
	});

	return false;
}

function edit_ticket(id, event_id, e){
    e.preventDefault();

    popup.show('edit_ticket', {id: id, event_id: event_id}, function(a){
        console.log(a);
    });

    return false;
}

function del_ticket(id, event_id, e){
    e.preventDefault();

    $.post('/ajax/ticket/del', {id: id, event_id: event_id}, function(data){
        data = JSON.parse(data);
        
        $("#tickets").html(data.table_html);
    })

    return false;
}

function edit_personal(id, event_id, e){
    e.preventDefault();

    popup.show('edit_personal', {id: id, event_id: event_id}, function(a){
        data = JSON.parse(data);
    });

    return false;
}

function del_personal(id, event_id, e){
    e.preventDefault();

    $.post('/ajax/ticket/del_personal', {id: id, event_id: event_id}, function(data){
        data = JSON.parse(data);
        
        $("#personal").html(data.table_html);
    })

    return false;
}

function edit_visitor(id, event_id, e){
    e.preventDefault();

    popup.show('edit_visitor', {id: id, event_id: event_id}, function(a){
        data = JSON.parse(data);
    });

    return false;
}

function del_visitor(id, event_id, e){
    e.preventDefault();

    $.post('/ajax/ticket/del_visitor', {id: id, event_id: event_id}, function(data){
        data = JSON.parse(data);
        
        $("#visitors").html(data.table_html);
    })

    return false;
}