<!--  DEVICE MANAGER -->
<!--
<div class="content-wrap">
    <div class="row">
        <div class="col-sm-3">
            <div class="profit" id="profitClose">
                <div class="headline ">
                    <h3>
                        <span>
                            <i class="maki-ferry"></i>&#160;&#160;Ferry Arrival</span>
                    </h3>
                    <div class="titleClose">
                        <a href="#profitClose" class="gone">
                            <span class="entypo-cancel"></span>
                        </a>
                    </div>
                </div>

                <div class="value">
                    <span class="pull-left"><i class="entypo-clock clock-position"></i>
                    </span>
                    <div id="getting-started">
                        <span>%M</span>

                        <span>%S</span>
                    </div>


                </div>

                <div class="progress-tinny">
                    <div style="width: 50%" class="bar"></div>
                </div>
                <div class="profit-line">
                    <i class="fa fa-caret-up fa-lg"></i>&#160;&#160; 50% &#160;From Last Hour</div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="revenue" id="revenueClose">
                <div class="headline ">

                    <h3>
                        <span>
                            <i class="maki-aboveground-rail"></i>&#160;&#160;Train Speed</span>
                    </h3>

                    <div class="titleClose">
                        <a href="#revenueClose" class="gone">
                            <span class="entypo-cancel"></span>
                        </a>
                    </div>
                </div>
                <div class="value">
                    <span class="pull-left"><i class="entypo-gauge gauge-position"></i>
                    </span>
                    <canvas id="canvas4" width="70" height="70"></canvas>
                    <i class="pull-right">/Km</i>

                </div>








                <div class="progress-tinny">
                    <div style="width: 25%" class="bar"></div>
                </div>
                <div class="profit-line">
                    <i class="fa fa-caret-down fa-lg"></i>&#160;&#160;Rate : 20 km/Hour</div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="order" id="orderClose">
                <div class="headline ">
                    <h3>
                        <span>
                            <i class="maki-airport"></i>&#160;&#160;AIR PORT TRAFFIC</span>
                    </h3>
                    <div class="titleClose">
                        <a href="#orderClose" class="gone">
                            <span class="entypo-cancel"></span>
                        </a>
                    </div>
                </div>
                <div class="value">
                    <span><i class="fa fa-plane fa-2x"></i>
                    </span><b id="speed"></b><b>Take Off</b>

                </div>

                <div class="progress-tinny">
                    <div style="width: 10%" class="bar"></div>
                </div>
                <div class="profit-line">
                    <i class="fa fa-caret-down fa-lg"></i>&#160;&#160;Rate : 20 Plane/Hour</div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class=" member" id="memberClose">
                <div class="headline ">
                    <h3>
                        <span>
                            <i class="fa fa-truck"></i>
                            &#160;&#160;CARGO
                        </span>
                    </h3>
                    <div class="titleClose">
                        <a href="#memberClose" class="gone">
                            <span class="entypo-cancel"></span>
                        </a>
                    </div>
                </div>
                <div class="value">
                    <span><i class="maki-warehouse"></i>
                    </span>45<b>Sent</b>

                </div>
                <div class="progress-tinny">
                    <div style="width: 50%" class="bar"></div>
                </div>
                <div class="profit-line">
                    <span class="entypo-down-circled"></span>&#160;50% From Last Month</div>
            </div>
        </div>
    </div>
</div>
<!--  / DEVICE MANAGER -->









<!--
<div class="content-wrap">
    <div class="row">
        <div class="col-sm-8">
            <div id="siteClose" class="nest">
                <div class="title-alt">
                    <h6>
                        <span class="fontawesome-truck"></span>&nbsp;Destination</h6>
                    <div class="titleClose">
                        <a class="gone" href="#siteClose">
                            <span class="entypo-cancel"></span>
                        </a>
                    </div>
                    <div class="titleToggle">
                        <a class="nav-toggle-alt" href="#site">
                            <span class="entypo-up-open"></span>
                        </a>
                    </div>
                </div>
                <div id="site" class="body-nest" style="min-height:296px;">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="armada-devider">
                                        <div class="armada">
                                            <span style="background:#65C3DF">
                                                <span class="maki-bus"></span>&nbsp;&nbsp;Trans Berlin</span>
                                            <p>
                                                <span class="entypo-gauge"></span>&nbsp;12 Km/<i>Hours</i>
                                            </p>
                                        </div>
                                    </td>
                                    <td class="driver-devider">
                                        <img class="armada-pic img-circle" alt="" src="http://api.randomuser.me/portraits/thumb/men/14.jpg">
                                        <h3>Mark Zukenbeirg</h3>
                                        <br>
                                        <p>Driver</p>
                                    </td>
                                    <td class="progress-devider">


                                        <section id="basic">
                                            <article>

                                                <div class="number-pb">
                                                    <div class="number-pb-shown"></div>
                                                    <div class="number-pb-num">0</div>
                                                </div>
                                            </article>
                                        </section>
                                        <span class="label pull-left">Berlin</span>
                                        <span class="label pull-right">Muchen</span>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="armada-devider">
                                        <div class="armada">
                                            <span style="background:#45B6B0">
                                                <span class="fontawesome-plane"></span>&nbsp;&nbsp;Fly Airlines</span>
                                            <p>
                                                <span class="entypo-gauge"></span>&nbsp;600 Km/<i>Hours</i>
                                            </p>
                                        </div>
                                    </td>
                                    <td class="driver-devider">
                                        <img class="armada-pic img-circle" alt="" src="http://api.randomuser.me/portraits/thumb/men/38.jpg">
                                        <h3>Marko Freytag</h3>
                                        <br>
                                        <p>Pilot</p>
                                    </td>
                                    <td class="progress-devider">


                                        <section id="percentage">
                                            <article>
                                                <div class="number-pb">
                                                    <div class="number-pb-shown dream"></div>
                                                    <div class="number-pb-num">0%</div>
                                                </div>
                                            </article>
                                        </section>


                                        <span class="label pull-left">Berlin</span>
                                        <span class="label pull-right">London</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="armada-devider">
                                        <div class="armada">
                                            <span style="background:#FF6B6B">
                                                <span class="maki-aboveground-rail"></span>&nbsp;&nbsp;Fazttrain</span>
                                            <p>
                                                <span class="entypo-gauge"></span>&nbsp;40 Km/<i>Hours</i>
                                            </p>
                                        </div>
                                    </td>
                                    <td class="driver-devider">
                                        <img class="armada-pic img-circle" alt="" src="http://api.randomuser.me/portraits/thumb/men/39.jpg">
                                        <h3>Dieter Gruenewald</h3>
                                        <br>
                                        <p>machinist</p>
                                    </td>
                                    <td class="progress-devider">

                                        <section id="step">
                                            <article>

                                                <div class="number-pb">
                                                    <div class="number-pb-shown sun"></div>
                                                    <div class="number-pb-num">0/0</div>
                                                </div>
                                            </article>
                                        </section>

                                        <span class="label pull-left">Berlin</span>
                                        <span class="label pull-right">Dortmund</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div id="RealTimeClose" class="nest">
                <div class="title-alt">
                    <h6>
                        <span class="fontawesome-resize-horizontal"></span>&nbsp;Direction</h6>
                    <div class="titleClose">
                        <a class="gone" href="#RealTimeClose">
                            <span class="entypo-cancel"></span>
                        </a>
                    </div>
                    <div class="titleToggle">
                        <a class="nav-toggle-alt" href="#RealTime">
                            <span class="entypo-up-open"></span>
                        </a>
                    </div>
                </div>
                <div id="RealTime" style="min-height:296px;padding-top:20px;" class="body-nest">
                    <ul class="direction">
                        <li>
                            <span class="direction-icon maki-fuel" style="background:#FF6B6B"></span>
                            <h3>
                                <span>Gas Station</span>
                            </h3>
                            <p>5 Km Foward&nbsp;&nbsp;<i class="fa fa-arrow-circle-up"></i>
                            </p>
                            <p><i>Estimated time </i>:&nbsp;<i class="fa fa-clock-o"></i>&nbsp;&nbsp;20 Min</p>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <span class="direction-icon maki-fast-food" style="background:#65C3DF"></span>
                            <h3>
                                <span>Restourant</span>
                            </h3>
                            <p>1 Km Turn Left&nbsp;&nbsp;<i class="fa fa-reply"></i>
                            </p>
                            <p><i>Estimated time </i>:&nbsp;<i class="fa fa-clock-o"></i>&nbsp;&nbsp;20 Min</p>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <span class="direction-icon maki-giraffe" style="background:#45B6B0"></span>
                            <h3>
                                <span>Zoo</span>
                            </h3>
                            <p>3 Km Turn Right &nbsp;&nbsp;<i class="fa fa-share"></i>
                            </p>
                            <p><i>Estimated time </i>:&nbsp;<i class="fa fa-clock-o"></i>&nbsp;&nbsp;20 Min</p>
                        </li>
                        <li class="divider"></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
-->
<div class="content-wrap">
    <div class="row">
        <div class="col-sm-12">
            <div class="nest" id="tableClose">
                <div class="title-alt">
                    <h6>Статистика</h6>
                    <div class="titleClose">
                        <a class="gone" href="#tableClose">
                            <span class="entypo-cancel"></span>
                        </a>
                    </div>
                    <div class="titleToggle">
                        <a class="nav-toggle-alt" href="#table">
                            <span class="entypo-up-open"></span>
                        </a>
                    </div>

                </div>

                <div class="body-nest" id="table">

                    <section id="flip-scroll">

                        <table class="table table-bordered table-striped cf">
                            <thead class="cf">
                                <tr>
                                    <th><input type="checkbox"></th>
                                    <th><a href="?sort=code&sort_type=desc">Code <i class="fa fa-sort-up"></i></a></th>
                                    <th><a href="?sort=code&sort_type=desc&sort_1=1">Company <i class="fa fa-sort"></i></a></th>
                                    <th class="numeric"><a href="?sort=code&sort_type=desc">Price <i class="fa fa-sort"></i></a></th>
                                    <th class="numeric"><a href="?sort=code&sort_type=desc">Change <i class="fa fa-sort"></i></a></th>
                                    <th class="numeric"><a href="?sort=code&sort_type=desc">Change % <i class="fa fa-sort"></i></a></th>
                                    <th class="numeric"><a href="?sort=code&sort_type=desc">Open <i class="fa fa-sort"></i></a></th>
                                    <th class="numeric"><a href="?sort=code&sort_type=desc">High <i class="fa fa-sort"></i></a></th>
                                    <th class="numeric"><a href="?sort=code&sort_type=desc">Low <i class="fa fa-sort"></i></a></th>
                                    <th class="numeric"><a href="?sort=code&sort_type=desc">Volume <i class="fa fa-sort"></i></a></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th><input type="checkbox"></th>
                                    <td>AAC</td>
                                    <td>AUSTRALIAN AGRICULTURAL COMPANY LIMITED.</td>
                                    <td class="numeric">$1.38</td>
                                    <td class="numeric">-0.01</td>
                                    <td class="numeric">-0.36%</td>
                                    <td class="numeric">$1.39</td>
                                    <td class="numeric">$1.39</td>
                                    <td class="numeric">$1.38</td>
                                    <td class="numeric">9,395</td>
                                </tr>
                                <tr>
                                    <th><input type="checkbox"></th>
                                    <td>AAD</td>
                                    <td>ARDENT LEISURE GROUP</td>
                                    <td class="numeric">$1.15</td>
                                    <td class="numeric">+0.02</td>
                                    <td class="numeric">1.32%</td>
                                    <td class="numeric">$1.14</td>
                                    <td class="numeric">$1.15</td>
                                    <td class="numeric">$1.13</td>
                                    <td class="numeric">56,431</td>
                                </tr>
                                <tr>
                                    <th><input type="checkbox"></th>
                                    <td>AAX</td>
                                    <td>AUSENCO LIMITED</td>
                                    <td class="numeric">$4.00</td>
                                    <td class="numeric">-0.04</td>
                                    <td class="numeric">-0.99%</td>
                                    <td class="numeric">$4.01</td>
                                    <td class="numeric">$4.05</td>
                                    <td class="numeric">$4.00</td>
                                    <td class="numeric">90,641</td>
                                </tr>
                                <tr>
                                    <th><input type="checkbox"></th>
                                    <td>ABC</td>
                                    <td>ADELAIDE BRIGHTON LIMITED</td>
                                    <td class="numeric">$3.00</td>
                                    <td class="numeric">+0.06</td>
                                    <td class="numeric">2.04%</td>
                                    <td class="numeric">$2.98</td>
                                    <td class="numeric">$3.00</td>
                                    <td class="numeric">$2.96</td>
                                    <td class="numeric">862,518</td>
                                </tr>
                                <tr>
                                    <th><input type="checkbox"></th>
                                    <td>ABP</td>
                                    <td>ABACUS PROPERTY GROUP</td>
                                    <td class="numeric">$1.91</td>
                                    <td class="numeric">0.00</td>
                                    <td class="numeric">0.00%</td>
                                    <td class="numeric">$1.92</td>
                                    <td class="numeric">$1.93</td>
                                    <td class="numeric">$1.90</td>
                                    <td class="numeric">595,701</td>
                                </tr>
                                <tr>
                                    <th><input type="checkbox"></th>
                                    <td>ABY</td>
                                    <td>ADITYA BIRLA MINERALS LIMITED</td>
                                    <td class="numeric">$0.77</td>
                                    <td class="numeric">+0.02</td>
                                    <td class="numeric">2.00%</td>
                                    <td class="numeric">$0.76</td>
                                    <td class="numeric">$0.77</td>
                                    <td class="numeric">$0.76</td>
                                    <td class="numeric">54,567</td>
                                </tr>
                                <tr>
                                    <th><input type="checkbox"></th>
                                    <td>ACR</td>
                                    <td>ACRUX LIMITED</td>
                                    <td class="numeric">$3.71</td>
                                    <td class="numeric">+0.01</td>
                                    <td class="numeric">0.14%</td>
                                    <td class="numeric">$3.70</td>
                                    <td class="numeric">$3.72</td>
                                    <td class="numeric">$3.68</td>
                                    <td class="numeric">191,373</td>
                                </tr>
                                <tr>
                                    <th><input type="checkbox"></th>
                                    <td>ADU</td>
                                    <td>ADAMUS RESOURCES LIMITED</td>
                                    <td class="numeric">$0.72</td>
                                    <td class="numeric">0.00</td>
                                    <td class="numeric">0.00%</td>
                                    <td class="numeric">$0.73</td>
                                    <td class="numeric">$0.74</td>
                                    <td class="numeric">$0.72</td>
                                    <td class="numeric">8,602,291</td>
                                </tr>
                                <tr>
                                    <th><input type="checkbox"></th>
                                    <td>AGG</td>
                                    <td>ANGLOGOLD ASHANTI LIMITED</td>
                                    <td class="numeric">$7.81</td>
                                    <td class="numeric">-0.22</td>
                                    <td class="numeric">-2.74%</td>
                                    <td class="numeric">$7.82</td>
                                    <td class="numeric">$7.82</td>
                                    <td class="numeric">$7.81</td>
                                    <td class="numeric">148</td>
                                </tr>
                                <tr>
                                    <th><input type="checkbox"></th>
                                    <td>AGK</td>
                                    <td>AGL ENERGY LIMITED</td>
                                    <td class="numeric">$13.82</td>
                                    <td class="numeric">+0.02</td>
                                    <td class="numeric">0.14%</td>
                                    <td class="numeric">$13.83</td>
                                    <td class="numeric">$13.83</td>
                                    <td class="numeric">$13.67</td>
                                    <td class="numeric">846,403</td>
                                </tr>
                                <tr>
                                    <th><input type="checkbox"></th>
                                    <td>AGO</td>
                                    <td>ATLAS IRON LIMITED</td>
                                    <td class="numeric">$3.17</td>
                                    <td class="numeric">-0.02</td>
                                    <td class="numeric">-0.47%</td>
                                    <td class="numeric">$3.11</td>
                                    <td class="numeric">$3.22</td>
                                    <td class="numeric">$3.10</td>
                                    <td class="numeric">5,416,303</td>
                                </tr>
                            </tbody>
                        </table>
                    </section>

                </div>

            </div>
        </div>
    </div>
</div>
<div class="content-wrap">
    <div class="row">
        <div class="col-sm-6">
            <div class="nest" id="SiteInfo">
                <div class="title-alt">
                    <h6>Информация о сайте</h6>
                    <div class="titleClose">
                        <a class="gone" href="#SiteInfo">
                            <span class="entypo-cancel"></span>
                        </a>
                    </div>
                    <div class="titleToggle">
                        <a class="nav-toggle-alt" href="#Addresses">
                            <span class="entypo-up-open"></span>
                        </a>
                    </div>

                </div>

                <div class="body-nest" id="Addresses">
                    <address>
                        <div class="row">
                            <div class="col-md-4">
                                <strong>Организация</strong>
                            </div>
                            <div class="col-md-8">
                                GabeVideoSystem
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <strong>Владелец сайта</strong>
                            </div>
                            <div class="col-md-8">
                                Алексей Сафронов
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <strong>Название сайта</strong>
                            </div>
                            <div class="col-md-8">
                                WowGirls
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <strong>Адрес сайта</strong>
                            </div>
                            <div class="col-md-8">
                                <a href="http://localhost" title="WowGirls">http://localhost</a>
                            </div>
                        </div>
                        
                    </address>

                    <div class="clearfix"></div>
                    <br>

                    <address>
                        <div class="row">
                            <div class="col-md-12">
                                <strong>Full Name</strong>
                                <br>
                                <a href="mailto:#">first.last@example.com</a>
                            </div>
                        </div>
                    </address>
                    <div class="clearfix"></div>
                    <br>
                    <a href="#">Изменить</a>

                </div>
            </div>

        </div>
    </div>
</div>


<div class="content-wrap" hidden>
    <div class="row">
        <div class="col-sm-6">
            <div class="chart-wrap">
                <div class="chart-dash">
                    <div id="placeholder" style="width:100%;height:200px;"></div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="speed">
                            <h2>Speed Avarage</h2>
                            <h1>74
                                <span>Km / hours</span>
                            </h1>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="traffic">
                            <h2>Traffic per day</h2>
                            <h1>2.5874</h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="traffic-rates">
                            <h4>Traffic Rates</h4>
                            <h1>30 %
                                <span>-1,3 %</span>
                            </h1>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="traffic-rates">
                            <h4>Traffic Rates</h4>
                            <h1>30 %
                                <span>-1,3 %</span>
                            </h1>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="traffic-rates">
                            <h4>Traffic Rates</h4>
                            <h1>30 %
                                <span>-1,3 %</span>
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="weather-wrap">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="temperature"><b>Monday</b>, 07:30 AM
                            <span>F</span>
                            <span><b>C</b>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="weather-icon">
                            <i class="wi-day-lightning"></i>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="weather-text">
                            <h2>Berlin
                                <br><i>Day Lightning</i>
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="weather-text">
                            <h3><i class="wi-thermometer"></i>18<i class="wi-celcius"></i>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="weather-dash">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="daily-weather">
                                <h2>Mon</h2>
                                <h3>85
                                    <span><i class="wi-fahrenheit"></i>
                                    </span>
                                </h3>
                                <h4>
                                    <span><i class="wi-day-lightning"></i>
                                    </span>
                                </h4>
                                <h5>15
                                    <i>km/h</i>
                                </h5>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="daily-weather">
                                <h2>Tue</h2>
                                <h3>81
                                    <span><i class="wi-fahrenheit"></i>
                                    </span>
                                </h3>
                                <h4><i class="wi-day-cloudy"></i>
                                </h4>
                                <h5>12
                                    <i>km/h</i>
                                </h5>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="daily-weather">
                                <h2>Wed</h2>
                                <h3>83
                                    <span><i class="wi-fahrenheit"></i>
                                    </span>
                                </h3>
                                <h4><i class="wi-rain-mix"></i>
                                </h4>
                                <h5>14
                                    <i>km/h</i>
                                </h5>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="daily-weather">
                                <h2>Thu</h2>
                                <h3>80
                                    <span><i class="wi-fahrenheit"></i>
                                    </span>
                                </h3>
                                <h4><i class="wi-day-sunny"></i>
                                </h4>
                                <h5>15
                                    <i>km/h</i>
                                </h5>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="daily-weather">
                                <h2>Fri</h2>
                                <h3>79
                                    <span><i class="wi-fahrenheit"></i>
                                    </span>
                                </h3>
                                <h4><i class="wi-day-storm-showers"></i>
                                </h4>
                                <h5>11
                                    <i>km/h</i>
                                </h5>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="daily-weather">
                                <h2>Sat</h2>
                                <h3>82
                                    <span><i class="wi-fahrenheit"></i>
                                    </span>
                                </h3>
                                <h4><i class="wi-cloudy"></i>
                                </h4>
                                <h5>10
                                    <i>km/h</i>
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /END OF CONTENT -->



    <!-- FOOTER -->
    <div class="footer-space"></div>
    <div id="footer">
        <div class="devider-footer-left"></div>
        <div class="time">
            <p id="spanDate"></p>
            <p id="clock"></p>
        </div>
        <div class="copyright">Make with Love
            <span class="entypo-heart"></span>2014 <a href="http://gamatechno.com">Thesmile</a> All Rights Reserved</div>
        <div class="devider-footer"></div>


    </div>
    <!-- / END OF FOOTER -->


</div>