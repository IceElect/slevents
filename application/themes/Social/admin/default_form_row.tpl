{strip}
{if isset($field.additional_field)}
    {assign var=v value='custom_'|cat:$field.additional_field}
    {if isset($aData.$v)}
        {$field.value = $aData.$v}
    {/if}
{/if}
{if ($field.form_show)}
    {if $field.type == 'hidden'}
        <input type="{$field.type}" name="{$field.field}" value="{$field.value}">
    {else}
        <div class="form-row clearfix {if $field.error}has-error{/if}" data-field="{$field.field}">
            <div class="form-label">
                <label for="def_{$i}">
                    {if (strpos($field.rules,'required')!== false)}{$required_star}{/if} 
                    {$field.title}
                </label>
            </div>
            <div class="form-input-wrap input-wrap-{$field.type}">
                {include file="default/fields/{$field.type}.tpl"}
                {if (!empty($field.description))}
                <p class="form-text text-muted">
                    {$field.description}
                </p>
                {/if}
        	</div>

            {if $field.multiple}
                {if isset($aData.screenshots)}
                <script>
                    $("input#def_{$i}").fileinput({literal}{{/literal}
                        initialPreview: {$aData.screenshots.preview|@json_encode},
                        initialPreviewAsData: true,
                        overwriteInitial: false,
                        deleteUrl: '{$aConf.base_url}{$aConf.active_module}/remove_file/{$nId}',
                        initialPreviewConfig: {$aData.screenshots.config|@json_encode},
                        initialCaption: "{$field.title}",
                        maxFilePreviewSize: 10240
                    {literal}}{/literal});
                    $('input#def_{$i}').on('filesorted', function(event, params) {literal}{{/literal}
                        var sort = [];
                        $( '.file-preview-thumbnails > .file-preview-frame' ).each(function( index ) {literal}{{/literal}
                            sort[index] = $(this).attr('data-uri');
                        {literal}}{/literal});
                        $.post('', {literal}{{/literal}sort: sort{literal}}{/literal}, function(data){literal}{{/literal}console.log(data);{literal}}{/literal})
                    {literal}}{/literal});
                </script>
                {/if}
            {/if}
            {if $field.type == 'wysiwyg'}
                <script>tinymce.init({ selector:'textarea#def_{$i}' });</script>
            {/if}
        </div>
    {/if}
{/if}
{/strip}