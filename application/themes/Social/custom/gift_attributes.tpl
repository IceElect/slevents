<div class="form-row clearfix">
    <div class="form-label">
        <label for="{$field.field}">{$field.title}</label>
    </div>
    <div class="form-input-wrap">
        <div class="attributes-tree">
            <ul class="attributes-list">
                {foreach from=$gift.attributes item=attribute_item}
                <li data-id="{$attribute_item.id}">
                    <div class="action-edit">
                        <a href="#" class="attribute-edit-cancel"><span class="md-icon">close</span></a> 
                        <a href="#" class="attribute-edit-save"><span class="md-icon">save</span></a> 
                        <input type="text" class="attribute-edit-field">
                    </div>
                    <div class="action-not-edit">
                        <a href="#" class="delete-attribute"><span class="md-icon">delete</span></a>
                        <a href="#" class="attribute-edit"><span class="md-icon">edit</span></a>
                        <div class="attribute-item-name">{$attribute_item.name}</div>
                    </div>
                    <ul>
                        {foreach from=$attribute_item.values item=value_item}
                            <li data-value-id="{$value_item.id}"  data-attribute-id="{$attribute_item.id}">
                                <a href="#" class="delete-value"><span class="md-icon">delete</span></a> 
                                <a href="#"><span class="md-icon">edit</span></a> 
                                <div class="attribute-value-name">{$value_item.value}</div>
                            </li>
                        {/foreach}
                        <li class="add-action add-value"><a href="#" data-attribute-id="{$attribute_item.id}"><span class="md-icon">add</span> Добавить значение</a></li>
                    </ul>
                </li>
                {/foreach}
                <li class="add-action add-attribute"><a href="#" data-gift-id="{$gift.id}"><span class="md-icon">add</span> Добавить аттрибут</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="hidden" hidden>
    <div id="example-attribute">
        <li data-id="replace_id">
            <a href="#" class="delete-attribute"><span class="md-icon">delete</span></a> 
            <a href="#"><span class="md-icon">edit</span></a> 
            <div class="attribute-item-name"></div>
            <ul>
                <li class="add-action add-value"><a href="#" data-attribute-id="replace_id"><span class="md-icon">add</span> Добавить значение</a></li>
            </ul>
        </li>
    </div>
    <div id="example-value">
        <li data-value-id="replace_id" data-attribute-id="replace_attribute_id">
            <a href="#" class="delete-value"><span class="md-icon">delete</span></a>
            <a href="#"><span class="md-icon">edit</span></a>
            <div class="attribute-value-name"></div>
        </li>
    </div>
</div>

<script>
    function add_attribute(id){
        var html = $("#example-attribute").html();
        html = html.replace(/replace_id/g, id);
        $(".attributes-tree .add-attribute").before(html);
        $('.attributes-list > li[data-id='+id+'] .attribute-item-name').html('Новый');
    }
    function add_value(attribute_id, value_id){
        var html = $("#example-value").html();
        html = html.replace(/replace_id/g, value_id);
        html = html.replace(/replace_attribute_id/g, attribute_id);
        $('.attributes-list > li[data-id='+attribute_id+'] > ul .add-value').before(html);
        $('.attributes-list > li[data-id='+attribute_id+'] > ul > li[data-value-id='+value_id+'] .attribute-value-name').html('Новое значение');
    }
    function del_attribute(attribute_id){
        $('.attributes-list > li[data-id='+attribute_id+']').remove();
    }
    function del_value(attribute_id, value_id){
        $('.attributes-list > li[data-id='+attribute_id+'] > ul > li[data-value-id='+value_id+']').remove();
    }

    $(document).on('click', '.attributes-tree > ul > .add-action a', function(e){ // ДОБАВЛЕНИЕ АТТРИБУТА
        e.preventDefault();

        var gift_id = $(this).data('gift-id');

        // Тут аякс
        var new_attribute_id = 0; // Получить из аякса
        add_attribute(new_attribute_id);
    })

    $(document).on('click', '.attributes-tree > ul > li .attribute-edit', function(e){ // ОТКРЫТЬ РЕДАКТИРОВАНИЕ АТРИБУТА
        e.preventDefault();

        var holder = $(this).parents('li');

        var attribute_id = holder.data('id');
        var attribute_name = holder.find('.attribute-item-name').html();

        holder.find('.action-edit input').val(attribute_name);

        holder.find('.action-edit').show();
        holder.find('.action-not-edit').hide();
    })

    $(document).on('click', '.attributes-tree > ul > li .attribute-edit-cancel', function(e){ // ОТМЕНИТЬ РЕДАКТИРОВАНИЕ АТТРИБУТА
        e.preventDefault();

        var holder = $(this).parents('li');

        holder.find('.action-edit').hide();
        holder.find('.action-not-edit').show();
    })

    $(document).on('click', '.attributes-tree > ul > li .attribute-edit-save', function(e){ // СОХРАНЕНИТЬ ИЗМЕНЕНИЕ АТТРИБУТА
        e.preventDefault();

        var holder = $(this).parents('li');

        holder.find('.action-edit').hide();
        holder.find('.action-not-edit').show();
    })
    $(document).on('click', '.attributes-tree > ul > li .delete-attribute', function(e){ // УДАЛИТЬ АТТРИБУТ
        e.preventDefault();

        var attribute_id = $(this).parents('li').data('id');

        del_attribute(attribute_id);
    })

    $(document).on('click', '.attributes-tree > ul > li > ul > .add-value a', function(e){ // ДОБАВИТЬ ЗНАЧЕНИЕ В АТТРИБУТ
        e.preventDefault();

        var attribute_id = $(this).data('attribute-id');

        // Тут аякс
        var new_value_id = 0; // Получить из аякса
        add_value(attribute_id, new_value_id);
    })
    $(document).on('click', '.attributes-tree > ul > li > ul > li .delete-value', function(e){ // УДАЛИТЬ ЗНАЧЕНИЕ АТТРИБУТА
        e.preventDefault();

        var value_id = $(this).parents('li').data('value-id');
        var attribute_id = $(this).parents('li').data('attribute-id');
        del_value(attribute_id, value_id);
    })
</script>