<div class="main-col">
    <h1 class="h2">Главная</h1>
    <div>
        <div class="sidebar">
            {include file="blocks/howto.tpl"}
            {include file="blocks/teaser.tpl"}
        </div>
        <div class="content">
            <div class="thumbs">
                {foreach from=$posts key=$i item=$post}
                    {include file="blog/post.tpl" post=$post}
                {/foreach}
            </div>
        </div>
    </div>
</div>