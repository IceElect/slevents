<style>
    .contacts{
        display: flex;
        justify-content: space-around;
    }
    .contacts b a{
        font-weight: bold;
    }
    @media screen and (max-width: 768px){
        .contacts{
            padding-top: 0px !important;
        }
        .contacts b{
            /*display: block;*/
            padding-bottom: 10px;
        }
        .contacts b:last-child{
            padding-bottom: 0px;
        }
    }
</style>
<div class="main-col">
    <h1 class="h2">Наша команда</h1>
    {*<div class="thumb padding" style="margin-top: 0px">
        Мы - Wandering Lights.
        <br>    
		Мы организуем концерты и конвенты по фэндомной тематике.
	   <br>
		Pony Rock Fest 17 декабря 2017 был нашим первым собственным мероприятием, но ребята из нашей команды имеют опыт в организации таких крупных конвентов как РуБрониКон, DerpFest, Summer Sun Celebration и многих других.
    </div>*}
    <div class="users-list users-list-tiles">
    	{foreach from=$contacts item=$user}
    	<div class="user user-tile block">
    		<div class="avatar big">
    			{get_avatar u=$user size=320}
    		</div>
    		<div class="info thumb padding">
    			<a href="{$hosted_url}@{$user->id}" class="name">{$user->fname} {$user->lname}</a>
                <span>{$user->role}</span>
    			<div class="buttons-group">
					<a href="{$hosted_url}im/{if $user->id}{$user->id}{else}5{/if}" class="button full_width">Сообщение</a>
					<a href="{$hosted_url}@{if $user->id}{$user->id}{else}5{/if}" class="button"><i class="fa fa-user"></i></a>
					<button class="button gray"><i class="fa fa-chevron-down"></i></button>
    			</div>
    		</div>
    	</div>
    	{/foreach}
    </div>
    <div class="thumb">
        <div class="inner contacts">
            <b><a href="callto:+79017448635">+7(901)744-86-35</b>
            <b><a href="mailto:fly@slto.ru">fly@slto.ru</a></b>
        </div>
    </div>
</div>