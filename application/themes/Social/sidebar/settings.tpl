{if $oUser->id}
<div class="users-list">
    <ul>
        <li class="user-item dialog-item">
            <a href="/@{$oUser->id}" class="avatar middle" data-type="load">
                {get_avatar id=$oUser->id}
            </a>
            <a href="/@{$oUser->id}" class="user {if ($oUser->last_action >= ($time - 900))}online{/if}" data-type="load">
                <span class="name">{$oUser->fname} {$oUser->lname}</span>
                <p>0 уровень</p>
            </a>
            <!--
            <div class="user-button">
                <button class="md-icon">textsms</button>
            </div>
            -->
        </li>
    </ul>
</div>
<ul class="app-content-menu">
    <li>
        <a href="/edit" data-type="load">
            <button class="md-icon">info_outline</button>
            <span>Информация</span>
        </a>
    </li>
    <li>
        <a href="/settings" data-type="load">
            <button class="md-icon">build</button>
            <span>Настройки</span>
        </a>
    </li>
    <li>
        <a href="/users/logout">
            <button class="md-icon">power_settings_new</button>
            <span>Выйти</span>
        </a>
    </li>
</ul>
{else}
    <div class="login_box">
        <input type="hidden" name="act" value="login">
        <div class="form_row">
            <input type="text" name="user_email" placeholder="Эл. почта">
        </div>
        <div class="form_row">
            <input type="text" name="user_name" placeholder="Имя и фамилия" hidden>
        </div>
        <div class="form_row">
            <input type="text" name="user_login" placeholder="Имя пользователя" hidden>
        </div>
        <div class="form_row">
            <input type="password" name="user_password" placeholder="Пароль">
        </div>
        <span class="error"></span>
        <a href="http://slto.ru/users/login?go=http://events.slto.ru&register=true">Регистрация</a> | <a href="#">Забыл пароль?</a>
        <input type="submit" name="submit" class="button big_button" id="login_but" value="Войти">
    </form>
{/if}