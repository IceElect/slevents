<div class="main-col">
    <h1 class="h2">Купить билеты</h1>
    <div class="tickets">
        {foreach from=$tickets item=event}
            <div class="ticket thumb padding">
                <div class="date-info mobile-hide">
                    <div class="odate">
                        <div class="day">{$event->day}<span class="wd"> / {$event->wd}</span></div>
                        <div class="weekday">{$event->month}</div>
                    </div>
                    <div class="time">{$event->time}</div>
                </div>
                <div class="loc-info">
                    <a href="/event/{$event->id}">{$event->name}</a>
                    {if $event->location}
                        <br>{$event->location}
                    {/if}
                    {if $event->location_cord}
                        <br>Координаты: {$event->location_cord}
                    {/if}
                </div>
                <div class="date-info mobile-show">
                    <div class="odate">
                        <div class="day">{$event->day}<span class="wd"> / {$event->wd}</span></div>
                        <div class="weekday">{$event->month}</div>
                    </div>
                    <div class="time">{$event->time}</div>
                </div>
                <div class="buy-info">
                    {if $event->closed}
                        <b>Продажа закрыта</b>
                    {else}
                        <a data-id="{$event->tickets[0]->id}" class="button buy-button {if !$oUser->id}login-button{/if}">Купить билет</a>
                        <button class="button change-ticket-button"><i class="fa fa-chevron-down"></i></button>
                        <div class="clearfix"></div>
                        <div class="count">Осталось <b>{$event->max_count - $event->t_count}</b> билетов</div>
                        <div class="price">{$event->tickets[0]->name} - {$event->tickets[0]->price} руб.</div>

                        <div class="change-ticket">
                            <ul>
                                {foreach from=$event->tickets item=t}
                                    <li class="button" data-id="{$t->id}" data-type="{$t->type}" data-name="{$t->name}" data-price="{$t->price}">{$t->name}</li>
                                {/foreach}
                            </ul>
                        </div>
                    {/if}
                </div>
            </div>
        {/foreach}
    </div>
    <div class="clearfix"></div>
    <script type="text/javascript">(function() {
      if (window.pluso)if (typeof window.pluso.start == "function") return;
      if (window.ifpluso==undefined) { window.ifpluso = 1;
        var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
        s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
        s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
        var h=d[g]('body')[0];
        h.appendChild(s);
      }})();</script>
    <script type="text/javascript">
        function toggle_change(elem){
            $(elem).find(".change-ticket").toggleClass("show");
        }
        $(document).on('click', ".change-ticket-button", function(e){
            e.preventDefault();
            toggle_change($(this).parents('.buy-info'));
        })
        $(document).on('click', ".change-ticket li", function(e){
            var h = $(this).parents('.buy-info'),
                id = $(this).attr('data-id'),
                name = $(this).attr('data-name'),
                type = $(this).attr('data-type'),
                price = $(this).attr('data-price');
            h.find('.price').html(name+' - '+price+' руб.');
            h.find('.buy-button').attr('data-id', id);
            //h.find('.buy-button').attr('href', '/ticket/buy/'+id).attr('data-type', type);
            toggle_change(h);
        })
    </script>
    <div class="hidden" id="pay-form">
        <script>
            {literal}
            $(document).on('click', ".buy-button", function(e){
                if(!aConf.is_logged){
                    popup.show('login');
                    return false;
                }
                var id = $(this).attr('data-id');
                var button = $(this);
                button.html('<i class="fa fa-circle-o-notch fa-spin"></i>');
                popup.show('pay', {id: id}, function(data){
                    button.html('Купить билет');
                })
            })
            {/literal}
        </script>
    </div>
    <div class="pluso" data-background="transparent" data-options="medium,square,line,horizontal,nocounter,theme=01" data-services="vkontakte,odnoklassniki,facebook,twitter"></div>
</div>