<table class="table" style="margin: 0">
    <thead>
        <tr>
            <th>Название</th>
            <th>Стоимость</th>
            <th>Количество</th>
            <th>Осталось</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        {foreach from=$tickets key=key item=ticket}
            <tr data-ticket-id="{$ticket->id}">
                <td>{$ticket->name}</td>
                <td>{$ticket->price}</td>
                <td>{$ticket->max_count}</td>
                <td>{$ticket->max_count - $ticket->t_count}</td>
                <td>
                    <a href="#"><i class="md-icon" onclick="edit_ticket({$ticket->id}, {$nId}, event)">edit</i></a>
                    <a href="#"><i class="md-icon" onclick="del_ticket({$ticket->id}, {$nId}, event)">delete</i></a>
                </td>
            </tr>
        {/foreach}
    </tbody>
</table>
<div class="row" style="margin-top: 10px">
    <button class="button" onclick="edit_ticket(0, {$nId}, event)">Добавить билет</button>
</div>