{strip}
    {assign var="required_star" value="<span class='required'>*</span>"}
    {assign var="disabled_attr" value='disabled = "disabled"'}
    {assign var="readonly_attr" value='readonly = "readonly"'}
    {nocache}
        <div class="form_wrapper">
            <form action="{$form.form_action}"
                  method="post" id="frm" enctype="multipart/form-data">
                {csrf}
                <div class="boxedit">
                    <div class="form">
                        <div class="form_row">
                            <div class="row_field">
                                <label>
                                    {$required_star} {translate code="form_required_fields" text="Обязательные поля"}
                                </label>
                            </div>
                        </div>
                        {$i = 0}
                        {foreach from=$form.fields key="field_key" item="field" }
                            {if (!isset($field.form_disable))}
                                {$i = $i +1}
                                {if ($field.type ne 'id') && ($field.type ne 'no_edit')}
                                    <div class="form_row">
                                        <div class="row_label col">
                                            <label for="def_{$i}">
                                                {$field.title} {if (strpos($field.rules,'required')!== false)}{$required_star}{/if}
                                            </label>
                                        </div>
                                        <div class="row_field col field_wrap_{$field.type}">
                                            {if ($field.type == 'select') }
                                                <select name="{$field_key}"
                                                        id="def_{$i}" {if (!empty($field.readonly)) }{$readonly_attr}{/if} {if (!empty($field.disabled)) }{$disabled_attr}{/if} >
                                                    {if (!empty($field.options))}
                                                        {foreach from=$field.options key="option_key" item="option" }
                                                            <option value="{$option_key}"  {if (!empty($aData.$field_key) && $aData.$field_key == $option_key) } selected {/if} >{$option}</option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            {/if}
                                            {if ($field.type == 'textarea') }
                                                <textarea rows="4" {if (!empty($field.readonly))}{$readonly_attr} class="readonly"{/if} {if (!empty($field.disabled))}{$disabled_attr}{/if}  name="{$field_key}" id="def_{$i}" />{if isset($aData.$field_key) }{$aData.$field_key}{/if}</textarea>
                                            {/if}
                                            {if ($field.type == 'text') }
                                                <input type="text" {if (!empty($field.readonly))}{$readonly_attr}{/if} {if (!empty($field.disabled))}{$disabled_attr}{/if}
                                                       name="{$field_key}" id="def_{$i}"
                                                       class="text {if (!empty($field.readonly))}readonly{/if}"
                                                       value="{if isset($aData.$field_key) }{$aData.$field_key}{/if}"/>
                                            {/if}
                                            {if ($field.type == 'checkbox') }
                                                <input type="checkbox" {if (!empty($field.readonly))}{$readonly_attr}{/if} {if (!empty($field.disabled))}{$disabled_attr}{/if}
                                                       name="{$field_key}" id="def_{$i}"
                                                       class="checkbox {if (!empty($field.readonly))}readonly{/if}"
                                                       value="1" {if !empty($aData.$field_key) } checked="checked" {/if}
                                                       autocomplete="off"/>
                                                <label for="def_{$i}">&nbsp</label>
                                            {/if}
                                            {if ($field.type == 'image') }
                                                {if (empty($field.readonly) && empty($field.disabled)) }
                                                    <input type="file" name="{$field_key}" id="def_{$i}" class="image"/>
                                                    
                                                {/if}
                                                <div class="image_holder">
                                                    {if (!empty($aData.$field_key)) }
                                                        <img class="image_preview"
                                                             src="/{$field.image_src}{$aData.$field_key}">
                                                        <button value="" type="button" class="btn_cancel image_remove"
                                                                onclick="fields_remove_image(this,{$nId},'{$field_key}')">
                                                            {translate code="form_delete" text="Удалить"}
                                                        </button>
                                                    {else}

                                                    {/if}
                                                </div>
                                            {/if}
                                            {if ($field.type == 'wysiwyg') }
                                                <textarea id="wysiwyg_{$field_key}" id="def_{$i}"
                                                          name="{$field_key}">{if isset($aData.$field_key) }{$aData.$field_key}{/if}</textarea>
                                            {literal}
                                                <script type="text/javascript">
                                                /*
                                                    tinymce.init({
                                                        selector: "#wysiwyg_{/literal}{$field_key}{literal}"
                                                    });*/
                                                    //var ckeditor_{/literal}{$field_key}{literal} = CKEDITOR.replace('wysiwyg_{/literal}{$field_key}{literal}');
                                                    //CKFinder.setupCKEditor( editor );
                                                    //editor.setData( '<p>Just click the <b>Image</b> or <b>Link</b> button, and then <b>&quot;Browse Server&quot;</b>.</p>' );
                                                    //CKFinder.setupCKEditor(ckeditor_{/literal}{$field_key}{literal}, '/ckfinder/');
                                                </script>
                                                <script>
                                                    tinymce.init({ 
                                                        selector:'textarea#def_{$i}',
                                                        menubar: "insert",
                                                        plugins: {"image", "autolink", "link"},
                                                    });
                                                </script>
                                            {/literal}
                                            {/if}
                                        </div>
                                    </div>
                                {/if}
                            {/if}
                        {/foreach}

                        {if (!empty($form.buttons))}
                            <div class="form_row box-btn form_actions">
                                {include file="sys/messages.tpl"}
                                <button type="submit" name="submit" value="1" class="btn success">
                                    <i class="fa fa-floppy-o"></i>{translate code="form_save" text="{$form.success_text}"}
                                </button>
                            </div>
                        {/if}
                    </div>
                </div>
            </form>
        </div>
    {/nocache}
    <script type="text/javascript">
        {literal}
        function fields_remove_image(el, id, field_name) {
            $.ajax({
                type: 'POST',
                url: '{/literal}{$aConf.base_url}{$aConf.active_module}/remove_file/{literal}' + id + '/' + field_name,
                dataType: 'json',
                success: function (data) {
                    $(el).parent('.image_holder').hide();
                },
                data: {'ajax': 1},
                async: false
            });
        }
        {/literal}
    </script>
{/strip}