<{$field.tag_name}
    id="def_{$i}"
    {foreach from=$field.attributes key=key item=value}
        {if $value !== false}{$key}="{$value}"{/if}
    {/foreach}
>{if isset($aData.$field_key)}{$aData.$field_key}{else}{$field.value}{/if}</{$field.tag_name}>