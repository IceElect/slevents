{if $field.new_value}
    {$value = $field.new_value}
{else}
    {if isset($aData.$field_key)}
        {$value = $aData.$field_key}
    {else}
        {$value = $field.value}
    {/if}
{/if}

<{$field.tag_name}
    id="def_{$i}"
    {foreach from=$field.attributes key=key item=value}
        {if $key !== 'value'}
            {if $value !== false}{$key}="{$value}"{/if}
        {else}
            
        {/if}
    {/foreach}
>

{literal}
<script>
    jQuery.datetimepicker.setLocale('ru');

    jQuery('#{/literal}def_{$i}{literal}').datetimepicker({
     // i18n:{
     //  de:{
     //   months:[
     //    'Januar','Februar','März','April',
     //    'Mai','Juni','Juli','August',
     //    'September','Oktober','November','Dezember',
     //   ],
     //   dayOfWeek:[
     //    "So.", "Mo", "Di", "Mi", 
     //    "Do", "Fr", "Sa.",
     //   ]
     //  }
     // },
     step: 30,
     value: '{/literal}{$value}{literal}',
     timepicker: true,
     formatDate: 'Y-m-d H:i:s',
     format: 'Y-m-d H:i:s',
     onSelectDate: function(ct,$i){
        console.log(ct);
        //$i[0].value(ct);
     }
    });
</script>
{/literal}