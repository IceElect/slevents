{if (!empty($field.options))}
    {foreach from=$field.options key="option_key" item="option" }
        <input type="radio" name="{$field.field}" value="{$option_key}" id="{$field.field}_{$option_key}" {if !empty($aData.$field_key)}
                                                                                    {if $aData.$field_key == $option_key} checked {/if}
                                                                                {else}
                                                                                    {if $field.value == $option_key} checked {/if}
                                                                                {/if}>
        <label for="{$field.field}_{$option_key}">{$option}</label>
    {/foreach}
{/if}