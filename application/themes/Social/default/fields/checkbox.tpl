<{$field.tag_name}
    id="def_{$i}"
    {foreach from=$field.attributes key=key item=value}
        {if $key !== 'value'}
            {if $value !== false}{$key}="{$value}"{/if}
        {else}
            {if $field.new_value}
                {$field.new_value}
            {else}
                {$key}="{if isset($aData.$field_key)}{$aData.$field_key}{else}{$field.value}{/if}"
            {/if}
        {/if}
    {/foreach}
>
<label for="def_{$i}">&nbsp;</label>