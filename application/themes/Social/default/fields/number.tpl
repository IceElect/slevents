{if (!empty($field.prefix) || !empty($field.suffix))}
    <div class="input-group">
{/if}
{if (!empty($field.prefix))}
      <span class="input-group-addon" id="basic-addon1">{$field.prefix}</span>
{/if}
                
<{$field.tag_name}
    id="def_{$i}"
    {foreach from=$field.attributes key=key item=value}
        {if $key !== 'value'}
            {if $value !== false}{$key}="{$value}"{/if}
        {else}
            {if $field.new_value}
                {$field.new_value}
            {else}
                {$key}="{if isset($aData.$field_key)}{$aData.$field_key}{else}{$field.value}{/if}"
            {/if}
        {/if}
    {/foreach}
>

{strip}
{if (!empty($field.suffix))}
      <span class="input-group-addon" id="basic-addon1">{$field.suffix}</span>
{/if}
{if (!empty($field.prefix) || !empty($field.suffix))}
    </div>
{/if}
{/strip}