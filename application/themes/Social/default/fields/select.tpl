<{$field.tag_name}
    id="def_{$i}"
    {foreach from=$field.attributes key=key item=value}
        {if $key !== 'value'}
            {if $value !== false}{$key}="{$value}"{/if}
        {else}
            {if $field.new_value}
                {$field.new_value}
            {else}
                {$key}="{if isset($aData.$field_key)}{$aData.$field_key}{else}{$field.value}{/if}"
            {/if}
        {/if}
    {/foreach}
>
{if (!empty($field.options))}
    {foreach from=$field.options key="option_key" item="option" }
        <option value="{$option_key}" {if !empty($aData.$field_key)}
                                        {if $aData.$field_key == $option_key} selected {/if}
                                      {else}
                                        {if $field.value == $option_key} selected {/if}
                                      {/if}>{$option}</option>
    {/foreach}
{/if}
</{$field.tag_name}>