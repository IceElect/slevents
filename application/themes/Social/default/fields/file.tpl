<{$field.tag_name}
    id="def_{$i}"
    {foreach from=$field.attributes key=key item=value}
        {if $key !== 'value'}
            {if $value !== false}{$key}="{$value}"{/if}
        {else}
            {if $field.new_value}
                {$field.new_value}
            {else}
                {$key}="{if isset($aData.$field_key)}{$aData.$field_key}{else}{$field.value}{/if}"
            {/if}
        {/if}
    {/foreach}
>
{if $field.multiple}
    {if isset($aData.screenshots)}
    <script>
        $("input#def_{$i}").fileinput({literal}{{/literal}
            initialPreview: {$aData.screenshots.preview|@json_encode},
            initialPreviewAsData: true,
            overwriteInitial: false,
            deleteUrl: '{$aConf.base_url}{$aConf.active_module}/remove_file/{$nId}',
            initialPreviewConfig: {$aData.screenshots.config|@json_encode},
            initialCaption: "{$field.title}",
            maxFilePreviewSize: 10240
        {literal}}{/literal});
        $('input#def_{$i}').on('filesorted', function(event, params) {literal}{{/literal}
            var sort = [];
            $( '.file-preview-thumbnails > .file-preview-frame' ).each(function( index ) {literal}{{/literal}
                sort[index] = $(this).attr('data-uri');
            {literal}}{/literal});
            $.post('', {literal}{{/literal}sort: sort{literal}}{/literal}, function(data){literal}{{/literal}console.log(data);{literal}}{/literal})
        {literal}}{/literal});
    </script>
    {/if}
{/if}