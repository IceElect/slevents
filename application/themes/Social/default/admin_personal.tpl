<div class="row" style="margin-bottom: 10px">
    <button class="button" onclick="edit_personal(0, {$nId}, event)">Добавить персонал</button>
</div>
<table class="table" style="margin: 0">
    <thead>
        <tr>
            <th>ID</th>
            <th>Имя</th>
            <th>Роль</th>
            <th>Билет</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        {foreach from=$personal key=key item=person}
            <tr data-person-id="{$person->id}">
                <td>{$person->user_id}</td>
                <td>{$person->fname} {$person->lname}</td>
                <td>{$person->role}</td>
                <td><a href="#"><i class="fa fa-link"></i></a></td>
                <td>
                    {*<a href="#"><i class="md-icon" onclick="edit_personal({$person->id}, {$nId}, event)">edit</i></a>*}
                    <a href="#"><i class="md-icon" onclick="del_personal({$person->id}, {$nId}, event)">delete</i></a>
                </td>
            </tr>
        {/foreach}
    </tbody>
</table>