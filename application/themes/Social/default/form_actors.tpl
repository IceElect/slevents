<div class="form-group">
    <div class="col-sm-3 control-label">
        <label for="actors">Актеры</label>
    </div>
    <div class="col-sm-9 field_wrap_text">
    	<ul class="actors">
            {if (!empty($aData.actors))}
                {foreach $aData.actors as $key => $actor}
                    <li>{$actor}</li>
                {/foreach}
            {/if}
        </ul>
        <p class="form-text text-muted">
            Выберите актеров, учавствовавших в видео
        </p>
        <script>
            {literal}
                $('.actors').tagit({
                    availableTags: {/literal}{$actors}{literal},
                    fieldName: 'actor[]',
                    tagLimit: 10,
                    allowSpaces: true,
                    onTagLimitExceeded: function(){
                        console.log(jQuery(this).parent());
                        jQuery(this).parent().children('.error').show();
                    },
                    onTagRemoved: function(){
                        jQuery(this).parent().children('.error').hide();
                    }
                });
            {/literal}
        </script>
    </div>
</div>