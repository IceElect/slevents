<div class="row" style="margin-bottom: 10px">
    <button class="button" onclick="edit_visitor(0, {$nId}, event)">Добавить посетителя</button>
</div>
<table class="table" style="margin: 0">
    <thead>
        <tr>
            <th>#</th>
            <th>Пользователь</th>
            <th>Тип билета</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        {foreach from=$visitors key=key item=person}
            <tr data-person-id="{$person->id}">
                <td>{$key+1}</td>
                <td>{$person->user_id} - {$person->fname} {$person->lname}</td>
                <td>{$person->ticket_type}</td>
                <td><a href="#"><i class="fa fa-link"></i></a></td>
                <td>
                    {*<a href="#"><i class="md-icon" onclick="edit_visitor({$person->id}, {$nId}, event)">edit</i></a>*}
                    <a href="#"><i class="md-icon" onclick="del_visitor({$person->id}, {$nId}, event)">delete</i></a>
                </td>
            </tr>
        {/foreach}
    </tbody>
</table>