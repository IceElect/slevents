<div class="nest">
    <div class="body-nest">
        <div class="col-xs-12 add-block-form form-inline">
            <b class="form-control-feedback error-holder" hidden>{translate code="error_empty_block_id" text="Введитите id элемента"}</b>
            <div class="row form-actions btn-group pull-right">
                <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-plus"></i><span>Добавить элемент</span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a onclick="blocks.add('video_list', '{$nId}', '{$aConf.active_module}' );">Список видео</a></li>
                </ul>
            </div>
            <input type="text" id="block_id_input" class="form-control pull-right" placeholder="ID элемента">
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="blocks">
    <ul class="sortable ui-sortable">
    {foreach from=$blocks item=$block}
        {include file="admin/nest_form.tpl" block_id=$block->block_id title=$block->title type=$block->type aFields=$block->form aData=$block->options}
    {/foreach}
    </ul>
</div>
<script>
    {literal}
    $('.sortable').sortable({
        handle: '.title-alt',
        items: 'li',
        toleranceElement: '> div',
        placeholder: 'item_placeholder',
        listType: 'ul',
        attribute: 'data-linkid'
    });
    {/literal}
</script>
<!--
<div class="nest">
    <div class="title-alt">
        <h6>Категории</h6>
        <div class="titleClose">
            <a class="gone" href="#elementClose">
                <span class="entypo-cancel"></span>
            </a>
        </div>
        <div class="titleToggle">
            <a class="nav-toggle-alt" href="#element">
                <span class="entypo-up-open"></span>
            </a>
        </div>
    </div>
    <div class="body-nest">
        <section class="col-xs-12">
        </section>
    </div>
</div>
-->