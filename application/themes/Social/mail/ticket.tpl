<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body style="margin: 0px;padding: 0px;font-family: sans-serif;font-size: 14px">
    
    <table width="100%">
        <tr>
            <td></td>
            <td width="400px">
                <div style="border-radius: 2px;border:1px solid #ccc;margin-top:20px">
                    <div style="background: #e54a55;background: #f5f8fa;padding: 10px;font-size: 15px;line-height: 37px;color: #333;border-bottom:1px solid #ccc;text-align: center;">
                        Билет успешно куплен
                    </div>
                    <div style="padding: 10px;">
                        <p style="text-align: center;margin:7px 0px"><b style="display: inline-block;padding-right: 7px;">Место</b> {$ticket->location}</p>
                        <p style="text-align: center;margin:7px 0px"><b style="display: inline-block;padding-right: 7px;">Мероприятие</b> {$ticket->name}</p>
                        <p style="text-align: center;margin:7px 0px"><b style="display: inline-block;padding-right: 7px;">Тип билета</b> {$ticket->ticket_type}</p>
                        <p style="text-align: center;margin:7px 0px"><b style="display: inline-block;padding-right: 7px;">Стоимость</b> {$ticket->price} Р</p>
                        <br>
                        <p style="">
                            <span style="text-align: center;float: left">{$ticket->fname} {$ticket->lname}</span>
                            <span style="text-align: center;float: right">{$ticket->date}</span>
                            <div style="clear: both;display: table"></div>
                        </p>
                    </div>
                    <div style="padding: 10px;text-align: center;border-top: 1px solid #ccc;">
                        <img src="{$base_url}{$ticket->qr}">
                    </div>
                    <div style="padding: 10px;text-align: center;border-top: 1px solid #ccc;overflow: hidden">
                        <a href="{$base_url}profile" style="background:#d2383b;padding: 7px 16px 8px;margin:0;font-size:12.5px;display:inline-block;zoom:1;cursor:pointer;white-space:nowrap;outline:none;vertical-align:top;text-align:center;text-decoration:none;color:#fff;border:0;border-radius:2px;box-sizing:border-box;">Перейти в профиль</a>
                    </div>
                </div>
            </td>
            <td></td>
        </tr>
    </table>

</body>
</html>