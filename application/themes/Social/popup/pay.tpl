<div class="popup_bg"></div>
<div class="popup block" data-id="login" width="600px">
    <div class="popup_header_holder">
        <div class="popup_header module_title">Оплатить билет <button class="md-icon close" onclick="popup.hide('avatar_upload');">close</button></div>
    </div>
    <div class="popup_content module_content">
        <p><b>Внимание!</b> Ваш билет забронирован и ожидает оплаты, если вы не произведете оплату в течение 20 минут, то билет будет снят с бронирования.</p>

        <div class="gift-block">
            <div class="gift-list">
                {foreach from=$Gifts item=gifts_item key=item_id}
                    <div>
                        <input name="GiftType" id="Type{$item_id}" type="radio" value="{$item_id}" data-name="{$gifts_item.name}"> <label for="Type{$item_id}">{$gifts_item.name}</label>
                    </div>
                {/foreach}
            </div>

            <div class="gift-attributes">
                {foreach from=$Gifts item=gifts_item key=gift_item_id}
                <div class="gift-item form-horizontal" data-gift="{$gift_item_id}" data-gift-name="{$gifts_item.name}">
                    {foreach from=$gifts_item item=attribute key=item_id}
                    {if is_array($attribute)}
                        <div class="form-row clearfix">
                            <div class="form-label">
                                <label class="LabelSelect{$gift_item_id}" for="Select{$item_id}">{$attribute.attribute}</label>
                            </div>
                            <div class="form-input-wrap">
                                <select ata-attribute-id="{$item_id}" data-attribute-name="{$attribute.attribute}" onchange="selectParam({$gift_item_id})" class="field gift-attribute attribute{$gift_item_id}" id="Select{$item_id}" name="Select{$item_id}">
                                    <option disabled value="0" selected>Выберите {$attribute.attribute}</option>
                                    {foreach from=$attribute item=values key=item_id}
                                        {if is_array($values)}
                                            <option value="{$item_id}">{$values.value}</option>
                                        {/if}
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                    {/if}
                    {/foreach}
                </div>
                {/foreach}
            </div>
        </div>

        <div id="pay-actions" {if $Gifts|@count > 0}class="hidden"{/if} style="margin-top: 10px">
            <a style="float: left;margin-right: 10px" href="{$url}" class="button" id="pay-button">Оплатить билет</a>
            <div>
                <span onclick="ShowPromo()" style="line-height: 30px" id="promoCode">Есть промокод?</span>
            </div>
            <div  class="hidden" id="promo">
                <div class="promo-form fl-l" style="display: inline-block">
                    <input type="text" name="promo" placeholder="Промо" class="field" style="display: inline-block; width: auto; margin-left: 8px; padding: 6px 12px 7px 12px;">
                    <button onclick="promo('{$tid}');" class="button gray" style="display: inline-block; width: auto; margin-left: 8px">Применить</button>
                </div>

                <p class="promo-error hidden"></p>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <script>
        var Gifts = {$Gifts|json_encode}
        var tid = {$tid}
        {literal}
        function promo(oid){
            var button = $("#pay-button");
            var promo = $("[name='promo']").val();
            jQuery.ajax({
                url: '/ajax/ticket/button/'+oid,
                type: 'POST',
                data: {promo: promo},
                beforeSend: function(){
                    jQuery("body").addClass('loading');
                    //button.html('<i class="fa fa-circle-o-notch fa-spin"></i>');
                },
                success: function(data){
                    jQuery(".promo-error").hide();
                    jQuery(".promo-error").html("");
                    var data = eval('('+data+')');
                    if(data.response){
                        button.replaceWith(data.html);
                        jQuery(".promo-form").hide();
                        jQuery(".promo-error").show();
                        jQuery(".promo-error").html(data.text);
                    }
                    if('error' in data){
                        jQuery(".promo-error").show();
                        jQuery(".promo-error").html(data.error);
                    }
                }
            });
        }
        function ShowPromo() {
            jQuery("#promo").show()
            jQuery("#promoCode").hide()
        }
        $("input[name=GiftType]").change(function (ev) {
            var Select = $("input[name=GiftType]:checked").val();

            $(".gift-item").hide();
            $(".gift-item[data-gift="+Select+"]").show();

            if( $(".gift-item select option:disabled:selected").length > 0 ) {
                $("#pay-actions").hide();
            }

        });

        function selectParam(id) {
            var items = 0;
            /*
            var data ={
                'gifts':[
                    {
                        gift_id: $("input[name=GiftType]:checked").val(),
                        gift_name: $("input[name=GiftType]:checked").data('name'),
                        attributes:[]
                    }
                ]
            }
            var attr = $(".attribute"+id)

            $.each( attr, function( key, value ) {
                if(value.value>0) items++
                data['gifts'][0]['attributes'].push({
                    'name': $(".LabelSelect"+id)[key].innerText,
                    'value': value.value,
                    'value_name': attr.find(":selected")[key].innerText
                })
            });
            */

            var data = {gifts: []};

            $.each($('.gift-item'), function(key, value){
                var gift = {};
                gift.id = $(this).data('gift');
                gift.name = $(this).data('gift-name');
                gift.attributes = [];
                $.each($(this).find('.gift-attribute'), function(key, attribute){
                    attribute = {};
                    attribute.id = $(this).data('attribute-id');
                    attribute.name = $(this).data('attribute-name');
                    attribute.value = $(this).val();
                    attribute.value_name = $(this).find(':selected').html();
                    gift.attributes.push(attribute);
                })

                data.gifts.push(gift);
            })

            console.log(data);

            if( $(".gift-item select option:disabled:selected").length == 0 ) {
                $("#pay-actions").show()
                $.post("/ajax/ticket/select_gift/"+tid, {selected_param: data});
            }
        }
        {/literal}
    </script>
    <style>
        .gift-block{
            width: 100%;
            display: flex;
        }
        .gift-attributes{
            flex: 1;
        }
        .gift-item{
            display: none;
        }
    </style>
</div>