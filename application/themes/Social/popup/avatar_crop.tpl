<div class="popup_bg"></div>
<div class="popup block" data-id="avatar_crop">
    <div class="popup_header_holder">
        <div class="popup_header module_title">

            Выберите миниатюру <button class="md-icon close" onclick="popup.hide('avatar_crop');">close</button>
        </div>
    </div>
    <div class="popup_content module_content">

        <div class="clearfix"></div>

        <div class="file-upload avatar-crop">
            <img src="/ajax/photo/view/{$oUser->avatar}/740" id="avatar-crop-target" alt="{$oUser->fname} {$oUser->lname}">
        </div>
        <div class="file-upload">
            <button id="submit" type="submit" class="button" onclick="photo.avatar_crop();">Применить</button>
        </div>
        
        <div class="load-info">
            <span id="message"></span>
        </div>
        <div class="clearfix"></div>
    </div>
</div>