<div class="popup_bg"></div>
<div class="popup block" data-id="photo_upload">
    <div class="popup_header_holder">
        <div class="popup_header module_title">Выберите фото <a class="fl_r close" onclick="popup.hide('avatar_upload');"><i class="md-icon">close</i></a></div>
    </div>
    <div class="popup_content module_content">
        <b>Ограничения</b>
        <ul>
            <li>Файл должен иметь размно не более 20 МБ и расширения PNG, JPEG или GIF</li>
        </ul>
        <form action="/upload/photo" method="POST" enctype="multipart/form-data" data-type="upload">
        <center>
            <input type="hidden" name="album" value="{$album}">
            <div class="file-upload">
                <input id="file" type="file" name="file" style="display: none;">
                <label for="file" class="btn btn_accept big_width">Загрузить файл</label>
            </div>
            
            <div class="load_info">
                <span id="message"></span>
            </div>
        </center>
        <br>
        <div class="fl_r">
            <button onclick="popup.hide('photo_upload');" type="button" class="btn">Отмена</button>
            <input type="submit" id="upload" class="btn btn_accept" value="Загрузить">
        </div>
        </form>
        <div style="clear: both;"></div>
    </div>
</div>
<!--<script src="/views/Social/js/drop_load.js"></script>-->