<div class="popup_bg"></div>
<div class="popup block" data-id="login" width="600px">
    <div class="popup_header_holder">
        <div class="popup_header module_title">Войти в аккаунт <button class="md-icon close" onclick="popup.hide('avatar_upload');">close</button></div>
    </div>
    <div class="popup_content module_content">
		<form method="post" action="/user/login" class="form" data-act="login" data-type="ajax">
			<input type="hidden" name="act" value="login">
			<fieldset>
				<label for="login">Логин/E-mail</label>
				<input type="text" name="user_email" id="login" class="field">
			</fieldset>
			<fieldset>
				<label for="pass">Пароль</label>
				<input type="password" name="user_password" id="pass" class="field">
			</fieldset>
			<fieldset>
				<button type="submit" name="submit" value="1" class="button">Войти</button>
				<a class="button gray" href="{$hosted_url}?destination">Регистрация</a>
                <a href="https://oauth.vk.com/authorize?client_id=6397351&display=popup&scope=friends,email,status,stats&response_type=code&state={$base_url}&redirect_uri={$base_url}users/vk_auth">Вконтакте</a>
				<span class="form_result" style="line-height: 28px;margin-left: 3px"></span>
			</fieldset>
		</form>
        <div class="clearfix"></div>
    </div>
</div>