<div class="popup_bg"></div>
<div class="popup block" data-id="ticket" width="600px">
    <div class="popup_header_holder">
        <div class="popup_header module_title">{$pageTextTitle} <button class="md-icon close" onclick="popup.hide('ticket');">close</button></div>
    </div>
    <div class="popup_content module_content">
        <form method="post" action="/ajax/ticket/personal" class="form form-horizontal" data-act="personal" data-type="ajax">
            {*<input type="hidden" name="id" value="{$id}">*}
            <input type="hidden" name="event_id" value="{$event_id}">

            <div class="form-row clearfix">
                <div class="form-label">
                    <label for="role">Роль</label>
                </div>
                <div class="form-input-wrap">
                    <input type="text" name="role" id="role" class="field">
                </div>
            </div>
            <div class="form-row clearfix">
                <div class="form-label">
                    <label for="user_id">ID пользователя</label>
                </div>
                <div class="form-input-wrap">
                    <input type="text" name="user_id" id="user_id" class="field">
                </div>
            </div>

            <div class="form-row clearfix">
                <div class="form-label"></div>
                <div class="form-input-wrap">
                    <button type="submit" name="submit" value="1" class="button">{if $id}Сохранить{else}Создать{/if}</button>
                    <button type="button" class="button gray" onclick="popup.hide('personal');">Отмена</button>
                    <span class="form_result" style="line-height: 28px;margin-left: 3px"></span>
                </div>
            </div>

        </form>
        <div class="clearfix"></div>
    </div>
</div>