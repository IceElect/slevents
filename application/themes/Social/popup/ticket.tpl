<div class="popup_bg"></div>
<div class="popup block" data-id="ticket" width="600px">
    <div class="popup_header_holder">
        <div class="popup_header module_title">{$pageTextTitle} <button class="md-icon close" onclick="popup.hide('ticket');">close</button></div>
    </div>
    <div class="popup_content module_content">
        <form method="post" action="/ajax/ticket/edit" class="form form-horizontal" data-act="ticket" data-type="ajax">
            <input type="hidden" name="id" value="{$id}">
            <input type="hidden" name="event_id" value="{$event_id}">

            <div class="form-row clearfix">
                <div class="form-label">
                    <label for="name">Название</label>
                </div>
                <div class="form-input-wrap">
                    <input type="text" name="name" id="name" class="field" value="{$ticket->name}">
                </div>
            </div>
            <div class="form-row clearfix">
                <div class="form-label">
                    <label for="price">Стоимость</label>
                </div>
                <div class="form-input-wrap">
                    <input type="text" name="price" id="price" class="field" value="{$ticket->price}">
                </div>
            </div>
            <div class="form-row clearfix">
                <div class="form-label">
                    <label for="count">Количество</label>
                </div>
                <div class="form-input-wrap">
                    <input type="text" name="count" id="count" class="field" value="{$ticket->count}">
                </div>
            </div>
            <div class="form-row clearfix">
                <div class="form-label">
                    <label for="description">Описание</label>
                </div>
                <div class="form-input-wrap">
                    <textarea name="description" id="description">{$ticket->description}</textarea>
                </div>
            </div>

            <div class="form-row clearfix">
                <div class="form-label">
                    <label for="count">Дни</label>
                </div>
                <div class="form-input-wrap">
                    {for $i=1 to $ticket->days_count}
                        <div style="display: flex;">
                            <input type="checkbox" name="days[]" value="{$i}" id="days_{$i}" {if in_array($i, $ticket->days)}checked{/if} style="width: auto;">
                            <label for="days_{$i}">{$i} день</label>
                        </div>
                    {/for}
                </div>
            </div>

            <div class="form-row clearfix">
                <div class="form-label"></div>
                <div class="form-input-wrap">
                    <button type="submit" name="submit" value="1" class="button">{if $id}Сохранить{else}Создать{/if}</button>
                    <button type="button" class="button gray" onclick="popup.hide('ticket');">Отмена</button>
                    <span class="form_result" style="line-height: 28px;margin-left: 3px"></span>
                </div>
            </div>

        </form>
        <div class="clearfix"></div>
    </div>
</div>