<div class="popup_bg"></div>
<div class="popup block" data-id="ticket" width="600px">
    <div class="popup_header_holder">
        <div class="popup_header module_title">{$pageTextTitle} <button class="md-icon close" onclick="popup.hide('ticket');">close</button></div>
    </div>
    <div class="popup_content module_content">
        <form method="post" action="/ajax/ticket/visitor" class="form form-horizontal" data-act="visitor" data-type="ajax">
            {*<input type="hidden" name="id" value="{$id}">*}
            <input type="hidden" name="event_id" value="{$event_id}">

            <div class="form-row clearfix">
                <div class="form-label">
                    <label for="ticket_id">Тип билета</label>
                </div>
                <div class="form-input-wrap">
                    <select name="ticket_id" id="ticket_id" class="field">
                        {foreach from=$tickets item="ticket"}
                            <option value="{$ticket->id}">{$ticket->name}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-row clearfix">
                <div class="form-label">
                    <label for="user_id">ID пользователя</label>
                </div>
                <div class="form-input-wrap">
                    <input type="text" name="user_id" id="user_id" class="field">
                </div>
            </div>

            <div class="form-row clearfix">
                <div class="form-label"></div>
                <div class="form-input-wrap">
                    <button type="submit" name="submit" value="1" class="button">{if $id}Сохранить{else}Создать{/if}</button>
                    <button type="button" class="button gray" onclick="popup.hide('visitor');">Отмена</button>
                    <span class="form_result" style="line-height: 28px;margin-left: 3px"></span>
                </div>
            </div>

        </form>
        <div class="clearfix"></div>
    </div>
</div>