function popup(){
    var self = {};
    self.bind = false;

    self.init = function(){
        if(get('photo') !== null){
            self.show('photo/'+get('photo'));
            if($('body').width()<=480){
                $(".app-bar").animate({ bottom: '-44px' }, 200);
            }
            self.bind = $(document).on('click', '.popup_bg', function(e){
                $(this).off('click', false);
                photo.photo(get('photo'), false, e);

                return false;
            })
        } else if(get('album') !== null){
            self.show('album', {album: get('album')});
        } else if(get('albums') !== null){
            self.show('albums', {user: get('albums')});
        } else {
            if($('body').width()<=480){
                $(".app-bar").animate({ bottom: '0px' }, 200);
            }
            self.hide();
        }
    }

    self.show = function(id){
        if($(".popup[data-id='"+id+"']").length > 0){
            $("body").addClass('with-popup');
            $(".popup_layer").addClass('show');

            $(".popup[data-id='"+id+"']").addClass('show');
        }else{
            $.post('/ajax/popup/'+id, function(data){
                data = eval('('+data+')');
                if(data.response){
                    $(".popup_layer").html(data.data);
                    $("body").addClass('with-popup');
                    $(".popup_layer").addClass('show');
                    $(".popup[data-id='"+id+"']").addClass('show');
                }
            })
        }

        self.bind = $(document).on('click', '.popup_bg', function(e){
            $(this).off('click', false);
            self.hide(id);

            return false;
        })
    }
    self.show = function(id, params){
        if($(".popup[data-id='"+id+"']").length > 0){
            $("body").addClass('with-popup');
            $(".popup_layer").addClass('show');

            $(".popup[data-id='"+id+"']").addClass('show');
        }else{
            $.post('/ajax/popup/'+id, params, function(data){
                data = eval('('+data+')');
                if(data.response){
                    $(".popup_layer").html(data.data);
                    $("body").addClass('with-popup');
                    $(".popup_layer").addClass('show');
                    $(".popup[data-id='"+id+"']").addClass('show');
                }
            })
        }

        self.bind = $(document).on('click', '.popup_bg', function(e){
            $(this).off('click', false);
            self.hide(id);

            return false;
        })
    }

    self.show = function(id, params, callback){
        if($(".popup[data-id='"+id+"']").length > 0){
            $("body").addClass('with-popup');
            $(".popup_layer").addClass('show');

            $(".popup[data-id='"+id+"']").addClass('show');
        }else{
            $.post('/ajax/popup/'+id, params, function(data){
                data = eval('('+data+')');
                if(data.response){
                    $(".popup_layer").html(data.data);
                    $("body").addClass('with-popup');
                    $(".popup_layer").addClass('show');
                    $(".popup[data-id='"+id+"']").addClass('show');

                    callback(data);
                }
            })
        }

        self.bind = $(document).on('click', '.popup_bg', function(e){
            $(this).off('click', false);
            self.hide(id);

            return false;
        })
    }

    var e = false;

    self.showSelect = function(id, data, callback){
        $(document).unbind(".popup[data-id='"+id+"_select'] a.select");
        $(".popup[data-id='"+id+"_select'] a.select").off('click');
        if($(".popup[data-id='"+id+"']").length > 10){
            $("body").addClass('with-popup');
            $(".popup_layer").addClass('show');

            $(".popup[data-id='"+id+"']").addClass('show');
        }else{
            $.post('/ajax/popup/'+id, data, function(data){
                data = eval('('+data+')');

                if(data.data){
                    $(".popup_layer").html(data.data);
                    $("body").addClass('with-popup');
                    $(".popup_layer").addClass('show');
                    $(".popup[data-id='"+id+"_select']").addClass('show');

                    e = $(".popup[data-id='"+id+"_select'] a.select").bind('click', function(){callback($(this).attr('data-id'));});
                }
            })
        }
    }

    self.hide = function(id){
        $("body").removeClass('with-popup');
        $(".popup_layer").removeClass('show');

        $(".popup[data-id='"+id+"']").removeClass('show');
    }
    self.hide = function(){
        $("body").removeClass('with-popup');
        $(".popup_layer").removeClass('show');

        $(".popup").removeClass('show');
    }

    self.scroll = function(){
        if($(".popup_layer.show").scrollTop() > 65){
            $(".popup .popup_header").addClass('header_fixed');
        }else{
            $(".popup .popup_header").removeClass('header_fixed');
        }
    }

    self.remove = function(id){
        self.hide(id);
        $(document).unbind(".popup[data-id='"+id+"_select'] a.select");
        $(".popup[data-id='"+id+"']").replaceWith("");
    }

    return self;
}
var popup = new popup();
popup.init();