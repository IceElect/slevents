function error(error_text){
    alert(error_text);
}

jQuery(document).on('submit', 'form[data-type=ajax]', function(e){
    e.preventDefault();
    var $that = jQuery(this),
    formData = new FormData($that.get(0));
    jQuery.ajax({
      url: $that.attr('action'),
      type: $that.attr('method'),
      contentType: false,
      processData: false,
      data: formData,
      dataType: 'html',
      beforeSend: function(){
          jQuery("body").addClass('loading');
          $that.find('.form_result').html('<i class="fa fa-spin fa-refresh"></i>');
      },
      success: function(data){
        jQuery("body").removeClass('loading');
        jQuery(".error").html("");
        var data = eval('('+data+')');
        if(data.response){
            if($that.attr('data-act') == 'ticket'){
                $("#tickets").html(data.table_html);
                popup.hide('ticket');
                return false;
            }
            if($that.attr('data-act') == 'personal'){
                $("#personal").html(data.table_html);
                popup.hide('personal');
                return false;
            }
            if($that.attr('data-act') == 'visitor'){
                $("#visitors").html(data.table_html);
                popup.hide('visitor');
                return false;
            }
            if('redir' in data){
                window.location.href = data.redir;
                return false;
            }
            if('refresh' in data){
                location.reload();
            }
            if('text' in data){
                if($that.attr('data-act') == 'login')
                  window.location.href = data.text;
                $that.find('.form_result').html(data.text);
            }
        }else{
            if($that.attr('data-act') == 'login' || $that.attr('data-act') == 'product_add'){
              jQuery(".form_result").html(data.error);
            }else{
              error(data.error);
            }
        }
      }
    });
});