<!DOCTYPE html>
<html lang="en">
<head {if isset($post)}prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#"{/if}>
	<title>{$pageTitle}</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="canonical" href="{$base_url}">

	{include file="sys/meta.tpl"}

	
	<!--<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>-->
</head>
<body>
	{include file="sys/js_css_inc.tpl" place="header"}
	<style>
    	body{
    		abackground: #fff;
    	}
    	.popup_layer{
			width: calc( 100% - 68px );
		}
    	.page-content{
    		width: calc( 100% - 68px );
    	}
    	.left-wrap .app-bar{
    		background: #883024;
    	}
    	.main-col{
    		max-width: 1000px;
    	}
    	@media screen and (max-width: 1024px){
    		.main-col{
    			max-width: 100%;
    		}
    		.main-col .sidebar{
    			width: 100%;
    		}
    	}
    	@media screen and (max-width: 768px){
    		.page-content{
    			width: 100%;
    		}
			.thumbs{
				width: 100%;
			}
			.thumbs .thumb{
				display: block;
			}
			.thumbs .thumb .image-holder{
				width: 100%;
			}
			.thumb .image-holder .time{
				left: 0px;
			}
			.thumb .content-holder .info{
				position: relative;
				bottom: auto;
			}
    	}
		div.send-form-area{
			background: #f4f4f4;
			border: none;
		}

		.send-post-form .send-form-area{
			padding-right: 45px;
		}

		.send-post-form .avatar{
			padding-left: 0px;
		}
    </style>

	{include file="sys/js_conf_inc.tpl"}
	<!--
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="/{$path}scripts/jquery-3.2.1.min.js"></script>
-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	{if isset($post)}
	<script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
	<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
	{/if}
	{include file="sys/js_inc.tpl" place="init"}
	{include file="sys/js_inc.tpl" place="header"}
	<link rel="stylesheet" href="/{$path}styles/tickets.css">

	<div class="popup_layer"></div>

	<div class="main">
		<div class="fll left-wrap">
			<!-- begin app-bar -->
			<div class="app-bar full-viewport-height main-flex fll">
				<nav role="navigation" class="left-nav simple-scrollbar">
					<ul class="flex-fill">
						<li class="tooltip tip-right" data-tip="Новости">
							<a href="/" class="app-bar-link">
								<span class="md-icon">assignment</span>
								<span class="app-bar-text">Новости</span>
							</a>
						</li>
						<li class="tooltip tip-right" data-tip="Купить билеты">
							<a href="/ticket" class="app-bar-link">
								<span class="md-icon">shopping_cart</span>
								<span class="app-bar-text">Купить</span>
							</a>
						</li>
						<li class="tooltip tip-right" data-tip="Наша команда">
							<a href="/about" class="app-bar-link">
								<span class="md-icon">group</span>
								<span class="app-bar-text">Наша команда</span>
							</a>
						</li>
						<li class="tooltip tip-right" data-tip="Галерея">
							<a href="/gallery" class="app-bar-link">
								<span class="md-icon">collections</span>
								<span class="app-bar-text">Галерея</span>
							</a>
						</li>
						<li class="mobile-show mobile-hide {if !$oUser->id}login-button{/if}">
							<a href="/profile" class="app-bar-link">
								<span class="md-icon">account_circle</span>
								<span class="app-bar-text">Профиль</span>
							</a>
						</li>
						<li class="mobile-hide spacer"></li>
						{if $oUser->group == 2}
						<li class="tooltip tip-right" data-tip="События">
							<a href="/admin/event" class="app-bar-link">
								<span class="md-icon">event</span>
								<span class="app-bar-text">События</span>
							</a>
						</li>
						<li class="tooltip tip-right" data-tip="Промокоды">
							<a href="/admin/promo" class="app-bar-link">
								<span class="md-icon">attach_money</span>
								<span class="app-bar-text">Промокоды</span>
							</a>
						</li>
						{/if}
						<li class="tooltip tip-right mobile-hide {if !$oUser->id}login-button{/if}" data-tip="{if $user->is_logged()}{$oUser->fname} {$oUser->lname}{else}Войти в аккаунт{/if}">
							<a href="/profile" class="app-bar-link avatar">
								{get_avatar u=$oUser}
							</a>
						</li>
					</ul>
				</nav>
			</div>
			<!-- end app-bar -->

			<!-- begin app-content -->
			<!--
			<div class="app-content full-viewport-height fll">
				<div class="loader">
					<a><i class="fa fa-circle-o-notch fa-spin fa-3x"></i></a>
				</div>
				<div class="app-content-wrap">
					<div class="search">
						<div class="app-top">
							<div class="search-flex">
								<form action="#" class="search-form">
									<div class="group">
										<input class="field text-field search-field" type="text" placeholder="Поиск">
										<button class="icon icon-search search-but"></button>
									</div>
								</form>
								<button class="icon icon-pencil app-icon search-outer"></button>
							</div>
						</div>
					</div>
					<div class="tab-content"></div>
				</div>
			</div>
			<!-- end app-content -->
		</div>
		<div class="page-content">
			{$content}
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>

	{include file="sys/js_inc.tpl" place="footer"}

	<script>
		var sidebar = '{$sidebar}';
		//load_sidebar(sidebar);
		jQuery(document).on('click', '[data-type="actions"]', function(e){
			e.preventDefault();
			var list = jQuery(this).parent().find('.actions-menu');
			if(jQuery('body').width()<480){
				list.width(jQuery(this).parent().parent().width());
			}
			if(list.hasClass('show')){
				list.removeClass('show');
			}else{
				list.addClass('show');
				if(!list.hasClass('loaded')){
					jQuery.post('/ajax/user/actions/'+jQuery(this).attr('data-user'), {}, function(data){
						list.addClass('loaded');
						data = eval('('+data+')');
						list.html(data.html);
					})
				}
				jQuery('.actions-menu').removeClass('show');
				list.toggleClass('show');
			}
		})
		jQuery(document).on('click', '.tree-header', function(e){
			var o = jQuery(this).find('.icon');
			var list = jQuery(this).parent().find(' > ul');
			if(o.hasClass('icon-down-dir')){
				list.hide();
				o.addClass('icon-right-dir').removeClass('icon-down-dir');
			}else{
				list.show();
				o.removeClass('icon-right-dir').addClass('icon-down-dir');
			}
		})
		jQuery(document).on('click', '.sapp-bar li a', function(e){
			e.preventDefault();
			var o = jQuery('.left-wrap .app-content');
			if(o.hasClass('show')){
				if(sidebar !== jQuery(this).attr('data-id'))
					return false;
				jQuery('body').css('overflow', 'auto');
				width = o.find('.app-content-wrap').width();
				o.animate({ left: '-'+parseInt(width + 20) }, 200).removeClass('show');
			}else{
				if(jQuery('body').width() < 480){
					jQuery('body').css('overflow', 'hidden');
					o.animate({ left: '0px' }, 200).addClass('show');
				}else if(jQuery('body').width() > 480 && jQuery('body').width() < 768){
					o.animate({ left: '42px' }, 200).addClass('show');
				}
			}
		})
		window.onresize = function(e) {
			var o = jQuery('.left-wrap .app-content');
			if(o.hasClass('show')){
				if(jQuery('body').width() < 480){
					o.animate({ left: '0px' }, 200).addClass('show');
				}else{
					o.animate({ left: '42px' }, 200).addClass('show');
				}
			}
		}

		$(".login-button").click(function(e){
			e.preventDefault();
			popup.show('login');
		})
	</script>
    {literal}
	<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125514172-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-125514172-1');
    </script>
    <script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?159",t.onload=function(){VK.Retargeting.Init("VK-RTRG-291043-binXN"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img src="https://vk.com/rtrg?p=VK-RTRG-291043-binXN" style="position:fixed; left:-999px;" alt=""/></noscript>
    <script type="text/javascript" src="https://vk.com/js/api/openapi.js?159"></script>

	<!-- VK Widget -->
	<div id="vk_community_messages"></div>
	<script type="text/javascript">
	VK.Widgets.CommunityMessages("vk_community_messages", 174909461, {expandTimeout: "0",tooltipButtonText: "Есть вопрос?"});
	</script>
    {/literal}
</body>
</html>