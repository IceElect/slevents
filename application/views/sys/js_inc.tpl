{strip}
    {if (!empty($aJsFiles[$place]))}
        {foreach from=$aJsFiles[$place] key="file_key" item="file_path" }
            <script type='text/javascript' src='{$file_path}'></script>
        {/foreach}
    {/if}
{/strip}