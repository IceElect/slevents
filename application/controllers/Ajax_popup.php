<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_popup extends Default_Controller {
	private $response = array('response' => false, 'html' => '');
	function __construct(){
		parent::__construct();
		$this->load->model('user_model');

		$this->user_id = $this->session->userdata('user_id');
	}

	public function index()
	{
		$this->frontend->view('welcome');
	}

	function confirm($type){
		if(!$this->user->is_logged())
			exit;
		$this->response = $this->frontend->fetch('popup/confirm_'.$type);
		echo $this->frontend->returnJson($this->response);
	}

	function login(){
		$this->response = $this->frontend->fetch('popup/login');
		echo $this->frontend->returnJson($this->response);
	}

    function photo($id){
        $this->load->model('Photo_model', 'OAlbums');
        $this->OAlbums->user_id = $this->oUser->id;

        //$this->pm->add_action('getPhotoComments', array($this->OAlbums,'getPhotoComments'));

        $response = array('response' => false);
        $photo = $this->OAlbums->getPhoto($id);
        if($photo){
            $this->my_smarty->assign('photo', $photo);
            $response = $this->frontend->fetch('popup/photo');
        }

        echo json_encode($response);
    }

    function photo_upload($type){
        $response = array('response' => false);

        $this->my_smarty->assign('album', $this->input->post('album'));

        switch ($type) {
            case 'upload':
                $response = $this->frontend->fetch('popup/photo_upload');
                break;
            case 'avatar':
                $response = $this->frontend->fetch('popup/avatar_upload');
                break;
            default:
                # code...
                break;
        }

        echo json_encode($response);
    }

    function avatar_crop(){
        $aData = $this->input->post();
        $response = array('response' => false);

        $response = $this->frontend->fetch('popup/avatar_crop');

        echo json_encode($response);
    }

	function pay(){
        if(!$this->user->is_logged())
            exit;

		$id = $this->input->post('id');
		$this->load->model('Ticket_model', 'ticket');
        $this->load->model('gifts_model', 'gifts');
        $this->ticket->user_id = $this->oUser->id;

		$ticket = $this->ticket->getTicket($id);
		$gifts = $this->gifts->GetGiftsFromTicket($ticket->id);

        if($ticket){
            if(($ticket->count - $ticket->t_count) > 0){
                $this->load->library('robokassa');
                $url = $this->robokassa->create_payment($this->oUser->id, $ticket->price, "PCR", "Покупка билета ".$ticket->name);
                $oid = $this->robokassa->get_order_id();

                $hash = md5(time().$this->oUser->id);
                $tid = $this->ticket->buy($id, $hash, $oid);

                $this->my_smarty->assign('url', $url);
                $this->my_smarty->assign('tid', $tid);
                $this->my_smarty->assign('oid', $oid);
                $this->my_smarty->debugging = true;
                $this->my_smarty->assign('Gifts', $gifts);

                $this->response = $this->frontend->fetch('popup/pay');
                $this->response['url'] = $url;
                $this->response['oid'] = $url;
            }
        }

		echo $this->frontend->returnJson($this->response);
	}

    function add_event(){
        $this->setActiveModule('admin/event');
        //$this->permission->check_action_redirect('view');

        $this->response = $this->frontend->fetch('popup/event_add');

        echo $this->frontend->returnJson($this->response);
    }

    function edit_ticket(){
        $id = $this->input->post('id');
        $event_id = $this->input->post('event_id');

        $this->load->model('Ticket_model', 'ticket');

        $this->setActiveModule('admin/ticket');
        //$this->permission->check_action_redirect('edit');

        $ticket = $this->ticket->getTicket($id);
        $ticket->days = explode(',', $ticket->days);

        if($id){
            $this->frontend->setTitle('Редактирование билета');
        }else{
            $this->frontend->setTitle('Создание нового билета');
        }

        $this->my_smarty->assign('id', $id);
        $this->my_smarty->assign('ticket', $ticket);
        $this->my_smarty->assign('event_id', $event_id);
        $this->response = $this->frontend->fetch('popup/ticket');

        echo $this->frontend->returnJson($this->response);
    }

    function edit_personal(){
        $id = $this->input->post('id');
        $event_id = $this->input->post('event_id');

        $this->load->model('Ticket_model', 'ticket');

        $this->setActiveModule('admin/ticket');

        $this->frontend->setTitle('Добавление персонала');

        $this->my_smarty->assign('id', $id);
        // $this->my_smarty->assign('ticket', $ticket);
        $this->my_smarty->assign('event_id', $event_id);
        $this->response = $this->frontend->fetch('popup/personal');

        echo $this->frontend->returnJson($this->response);
    }

    function edit_visitor(){
        $id = $this->input->post('id');
        $event_id = $this->input->post('event_id');

        $this->load->model('Ticket_model', 'ticket');
        $tickets = $this->ticket->getTickets($event_id, true);
        $this->my_smarty->assign('tickets', $tickets);

        $this->setActiveModule('admin/ticket');

        $this->frontend->setTitle('Добавление посетителя');

        $this->my_smarty->assign('id', $id);
        // $this->my_smarty->assign('ticket', $ticket);
        $this->my_smarty->assign('event_id', $event_id);
        $this->response = $this->frontend->fetch('popup/visitor');

        echo $this->frontend->returnJson($this->response);
    }
}