<?php

class Admin_event extends Default_controller
{
    function __construct()
    {
        parent::__construct();
        $this->frontend->used_backend(); // Указываем, что это админка
        $this->setActiveModule('admin/event'); // Нужно для генерации ссылок

        $this->frontend->setTitle($this->translate->t('admin_event_title', 'мероприятия')); // Задаем заголовок t(translate_name, default_value)

        $this->load->model('Default_model', 'event');
        $this->event->setTable('events');
    }

    // Страница с таблицей - список всех мероприятий
    function index(){
        $this->permission->check_action_redirect('view'); // Проверяем права пользователя на открытие этой страницы

        $this->load->library('fields'); // Подгружаем либу fields
        $this->setFields(); // Инициализируем поля

        // Настройки таблицы, добавляем кнопки
        $left = array(
            'buttons' => array(
                array('text' => 'Создать мероприятие', 'url' => '#', 'onclick' => 'add_event(event)')
            ),
        );

        $this->fields->grid($left); // Отобразить таблицу
    }

    // Редактирование
    function edit($id = false){
        $this->permission->check_action_redirect('edit'); // Проверяем права пользователя на открытие этой страницы

        $this->load->model('Ticket_model', 'ticket');
        $tickets = $this->ticket->getTickets($id, true);
        $this->my_smarty->assign('tickets', $tickets);

        $personal = $this->ticket->getTicketsByEvent($id, false, 'personal', 999);
        $this->my_smarty->assign('personal', $personal);

        $visitors = $this->ticket->getTicketsByEvent($id, true, 'visitor', 999);
        $this->my_smarty->assign('visitors', $visitors);

        // Задаем заголовок
        if (!empty($id)) {
            $action = 'edit';
            $this->frontend->setTitle($this->translate->t('vote_'. $action .'_form_title', 'Редактирование мероприятия'));
        } else {
            $action = 'add';
            $this->frontend->setTitle($this->translate->t('vote_'. $action .'_form_title', 'Создание мероприятия'));
        }

        $this->load->library('fields'); // Подгружаем либу fields
        $this->fields->addTab(); // Инициализируем табы в форме
        $this->fields->addTab(array('title' => 'Билеты', 'id' => 'tickets')); // Добавляем таб
        $this->fields->addTab(array('title' => 'Персонал', 'id' => 'personal')); // Добавляем таб
        $this->fields->addTab(array('title' => 'Посетители', 'id' => 'visitors')); // Добавляем таб
        $this->fields->addTab(array('title' => 'Дополнительные поля', 'id' => 'additional')); // Добавляем таб

        $this->setFields($id); // Инициализируем поля

        // Устанавливаем сообщения по мероприятиям
        $this->fields->setMessage('wrong_id', 'products_wrong_id', 'мероприятие с таким id не найдено.');
        $this->fields->setMessage('edit_save', 'products_edit_save', 'мероприятие обновлено.');
        $this->fields->setMessage('add_save', 'products_add_save', 'мероприятие добавлено.');

        $this->fields->setHook('tickets', 'admin_tickets');
        $this->fields->setHook('personal', 'admin_personal');
        $this->fields->setHook('visitors', 'admin_visitors');
        $this->fields->add_hook('after_save_', array($this, '_after_save'));

        $this->fields->form($id); // Отображаем форму
    }

    // Функция создания мероприятия
    // Ajax форма в модальном окне
    function add(){
        $response = array('response' => false);

        $name = $this->input->post('name');

        if($name){
            $data = array(
                'name' => $name,
            );

            $id = $this->event->save($data, 'add');
            if($id){
                $response['response'] = true;
                $response['redir'] = '/admin/event/edit/' . $id;
            }else{
                $response['error'] = "Произошла ошибка при сохранении";
            }
        }else{
            $response['error'] = "Заполните обязательные поля";
        }

        echo json_encode($response);
    }

    // Удаление ajax
    function del()
    {
        // $this->permission->check_action_redirect('delete'); // Тут сейчас баг с префиксом таблиц... Опять...
        $this->load->library('fields');
        $this->fields->setTable('events');
        $return = $this->fields->del();
        return $return;
    }

    function _after_save($params){
        $id = $params['id'];
        $aData = $params['aData'];

        //$collection = json_encode($this->input->post('collection'));
        //$steps = json_encode($this->input->post('steps_block'));
        //$advantages = json_encode($this->input->post('advantages'));
       // $this->product_model->save(array('custom_collection' => $collection, 'custom_steps_block' => $steps, 'custom_advantages' => $advantages), 'edit', $id);
    }

    // Поля
    protected function setFields(){
        $this->fields->setTable('events'); // Устанавливаем название таблицы в базе, из которой беруться поля

        $this->fields->addField_check(); // Отображаем чекбокс (выбрать несколько) в таблице
        $this->fields->addField_id(); // Выводим ID на странице редактирования

        // Название мероприятия
        $this->fields->addField_text(array(
            'field' => 'name', // Название поля в базе
            'title' => $this->translate->t('field_name', 'Название'),
            'rules' => 'trim|required|max_length[255]',
        ));

        $this->fields->addField_hidden(array(
            'rules' => '',
            'field' => 'tickets', // Название поля в базе
            'form_disable' => true,
            'tab'=> 1
        ));

        $this->fields->addField_hidden(array(
            'rules' => '',
            'field' => 'personal', // Название поля в базе
            'form_disable' => true,
            'tab'=> 2
        ));

        $this->fields->addField_hidden(array(
            'rules' => '',
            'field' => 'visitors', // Название поля в базе
            'form_disable' => true,
            'tab'=> 3
        ));


        // Обложка мероприятия
        $this->fields->addField_image(array(
            'field' => 'image', // Название поля в базе
            'rules' => 'trim',
            'multiple' => false, // Возможность выбрать несколько (не доделано)
            'title' => 'Обложка',
            'table_show' => false,
        ), array(
            'file_name' => uniqid(), // Название файла для сохранения
            'upload_path' => 'uploads/events/', // Путь сохранения
            'allowed_types' => 'gif|jpg|png|jpeg', // Доступные форматы
            'allowed_types' => '*', // Доступные форматы
            'create_folder' => true, // Создавать папку из пути, если её не существует
            'max_size' => 20000,
        ));

        // Фон мероприятия
        $this->fields->addField_image(array(
            'field' => 'background', // Название поля в базе
            'rules' => 'trim',
            'multiple' => false, // Возможность выбрать несколько (не доделано)
            'title' => 'Фон',
            'table_show' => false,
        ), array(
            'file_name' => uniqid(), // Название файла для сохранения
            'upload_path' => 'uploads/background/', // Путь сохранения
            'allowed_types' => 'gif|jpg|png|jpeg', // Доступные форматы
            'allowed_types' => '*', // Доступные форматы
            'create_folder' => true, // Создавать папку из пути, если её не существует
            'max_size' => 20000,
        ));

        $this->fields->addField_date (array(
            'field' => 'date', // Название поля в базе
            'title' => $this->translate->t('field_name', 'Дата проведения'),
            'rules' => 'trim|required|max_length[255]',
        ));
        $this->fields->addField_date (array(
            'field' => 'end_date', // Название поля в базе
            'title' => $this->translate->t('field_name', 'Дата окончания'),
            'rules' => 'trim|required|max_length[255]',
        ));
        $this->fields->addField_text(array(
            'field' => 'location', // Название поля в базе
            'title' => $this->translate->t('field_name', 'Место проведения'),
            'rules' => 'trim|required|max_length[255]',
        ));
        $this->fields->addField_text(array(
            'field' => 'on_map', // Название поля в базе
            'title' => $this->translate->t('field_name', 'На карте'),
            'rules' => 'trim|required|max_length[500]',
            'table_show' => false,
        ));
        $this->fields->addField_text(array(
            'field' => 'short_description', // Название поля в базе
            'title' => $this->translate->t('field_name', 'Краткое описание'),
            'table_show' => false,
            'rules' => 'trim|required|max_length[2555]',
        ));

        // Статус продажи билетов - ВЫВОДИТСЯ В САЙДБАРЕ
        $this->fields->addField_select(array(
            'field' => 'closed_buy', // Название поля в базе
            'rules' => 'trim',
            'table_width' => 100,
            'title' => $this->translate->t('field_closed_buy', 'Продажа билетов'),
            'options' => array(
                0 => $this->translate->t('event_closed_buy_open', 'Открыта'),
                1 => $this->translate->t('event_closed_buy_closed', 'Закрыта'),
            ),
            'tab' => 'sidebar', // Указываем, что выводится справа, у кнопки "сохранить"
        ));
        // Статус мероприятия - ВЫВОДИТСЯ В САЙДБАРЕ
        $this->fields->addField_select(array(
            'field' => 'status', // Название поля в базе
            'rules' => 'trim',
            'table_width' => 100,
            'title' => $this->translate->t('field_status', 'Статус'),
            'options' => array(
                0 => $this->translate->t('event_status_draft', 'Скрыто'),
                1 => $this->translate->t('event_status_approved', 'Активно'),
                2 => $this->translate->t('event_status_denied', 'Окончено'),
            ),
            'tab' => 'sidebar', // Указываем, что выводится справа, у кнопки "сохранить"
        ));

        // Описание мероприятия
        $this->fields->addField_wysiwyg(array(
            'field' => 'description', // Название поля в базе
            'type' => 'wysiwyg',
            'table_show' => false,
            'title' => $this->translate->t('field_description', 'Описание'),
            'translable' => 1,
            'rules' => '',
        ));

        $this->fields->addField_text(array(
            'type' => 'number',
            'field' => 'days_count', // Название поля в базе
            'title' => $this->translate->t('field_days_count', 'Количество дней'),
            'rules' => 'trim|required',
            'table_show' => false,
        ));

        // Кнопки действий в таблице (редактинование)
        $this->fields->addField_text(array(
            'field' => '',
            'form_show' => false,
            'form_disable' => true,
            'validate' => false,
            'title' => $this->translate->t('field_empty', ' '),
            'view_callback' => array($this, 'event_buttons_callback'), // Задаем функцию, которая возвращает html содержимое колонки (в данном случае $this->event_buttons_callback())
            'sortable' => false,
            'table_width' => '30',
        ));
    }

    /*
    * Функция возвращает html значение колонки в таблице
    * $row - информация о записи ивента (название, описание и всё остальное)
    */
    function event_buttons_callback($row)
    {
        $html = '<a href="'. base_url() .'admin/event/edit/'.$row->id.'"><i class="md-icon">edit</i></a>';
        return $html;
    }


    function generate_personal($event_id){
        $this->load->model('Ticket_model', 'ticket');

        $offset = 0;
        if(isset($_GET['offset'])){
            $offset = $_GET['offset'];
        }

        $personal = $this->ticket->getUTickets($event_id, true, 'personal', 35, $offset);
        $tickets = array();

        foreach($personal as $ticket){
            $tickets[] = (object) array(
                'title' => $ticket->fname . ' ' . $ticket->lname,
                'qr' => FCPATH . 'tickets/' . $ticket->hash . '.png',
            );
        }

        $this->_list($tickets);
    }

    function generate_guest($event_id){
        $offset = 0;
        $tickets = array();

        if(isset($_GET['offset'])){
            $offset = $_GET['offset'];
        }

        $this->load->model('Ticket_model', 'ticket');
        $this->ticket->user_id = 0;

        require(APPPATH.'third_party/phpqrcode/qrlib.php');

        for($i = $offset+1; $i <= $offset+35; $i++){
            $hash = md5(time() . 'guest' . $i);
            $tid = $this->ticket->buy(-1, $hash, 0, 1, $event_id, 'guest', $i);

            $qr = "tickets/".$hash.".png";
            QRcode::png(base_url('ticket/check/'.$hash), $qr, "L", 6, 3);

            $tickets[] = (object) array(
                'title' => 'Гостевой билет №' . $i,
                'qr' => FCPATH . $qr,
            );
        }

        $this->_list($tickets);
    }

    function _list($tickets){
        header ('Content-Type: image/png');

        $width = 1240;
        $height = 1754;

        $item_size = 248;
        $code_size = 212;

        $im = imagecreatetruecolor($width, $height);
        $darkBackground = imagecolorallocate($im, 170, 170, 170);
        $blackBackground = imagecolorallocate($im, 0, 0, 0);
        $whiteBackground = imagecolorallocate($im, 255, 255, 255);
        imagefill($im,0,0,$whiteBackground);

        // BORDERS
        $x_count = floor( $width / $item_size );
        $y_count = floor( $height / $item_size );

        for($i = 1; $i <= $x_count; $i++){
            $space = $item_size * $i;
            imageline($im, $space, 0, $space, $height, $darkBackground);
        }

        for($i = 1; $i <= $y_count; $i++){
            $space = $item_size * $i;
            imageline($im, 0, $space, $width, $space, $darkBackground);
        }

        // QR CODES
        for($line = 1; $line <= $x_count; $line++){

            $space_y = $item_size * ($line - 1);

            for($column = 1; $column <= $y_count; $column++){
                $space_x = $item_size * ($column - 1);

                
            }

        }

        $count = 35;

        $font = FCPATH . 'SegoeUIRegular.ttf';

        foreach($tickets as $i => $ticket){
            $line = intval( $i / $x_count );
            $column = $i % $x_count;

            $space_y = ($item_size + 1) * ($line);
            $space_x = ($item_size + 1) * ($column);

            $qr = imagecreatefrompng($ticket->qr);
            imagecopyresized($im, $qr, $space_x + 18, $space_y, 0, 0, $code_size, $code_size, 234, 234);

            $text = $ticket->title;

            $box = imageftbbox( 14, 0, $font, $text ); 
            $x = ($item_size - ($box[2] - $box[0])) / 2; 
            $y = ($item_size - ($box[1] - $box[7])) / 2; 
            $y -= $box[7];

            imagettftext($im, 14, 0, $space_x + $x, $space_y + $code_size + 12, $blackBackground, $font, $text);
        }

        imagepng($im);
    }
}