<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends Default_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('blog_model', 'blog');
		$posts = $this->blog->getDataByWhere('p.category_id = 5');
		$this->my_smarty->assign('posts', $posts);
		$this->frontend->view('info');
	}

	public function about(){
		$contacts = array();
		$aContacts = array(
			// array('id' => 1, 'role' => 'Ведущий разработчик'),
			array('id' => 5, 'role' => 'Главный организатор'),
			array('id' => 360, 'role' => 'Зам. глав. орга'),
			array('id' => 227, 'role' => 'Орг развлекательной зоны'),
			array('id' => 256, 'role' => 'Орг аукциона и косплея'),
		);

		$this->load->model('user_model');

		foreach($aContacts as $key => $contact){
			$user = $this->user_model->getUserById($contact['id']);
			$user->role = $contact['role'];
			$contacts[] = $user;
		}
		$you = $this->user->get_anon_user();
		$you->fname = "Здесь могли бы быть";
		$you->lname = "Вы";
		$you->role = "Кем-нибудь";

        $i_rem = (4-count($aContacts));

        for($i=0;$i<$i_rem;$i++){
    		$contacts[] = $you;
        }

		$this->my_smarty->assign('roles', $contacts);
		$this->my_smarty->assign('contacts', $contacts);

		$this->frontend->view('about');
	}

	public function gallery(){
		$this->load->model('ticket_model');

		$events = $this->ticket_model->getEvents(2);

		$this->my_smarty->assign('events', $events);
		$this->frontend->view('gallery');
	}

	public function album(){
		$response = array('response' => false);
		$aData = $this->input->post();

		if($aData['id']){
			$this->load->model('photo_model');

			$photos = $this->photo_model->getPhotos($aData['id']);

			$this->my_smarty->assign('photos', $photos);
			$response = $this->frontend->fetch('user/album');
		}

		echo json_encode($response);
	}
}
