<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_ticket extends Default_Controller {
    private $response = array('response' => false, 'html' => '');
    function __construct(){
        parent::__construct();
        $this->load->model('user_model');

        $this->user_id = $this->session->userdata('user_id');
    }

    function button($id = false){

        if(!$this->user->is_logged())
            exit;

        $response = true;

        $promo = $this->input->post('promo');

        //$id = $this->input->post('id');
        $this->load->model('Ticket_model', 'ticket');
        $this->ticket->user_id = $this->oUser->id;

        $ticket = $this->ticket->getUserTicket($id);

        $this->load->library('robokassa');

        $price = $ticket->price;

        if($promo){
            $promo = $this->robokassa->check_promo($promo, $this->user_id);
            if($promo){
                $price = ($promo->discount>$price)?0:($price-$promo->discount);
                $this->response['text'] = "Вы применили промокод на сумму " . $promo->discount . " рублей";
            }else{
                $response = false;
                $this->response['error'] = 'Данный промокод не существует или уже был использован';
            }
        }


        $url = $this->robokassa->set_payment($ticket->payment_id, $price, "PCR", "Покупка билета ".$ticket->name);

        if($url && $response)
            $this->response['response'] = true;

        $this->response['url'] = $url;
        $this->response['html'] = '<a href="'.$url.'" class="button" id="pay-button">Оплатить билет</a>';

        echo $this->frontend->returnJson($this->response);
    }

    function select_gift($id = false){
        if(!$this->user->is_logged())
            exit;
        $this->load->model('Ticket_model', 'ticket');

        $selected_param = $this->input->post('selected_param');
        $this->ticket->set_gift($id,json_encode($selected_param));
    }

    function edit(){
        $this->load->model('Ticket_model', 'ticket');
        $this->load->model('Default_model', 'event');
        $this->event->setTable('events');

        $data = $this->input->post();

        $this->setActiveModule('admin/ticket');
        //$this->permission->check_action_redirect('edit');

        $id = $data['id'];
        $event_id = $data['event_id'];

        $aData = array(
            'id' => $id,
            'event_id' => $event_id,
            'name' => $data['name'],
            'price' => $data['price'],
            'count' => $data['count'],
            'description' => $data['description'],
            'days' => implode(',', $data['days']),
        );

        if($id){
            $result = $this->ticket->save($aData, 'edit', $id);
        }else{
            $result = $this->ticket->save($aData, 'add');
        }

        if($result){
            $this->response['response'] = true;

            $tickets = $this->ticket->getTickets($event_id, true);
            $this->my_smarty->assign('tickets', $tickets);
            $this->my_smarty->assign('nId', $event_id);

            $count = 0;

            foreach($tickets as $t){
                $count += $t->count;
            }

            $this->event->update(array('max_count' => $count), array('id' => $event_id));

            $table_html = $this->frontend->fetch('default/admin_tickets');
            $this->response['table_html'] = $table_html['data'];
        }

        echo $this->frontend->returnJson($this->response);
    }

    function del(){
        $this->load->model('Ticket_model', 'ticket');

        $data = $this->input->post();

        //$this->setActiveModule('admin/ticket');
        //$this->permission->check_action_redirect('delete');

        $id = $data['id'];
        $event_id = $data['event_id'];

        $result = $this->ticket->delWhere(array('id' => $id));

        if($result){
            $this->response['response'] = true;

            $tickets = $this->ticket->getTickets($event_id, true);
            $this->my_smarty->assign('tickets', $tickets);
            $this->my_smarty->assign('nId', $event_id);

            $table_html = $this->frontend->fetch('default/admin_tickets');
            $this->response['table_html'] = $table_html['data'];
        }

        echo $this->frontend->returnJson($this->response);
    }


    function personal(){
        $this->load->model('Ticket_model', 'ticket');

        $data = $this->input->post();

        $this->setActiveModule('admin/ticket');
        //$this->permission->check_action_redirect('edit');

        // $id = $data['id'];
        $role = $data['role'];
        $user_id = $data['user_id'];
        $event_id = $data['event_id'];

        $this->ticket->user_id = $user_id;

        $hash = md5(time().$user_id);
        $tid = $this->ticket->buy(-1, $hash, 0, 1, $event_id, 'personal', $role);

        $this->response['response'] = true;

        $user = $this->user_model->getUserById($user_id);
        $ticket = $this->ticket->getTicketByHash($hash);

        // Генерируем qr код
        require(APPPATH.'third_party/phpqrcode/qrlib.php');
        $ticket->qr = "tickets/".$hash.".png";
        QRcode::png(base_url('ticket/check/'.$hash), $ticket->qr, "L", 6, 3);

        // Отправляем email
        $this->my_smarty->assign('ticket', $ticket);
        $html = $this->frontend->get_mail_html('personal');

        $address_list = $this->config->item('address', 'mailer');
        $this->load->library('mailer');
        $this->mailer->send_mail(
            $address_list['site']['mail'],
            $address_list['site']['name'],
            $user->email,
            $user->fname,
            'Пропуск персонала',
            $html
        );

        // Генерируем новую таблицу
        $personal = $this->ticket->getTicketsByEvent($event_id, true, 'personal', 999);
        $this->my_smarty->assign('personal', $personal);
        $this->my_smarty->assign('nId', $event_id);

        $table_html = $this->frontend->fetch('default/admin_personal');
        $this->response['table_html'] = $table_html['data'];

        echo $this->frontend->returnJson($this->response);
    }

    function visitor(){
        $this->load->model('Ticket_model', 'ticket');

        $data = $this->input->post();

        $this->setActiveModule('admin/ticket');
        //$this->permission->check_action_redirect('edit');

        // $id = $data['id'];
        $user_id = $data['user_id'];
        $event_id = $data['event_id'];
        $ticket_id = $data['ticket_id'];

        $this->ticket->user_id = $user_id;

        $hash = md5(time().$user_id);
        $tid = $this->ticket->buy($ticket_id, $hash, 0, 1, $event_id, 'visitor');

        $this->response['response'] = true;

        $user = $this->user_model->getUserById($user_id);
        $ticket = $this->ticket->getTicketByHash($hash);

        // Генерируем qr код
        require(APPPATH.'third_party/phpqrcode/qrlib.php');
        $ticket->qr = "tickets/".$hash.".png";
        QRcode::png(base_url('ticket/check/'.$hash), $ticket->qr, "L", 6, 3);

        // Отправляем email
        $this->my_smarty->assign('ticket', $ticket);
        $html = $this->frontend->get_mail_html('ticket');

        $address_list = $this->config->item('address', 'mailer');
        $this->load->library('mailer');
        $this->mailer->send_mail(
            $address_list['site']['mail'],
            $address_list['site']['name'],
            $user->email,
            $user->fname,
            'Вам оформлен билет',
            $html
        );

        // Генерируем новую таблицу
        $visitors = $this->ticket->getTicketsByEvent($event_id, true, 'visitor', 999);
        $this->my_smarty->assign('visitors', $visitors);
        $this->my_smarty->assign('nId', $event_id);

        $table_html = $this->frontend->fetch('default/admin_visitors');
        $this->response['table_html'] = $table_html['data'];

        echo $this->frontend->returnJson($this->response);
    }

    function del_personal(){
        $this->load->model('default_model');
        $this->default_model->setTable('user_ticket');

        $data = $this->input->post();

        $id = $data['id'];
        $event_id = $data['event_id'];

        $result = $this->default_model->delWhere(array('id' => $id, 'type' => 'personal'));

        if($result){
            $this->load->model('Ticket_model', 'ticket');

            $personal = $this->ticket->getTicketsByEvent($event_id, true, 'personal', 999);
            $this->my_smarty->assign('personal', $personal);
            $this->my_smarty->assign('nId', $event_id);

            $table_html = $this->frontend->fetch('default/admin_personal');
            $this->response['table_html'] = $table_html['data'];
        }

        echo $this->frontend->returnJson($this->response);
    }

    function del_visitor(){
        $this->load->model('default_model');
        $this->default_model->setTable('user_ticket');

        $data = $this->input->post();

        $id = $data['id'];
        $event_id = $data['event_id'];

        $result = $this->default_model->delWhere(array('id' => $id, 'type' => 'visitor'));

        if($result){
            $this->load->model('Ticket_model', 'ticket');

            $visitors = $this->ticket->getTicketsByEvent($event_id, true, 'visitor', 999);
            $this->my_smarty->assign('visitors', $visitors);
            $this->my_smarty->assign('nId', $event_id);

            $table_html = $this->frontend->fetch('default/admin_visitors');
            $this->response['table_html'] = $table_html['data'];
        }

        echo $this->frontend->returnJson($this->response);
    }
}