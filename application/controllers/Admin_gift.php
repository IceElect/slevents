<?php

class Admin_gift extends Default_controller
{
    function __construct()
    {
        parent::__construct();
        $this->frontend->used_backend(); // Указываем, что это админка
        $this->setActiveModule('admin/gift'); // Нужно для генерации ссылок

        $this->frontend->setTitle($this->translate->t('admin_gift_title', 'Подарки')); // Задаем заголовок t(translate_name, default_value)

        $this->load->model('Default_model', 'gift');
        $this->gift->setTable('gift');
    }

    // Страница с таблицей - список всех мероприятий
    function index(){
        $this->permission->check_action_redirect('view'); // Проверяем права пользователя на открытие этой страницы

        $this->load->library('fields'); // Подгружаем либу fields
        $this->setFields(); // Инициализируем поля

        // Настройки таблицы, добавляем кнопки
        $left = array(
            'buttons' => array(
                array('text' => 'Создать подарок', 'url' => '#', 'onclick' => 'add_gift(gift)')
            ),
        );

        $this->fields->grid($left); // Отобразить таблицу
    }

    // Редактирование
    function edit($id = false){
        //$this->permission->check_action_redirect('edit'); // Проверяем права пользователя на открытие этой страницы

        $this->load->model('gifts_model','gifts');
        $gift = $this->gifts->getGiftById($id);
        $this->my_smarty->assign('gift', $gift);

        // Задаем заголовок
        if (!empty($id)) {
            $action = 'edit';
            $this->frontend->setTitle($this->translate->t('gift_'. $action .'_form_title', 'Редактирование подарка'));
        } else {
            $action = 'add';
            $this->frontend->setTitle($this->translate->t('gift_'. $action .'_form_title', 'Создание подарка'));
        }

        $this->load->library('fields'); // Подгружаем либу fields
        $this->fields->addTab(); // Инициализируем табы в форме
        //$this->fields->addTab(array('title' => 'Билеты', 'id' => 'tickets')); // Добавляем таб
        //$this->fields->addTab(array('title' => 'Дополнительные поля', 'id' => 'additional')); // Добавляем таб

        $this->setFields($id); // Инициализируем поля

        // Устанавливаем сообщения по мероприятиям
        $this->fields->setMessage('wrong_id', 'products_wrong_id', 'мероприятие с таким id не найдено.');
        $this->fields->setMessage('edit_save', 'products_edit_save', 'мероприятие обновлено.');
        $this->fields->setMessage('add_save', 'products_add_save', 'мероприятие добавлено.');

        //$this->fields->setHook('tickets', 'admin_tickets');
        //$this->fields->add_hook('after_save_', array($this, '_after_save'));

        $this->fields->form($id); // Отображаем форму
    }

    // Удаление ajax
    function del()
    {
        // $this->permission->check_action_redirect('delete'); // Тут сейчас баг с префиксом таблиц... Опять...
        $this->load->library('fields');
        $this->fields->setTable('gift');
        $return = $this->fields->del();
        return $return;
    }

    // Поля
    protected function setFields(){
        $this->fields->setTable('gift'); // Устанавливаем название таблицы в базе, из которой беруться поля

        $this->fields->addField_check(); // Отображаем чекбокс (выбрать несколько) в таблице
        $this->fields->addField_id(); // Выводим ID на странице редактирования

        // Название мероприятия
        $this->fields->addField_text(array(
            'field' => 'name', // Название поля в базе
            'title' => $this->translate->t('field_name', 'Название'),
            'rules' => 'trim|required|max_length[255]',
        ));

        $this->fields->addField_text(array(
            'field' => 'attributes',
            'rules' => '',
            'disable_save' => true,
            'title' => $this->translate->t('field_gift_attributes', 'Аттрибуты'),
            'custom_field_template' => 'gift_attributes',
        ));

        //$this->fields->addField_hidden(array(
        //    'rules' => '',
        //    'field' => 'tickets',
        //    'form_disable' => true,
        //    'tab'=> 1
        //));

        // Кнопки действий в таблице (редактинование)
        $this->fields->addField_text(array(
            'field' => '',
            'form_show' => false,
            'form_disable' => true,
            'validate' => false,
            'title' => $this->translate->t('field_empty', ' '),
            'view_callback' => array($this, 'gift_buttons_callback'), // Задаем функцию, которая возвращает html содержимое колонки (в данном случае $this->gift_buttons_callback())
            'sortable' => false,
            'table_width' => '30',
        ));
    }

    /*
    * Функция возвращает html значение колонки в таблице
    * $row - информация о записи ивента (название, описание и всё остальное)
    */
    function gift_buttons_callback($row)
    {
        $html = '<a href="'. base_url() .'admin/gift/edit/'.$row->id.'"><i class="md-icon">edit</i></a>';
        return $html;
    }
}