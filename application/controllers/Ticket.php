<?php

class Ticket extends Default_controller
{

    function __construct()
    {
        parent::__construct();
        $this->setActiveModule('user');
        $this->load->model('Ticket_model', 'ticket');
        $this->ticket->user_id = $this->oUser->id;
    }

    function test(){
        $array = array();
        for($i=0;$i<6;$i++){
            $array[$i] = (object) array('title' => 'Стенд', 'tile' => $i, 'left' => 50, 'top' => 50);
        }
        echo json_encode((object) $array);
    }

    function index(){

        $tickets = array();
        $aTickets = $this->ticket->getEvents(1);


        $month = array("1"=>"Январь","2"=>"Февраль","3"=>"Март","4"=>"Апрель","5"=>"Май", "6"=>"Июнь", "7"=>"Июль","8"=>"Август","9"=>"Сентябрь","10"=>"Октябрь","11"=>"Ноябрь","12"=>"Декабрь");

        if(count($aTickets) > 0){
            foreach($aTickets as $key => $ticket){
                $ticket->day = date("d", strtotime($ticket->date));
                $ticket->time = date("H:i", strtotime($ticket->date));
                $ticket->month = $month[date("n", strtotime($ticket->date))];
                $ticket->tickets = $this->ticket->getTickets($ticket->id);
                $ticket->closed = $ticket->closed_buy;

                $tickets[] = $ticket;
            }
        }

        $this->my_smarty->assign('tickets', $tickets);
        $this->frontend->view('buy');
    }

    function check($hash){
        $ticket = $this->ticket->getTicketByHash($hash, true);
        if($ticket){
            $this->my_smarty->assign('ticket', $ticket);
            $html = $this->frontend->fetch('check');
            echo $html['data'];
        }else{
            dump("Валите его");
        }
    }

    function event($id){
        $event = $this->ticket->getEvent($id);

        $month = array("1"=>"Января","2"=>"Февраля","3"=>"Марта","4"=>"Апреля","5"=>"Мая", "6"=>"Июня", "7"=>"Июля","8"=>"Августа","9"=>"Сентября","10"=>"Октября","11"=>"Ноября","12"=>"Декабря");

        if($event){

            $event->day = date("d", strtotime($event->date));
            $event->year = date("Y", strtotime($event->date));
            $event->time = date("H:i", strtotime($event->date));
            $event->month = $month[date("n", strtotime($event->date))];
            $event->tickets = $this->ticket->getTickets($id, $event->days_count);
            $event->closed = $event->closed_buy;

            $this->my_smarty->assign('event', $event);

            $this->frontend->view('event');
        }
    }

    function buy2($id){
        $ticket = $this->ticket->getTicket($id);

        if($ticket){
            if(($ticket->max_count - $ticket->count) > 0){
                $this->load->library('robokassa');
                echo $this->robokassa->create_payment($this->oUser->id, $ticket->price, "PCR", "За дело git!");
                die();
                
            }
        }
    }

    function result(){
        $this->load->library('robokassa');
        if($this->robokassa->check_pay($_GET['OutSum'], $_GET['InvId'], $_GET['SignatureValue']))
            $this->robokassa->register_payment($_GET['InvId']);
        else
            echo "hm! error!";
    }
    
    function success(){
        $this->load->library('robokassa');
        if($this->robokassa->check_success_pay($_GET['OutSum'], $_GET['InvId'], $_GET['SignatureValue'])){
            $this->robokassa->register_payment($_GET['InvId']);

            $ticket = $this->ticket->getTicketByOrder($_GET['InvId']);

            if($ticket){
                require(APPPATH.'third_party/phpqrcode/qrlib.php');

                $ticket->qr = "tickets/".$ticket->hash.".png";
                QRcode::png(base_url('ticket/check/'.$ticket->hash), $ticket->qr, "L", 6, 3);

                $this->my_smarty->assign('ticket', $ticket);
                $html = $this->frontend->get_mail_html('ticket');

                $address_list = $this->config->item('address','mailer');
                $this->load->library('mailer');
                $this->mailer->send_mail(
                    $address_list['site']['mail'],
                    $address_list['site']['name'],
                    $this->oUser->email,
                    $this->oUser->fname,
                    'Покупка нового билета',
                    $html
                );

                $this->ticket->setTable('ev_user_ticket');
                $this->ticket->update(array(
                    'pay_status' => 1,
                ), array(
                    'id' => $ticket->t_id,
                    'hash' => $ticket->hash,
                ));

                echo $html;
            }
        }else{
            echo "hm! error!";
        }
    }

    function custom_send_mail($id){
        $ticket = $this->ticket->getTicketByOrder($id);

        if($ticket){
            require(APPPATH.'third_party/phpqrcode/qrlib.php');

            $ticket->qr = "tickets/".$ticket->hash.".png";
            QRcode::png(base_url('ticket/check/'.$ticket->hash), $ticket->qr, "L", 6, 3);

            $this->my_smarty->assign('ticket', $ticket);
            $html = $this->frontend->get_mail_html('ticket');

            $address_list = $this->config->item('address','mailer');
            $this->load->library('mailer');
            $this->mailer->send_mail(
                $address_list['site']['mail'],
                $address_list['site']['name'],
                $ticket->email,
                $ticket->fname,
                'Покупка нового билета',
                $html
            );

            $this->ticket->setTable('ev_user_ticket');
                $this->ticket->update(array(
                    'type' => 1,
                ),array(
                    'id' => $ticket->t_id,
                    'hash' => $ticket->hash,
                ));

            echo $html;
        }
    }

    function custom_gen_qr($hash = false){
        require(APPPATH.'third_party/phpqrcode/qrlib.php');

        if($hash){
            $qr = "tickets/".$hash.".png";
            QRcode::png(base_url('ticket/check/'.$hash), $qr, "L", 6, 3);
        }else{
            $this->ticket->setTable('user_ticket');
            $tickets = $this->ticket->getDataByWhere('type > 0');

            foreach($tickets as $ticket){
                $qr = "tickets/".$ticket->hash.".png";
                QRcode::png(base_url('ticket/check/'.$ticket->hash), $qr, "L", 6, 3);
            }
        }
    }

    function success_go($hash){
        $this->ticket->setTable('ev_user_ticket');
        $ticket = $this->ticket->getTicketByHash($hash);

        if($ticket){
            $r = $this->ticket->update(array('enter_status' => 1), array('hash' => $hash));
            if($r){
                echo "Успешно";
                return;
            }
        }
        echo "Ошибка";
    }

    function success_merch($hash){
        $this->ticket->setTable('ev_user_ticket');
        $ticket = $this->ticket->getTicketByHash($hash);

        if($ticket){
            $r = $this->ticket->update(array('merch_status' => 1), array('hash' => $hash));
            if($r){
                echo "Успешно";
                return;
            }
        }
        echo "Ошибка";
    }

    function export($event){
        $list = $this->ticket->getTicketsByEvent($event, true);

        $titles = array(
            'ID',
            'Имя',
            'Фамилия',
            'Дата рождения',
            'Тип билета',
            'Стоимость',
            'Хэш'
        );

        $fp = fopen(FCPATH. 'export/' .  $event . '.' . date('Y-m-d') . '.csv', 'w+');

        fputcsv($fp, $titles);

        foreach ($list as $fields) {
            fputcsv($fp, (array) $fields);
        }

        fclose($fp);
    }
    
    function fail(){
        $this->load->library('robokassa');
        echo "fail";
        print_r($_POST);
    }
}