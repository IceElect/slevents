<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Photo_model extends Default_model
{
    public $user_id = 0;
    function __construct()
    {
        parent::__construct();
        $this->table = 'albums';
    }

    function getAlbums($user, $page = 1, $limit = 50, $search = false){
        $offset = ($page - 1) * $limit;

        $this->db->select("a.id, a.name, a.author, i.src as cover, COUNT(DISTINCT ifc.id) as photos_count")
            ->from('albums a')
            ->join('photos i', "a.cover = i.id", 'left')
            ->join('photos ifc', "a.id = ifc.album", 'left')
            ->order_by('a.type, a.created', 'desc')
            ->where('a.author', $user)
            ->limit($limit, $offset)
            ->group_by('a.id');
        
        $query = $this->db->get();
        //dump($this->db->last_query());
        return $query->result();
    }

    function getAlbum($album_id){

        $this->db->select('a.*')
            ->from('albums a')
            ->where('a.id', $album_id);
        
        $query = $this->db->get();
        //dump($this->db->last_query());
        return $query->row();
    }

    function getUploadedAlbum($user_id){
        $this->db->set_dbprefix('soc_');
        $this->db->select('*')
            ->from('albums a')
            ->where('a.author = '.$user_id.' AND a.type = 0');

        $query = $this->db->get();
        //dump($this->db->last_query());
        $result = $query->row();

        if($result){
            return $result->id;
        }else{
            $this->save(array('name' => 'Загруженные', 'author' => $user_id, 'type' => 0, 'created' => time()), 'add');

            return $this->getUploadedAlbum($user_id);
        }
    }

    function getAlbumAuthor($id){
        $sql = "SELECT * 
                FROM ".$this->config['dbprefix']."albums
                WHERE id = $id";
        
        $query = $this->db->query($sql);
        $query->setFetchMode(PDO::FETCH_ASSOC);

        $result = $query->fetch();
        if($result){
            return $result['author'];
        }
        return false;
    }

    function checkAlbumAuthor($id, $user){

        $this->db->select('*')
            ->from('albums')
            ->where('id', $id);
        
        $query = $this->db->query($sql);
        $query->setFetchMode(PDO::FETCH_ASSOC);

        $result = $query->fetch();
        if($result){
            if($result['author'] == $user) return true;
        }
        return false;
    }

    function getPhotos($album, $page = 1, $limit = 50){
        $offset = ($page - 1) * $limit;

        $this->db->select('p.*');
        $this->db->from('photos p');
        $this->db->order_by('p.added', 'DESC');
        //$this->db->limit($limit, $offset);

        if($album){
            $this->db->where('p.album', $album);
        }
        
        $query = $this->db->get();
        //dump($this->db->last_query());
        $result = $query->result();
        return $result;
    }

    function getPhoto($photo_id){
        /*$sql = "SELECT p.*,
                    count(DISTINCT l.id) as likes_count,
                    count(DISTINCT liked.id) as is_liked,
                    count(DISTINCT disliked.id) as is_disliked,
                    count(DISTINCT dl.id) as dislikes_count,
                    count(DISTINCT c.id) as comments_count,
                    a.name as album_name, a.author as album_author
                FROM ".$this->config['dbprefix']."images p
                LEFT JOIN ".$this->config['dbprefix']."users u ON(p.author = u.id) 
                LEFT JOIN ".$this->config['dbprefix']."likes l ON(l.item = p.id AND l.type = 'photo' AND l.value = 0) 
                LEFT JOIN ".$this->config['dbprefix']."likes liked ON(liked.item = p.id AND liked.type = 'photo' AND liked.value = 0 AND liked.author = ".$this->userid.") 
                LEFT JOIN ".$this->config['dbprefix']."likes dl ON(dl.item = p.id AND dl.type = 'photo' AND dl.value = 1) 
                LEFT JOIN ".$this->config['dbprefix']."likes disliked ON(disliked.item = p.id AND disliked.type = 'photo' AND disliked.value = 1 AND disliked.author = ".$this->userid.") 
                LEFT JOIN ".$this->config['dbprefix']."comments c ON(c.item = p.id AND c.type = 'photo' AND c.deleted = 0) 
                LEFT JOIN ".$this->config['dbprefix']."albums a ON a.id = p.album
                WHERE p.id = $photo 
                ORDER BY p.added DESC 
                LIMIT 0,1";*/

        $select = "p.*,
                    count(DISTINCT l.id) as likes_count,
                    count(DISTINCT liked.id) as is_liked,
                    count(DISTINCT disliked.id) as is_disliked,
                    count(DISTINCT dl.id) as dislikes_count,
                    count(DISTINCT c.id) as comments_count,
                    a.name as album_name, a.author as album_author, u.avatar";

        $this->db->select($select)
            ->from('photos p')
            ->join('users u', 'p.author = u.id', 'left')
            ->join('likes l', "l.item = p.id AND l.type = 'photo' AND l.value = 0", 'left')
            ->join('likes liked', "liked.item = p.id AND liked.type = 'photo' AND liked.value = 0 AND liked.author = ".$this->user_id, 'left')
            ->join('likes dl', "dl.item = p.id AND dl.type = 'photo' AND dl.value = 1", 'left')
            ->join('likes disliked', "disliked.item = p.id AND disliked.type = 'photo' AND disliked.value = 1 AND disliked.author = ".$this->user_id, 'left')
            ->join('comments c', "c.item = p.id AND c.type = 'photo' AND c.deleted = 0", 'left')
            ->join('albums a', "a.id = p.album", 'left')
            ->order_by('p.added', 'desc')
            ->where('p.id', $photo_id)
            ->limit(1, 0);
        
        $query = $this->db->get();
        //dump($this->db->last_query());
        return $query->row();
    }
    function getPhotoComments($id, $count = 5, $offset = 0){
        if(empty($this->userid))
            $this->userid = 0;

        $sql = "SELECT c.*,u.fname,u.lname,u.last_action, u.avatar, u.id AS author,CONCAT(au.fname , ' ' , au.lname) as answer_name, 
                    count(DISTINCT l.id) as likes_count,
                    count(DISTINCT liked.id) as is_liked,
                    count(DISTINCT disliked.id) as is_disliked,
                    count(DISTINCT dl.id) as dislikes_count
                FROM ".$this->config['dbprefix']."comments c 
                LEFT JOIN ".$this->config['dbprefix']."users u ON(u.id = c.author) 
                LEFT JOIN ".$this->config['dbprefix']."comments ac ON(ac.id = c.answer) 
                LEFT JOIN ".$this->config['dbprefix']."users au ON(au.id = ac.author) 
                LEFT JOIN ".$this->config['dbprefix']."likes l ON(l.item = c.id AND l.type = 'comment' AND l.value = 0) 
                LEFT JOIN ".$this->config['dbprefix']."likes liked ON(liked.item = c.id AND liked.type = 'comment' AND liked.value = 0 AND liked.author = ".$this->userid.") 
                LEFT JOIN ".$this->config['dbprefix']."likes dl ON(dl.item = c.id AND dl.type = 'comment' AND dl.value = 1) 
                LEFT JOIN ".$this->config['dbprefix']."likes disliked ON(disliked.item = c.id AND disliked.type = 'comment' AND disliked.value = 1 AND disliked.author = ".$this->userid.") 
                WHERE c.item = $id AND c.type = 'photo' AND c.deleted = 0
                GROUP BY c.id
                HAVING c.id <> '' 
                ORDER BY c.date ASC";
        
        
        $query = $this->db->query($sql);
        $query->setFetchMode(PDO::FETCH_ASSOC);

        $result = $query->fetchAll();
        
        return $result;
    }

    function addPhoto($data){
        $this->setTable('photos');
        return $this->save($data, 'add');
    }

    function addAlbum($data){
        $id = $this->db->insert($this->config['dbprefix'].'albums', $data);
        if($id){
            return $id;
        }
        return false;
    }

    function updateAlbum($id, $data){
        if($this->db->update($this->config['dbprefix'].'albums', array('id' => $id), $data)){
            return true;
        }
        return false;
    }

}