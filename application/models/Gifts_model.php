<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/*
 *
 *  Ev_gift - таблица подарков
 *     - id
 *     - user_id (Создатель)
 *     - name (Название)
 *  ev_gifts_attribute - Параметры
 *     - id
 *     - name - Название параметра
 *     - gift_id Номер подарка
 *  Ev_gifts_values - таблица значений параметров подарков
 *     - id
 *     - gift_id - Номер подарка
 *     - attribute_id - номер параметра
 *     - name - Название значения
 *  ev_gifts_tickets - таблица соотвествия Билет = подарок
 *     - id
 *     - gift_id - номер подарка
 *     - ticket_id - Номер билета
 *     - name - Название?
 *     - optional - Обязательно?
 *
 */


class Gifts_model extends Default_model
{
    function __construct()
    {
        parent::__construct();
        $this->table = 'gift';

        $this->db->set_dbprefix('ev_');
    }

    function GetGiftsFromTicket($ticket_id){
        $this->db->select('gifts.name as gift_name, gifts.id as gift_id,
                        gift_attributes.name as attribute, gift_attributes.id as attribute_id,
                        gifts_values.name as values_name, gifts_values.id as values_id')
            ->from('gift gifts')
            ->join('gifts_tickets giftsTickets', 'giftsTickets.gift_id = gifts.id', 'left')
            ->join('gifts_attribute gift_attributes', 'gifts.id = gift_attributes.gift_id', 'left')
            ->join('gifts_values gifts_values', 'gift_attributes.id = gifts_values.attribute_id and  gifts.id = gifts_values.gift_id', 'left')
            ->where('giftsTickets.ticket_id = \''.$ticket_id.'\'');
        $query = $this->db->get();

        $data = $query->result();
        $variables = array();
        foreach ($data as $gift){
            $variables[$gift->gift_id]['name'] = $gift->gift_name;
            foreach ($data as $variable){
                if($gift->attribute_id===$variable->attribute_id) {
                    $variables[$gift->gift_id][$variable->attribute_id]['attribute'] = $variable->attribute;
                    foreach ($data as $values) {
                        if ($variable->values_id === $values->values_id) {
                            $variables[$gift->gift_id][$variable->attribute_id][$values->values_id]['value'] = $values->values_name;
                        }
                    }
                }
            }
        }

        return $variables;
    }

    function getGiftById($gift_id){
        $this->db->select('gifts.name as gift_name, gifts.id as gift_id,
                        gift_attributes.name as attribute, gift_attributes.id as attribute_id,
                        gifts_values.name as values_name, gifts_values.id as values_id')
            ->from('gift gifts')
            ->join('gifts_attribute gift_attributes', 'gifts.id = gift_attributes.gift_id', 'left')
            ->join('gifts_values gifts_values', 'gift_attributes.id = gifts_values.attribute_id and  gifts.id = gifts_values.gift_id', 'left')
            ->where('gifts.id = \''.$gift_id.'\'');
        $query = $this->db->get();

        $data = $query->result();
        $variables = array();
        foreach ($data as $gift){
            $variables[$gift->gift_id]['id'] = $gift->gift_id;
            $variables[$gift->gift_id]['name'] = $gift->gift_name;
            if(!isset($variables[$gift->gift_id]['attributes']))
                $variables[$gift->gift_id]['attributes'] = array();
            foreach ($data as $variable){
                if($gift->attribute_id===$variable->attribute_id) {
                    $variables[$gift->gift_id]['attributes'][$variable->attribute_id]['id'] = $variable->attribute_id;
                    $variables[$gift->gift_id]['attributes'][$variable->attribute_id]['name'] = $variable->attribute;
                    if(!isset($variables[$gift->gift_id]['attributes'][$variable->attribute_id]['values']))
                        $variables[$gift->gift_id]['attributes'][$variable->attribute_id]['values'] = array();
                    foreach ($data as $values) {
                        if ($variable->values_id === $values->values_id) {
                            $variables[$gift->gift_id]['attributes'][$variable->attribute_id]['values'][$values->values_id]['id'] = $values->values_id;
                            $variables[$gift->gift_id]['attributes'][$variable->attribute_id]['values'][$values->values_id]['value'] = $values->values_name;
                        }
                    }
                }
            }
        }

        $gift = array_shift($variables);

        return $gift;
    }

}