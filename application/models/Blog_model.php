<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Blog_model extends Default_model
{
    public $user_id = 0;
    public $limit = 20;
    public $page = 1;

    function __construct()
    {
        parent::__construct();
        $this->table = 'post';

        $this->db->set_dbprefix('blog_');
    }

    function save($aData, $subact = 'edit', $fp_nId = '')
    {
        if(empty($fp_nId)) $subact = 'add';
        if ($subact == 'add') {
            $res = $this->db->insert($this->table, $aData);
            $res = $this->db->insert_id();
        } else {
            if (!$fp_nId) return false;
            $res = $this->db->update($this->table, $aData, "id = '" . $fp_nId . "'");
        }
        if (!$res) {
            return false;
        } else {
            if ($subact == 'add') {
                return $res;
            } else {
                return $fp_nId;
            }
        }
    }

    function getCategories(){
        $this->db->set_dbprefix('blog_');
        $this->db->select('*')
            ->from('categories');
        $query = $this->db->get();
        return $query->result();
    }

    function getCategory($fp_nId){
        $this->db->set_dbprefix('blog_');
        if (!$fp_nId) return false;
        $query = $this->db->get_where('categories', array('id' => $fp_nId));
        $data = $query->row_array();
        //dump($this->db->last_query(),0);
        return $data;
    }

    function getDataByWhere($fp_aFilter, $select = 'p.*, count(c.id) as comments_count', $page = 1, $limit = 20)
    {
        $offset = ($page -1) * $limit;

        $this->db->set_dbprefix('blog_');
        $this->db->select($select);
        $this->db->from($this->table.' p');
        $this->db->join('comments c', 'c.post_id = p.id', 'left');
        $this->db->where($fp_aFilter)
        ->order_by('p.date', 'desc')
        ->group_by('p.id')
        ->limit($limit, $offset);

        //$offset = ($this->page - 1) * $this->limit;

        //dump($this->db->last_query(),0);
        $query = $this->db->get();
        //dump($this->db->last_query());
        return $query->result();
    }

    function getComments($id, $page = 1, $limit = 20){
        $offset = ($page -1) * $limit;

        $this->db->set_dbprefix('');
        $this->db->select('c.*, u.fname, u.lname, u.avatar');
        $this->db->from('blog_comments c')
        ->join('soc_users u', "u.id = c.author_id", 'left');
        $this->db->where('post_id', $id)
        ->order_by('date', 'desc')
        ->limit($limit, $offset);

        //$offset = ($this->page - 1) * $this->limit;

        //dump($this->db->last_query(),0);
        $query = $this->db->get();
        //dump($this->db->last_query());
        return $query->result();
    }

    function getDataById($fp_nId, $table = false)
    {
        $this->db->set_dbprefix('blog_');
        if (!$fp_nId) return false;
        if (!$table) {
            $table = $this->table;
        }
        $query = $this->db->get_where($table, array('id' => $fp_nId));
        $data = $query->row_array();
        //dump($this->db->last_query(),0);
        return $data;
    }

    function inc_views($id){
        $this->db->set_dbprefix('blog_');
        $this->db->set('views_count', 'views_count+1', FALSE);
        $this->db->where('id', $id);
        $this->db->update($this->table);
    }

    function getAllData($page = 1, $limit = 20)
    {
        $offset = ($page -1) * $limit;

        $this->db->select('p.*, count(DISTINCT c.id) as comments_count')
            ->from($this->table.' p')
            ->join('comments c', 'c.post_id = p.id', 'left')
            ->order_by('p.date', 'desc')
            ->group_by('p.id')
            ->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result();
    }

}