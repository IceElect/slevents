<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Ticket_model extends Default_model
{
    public $user_id = 0;
    public $limit = 20;
    public $page = 1;

    function __construct()
    {
        parent::__construct();
        $this->table = 'ticket';

        $this->db->set_dbprefix('ev_');
    }

    function getTicketsByUser($user_id){
        $this->db->set_dbprefix('ev_');
        $this->db->select('e.name as event_name, e.description, e.location, e.location_cord, e.date, e.wd,  t.price, t.name, ut.ticket_id, ut.id as user_ticket_id, ut.hash, ut.type, rh.status as payment_status')
            ->from('user_ticket ut')
            ->join('ticket t', 't.id = ut.ticket_id', 'left')
            ->join('events e', 't.event_id = e.id or ut.event_id = e.id', 'left')
            ->join('ev_robokassa_history rh', 'ut.payment_id = rh.id', 'left')
            ->where('ut.user_id = '.$user_id.' AND (ut.pay_status = 1 OR (ut.pay_status = 0 AND ut.date > '. (time() - (20*60)) .'))')
            ->order_by('ut.date', 'desc');

        $query = $this->db->get();
        //dump($query->result());
        return $query->result();
    }

    function getEvent($id){
        $this->db->select('e.*, m.svg, m.image as map_image, m.stands, m.places, COUNT(ut.id) as t_count')
            ->from('events e')
            ->join('user_ticket ut', 'ut.ticket_id = e.id', 'left')
            //->join('ev_robokassa_history rh', 'ut.payment_id = rh.id', 'left')
            ->join('maps m', 'm.id = e.map', 'left')
            ->group_by('e.id')
            ->where('e.id = '.$id);

        $query = $this->db->get();
        return $query->row();
    }

    function getEvents($status = 1){
        $this->db->select('e.*, COUNT(ut.id) as t_count')
            ->from('events e')
            ->join('ticket t', 't.event_id = e.id', 'left')
            ->join('user_ticket ut', 'ut.ticket_id = t.id AND (ut.pay_status = 1 OR (ut.pay_status = 0 AND ut.date > '. (time() - (20*60)) .'))', 'left')
            //->join('ev_robokassa_history rh', 'ut.payment_id = rh.id', 'left')
            ->group_by('e.id')
            ->order_by('e.date', 'desc');
        if($status)
            $this->db->where('e.status = '.$status);

        $query = $this->db->get();
        return $query->result();
    }

    function getTickets($e_id, $days_count = 1){
        $this->db->select('t.*, t.count as max_count, COUNT(ut.id) as t_count')
            ->from('ticket t')
            ->join('user_ticket ut', 'ut.ticket_id = t.id AND (ut.pay_status = 1 OR (ut.pay_status = 0 AND ut.date > '. (time() - (20*60)) .'))', 'left')
            ->group_by('t.id')
            ->order_by('t.price', 'asc')
            ->where('t.event_id = '.$e_id);

        $query = $this->db->get();
        $tickets = $query->result();

        if($days_count > 1){
            foreach ($tickets as $key => $item) {
                $days = explode(',', $item->days);
                $tickets[$key]->t_count = $item->t_count + $this->getTicketsCountByDays($e_id, $item->id, $days);
            }
        }

        return $tickets;
    }

    function getTicketsCountByDays($e_id, $t_id, $days){
        $this->db->select('user_ticket ut');
        $this->db->join('ticket t', 'ticket_id = t.id', 'left');
        $this->db->where('(ut.pay_status = 1 OR (ut.pay_status = 0 AND ut.date > '. (time() - (20*60)) .'))');
        $where_like = '';
        for($i=0;$i<=count($days);$i++){
            if($i > 0) $where_like .= ' OR ';
            $where_like .= " days LIKE '%{$i}%' ESCAPE '!' ";
        }
        $this->db->where('ut.event_id = '.$e_id);
        $this->db->where('(' . $where_like . ')');
        $this->db->where("t.id != {$t_id}");

        $count = $this->db->count_all_results('user_ticket ut');
        return $count;
    }

    function getTicketsByEvent($e_id, $pay = false, $type = 'visitor', $limit = 35, $offset = 0){
        $this->db->set_dbprefix('');
        $this->db->select('u.id as user_id, u.fname, u.lname, u.birthdate, e.date, e.name, t.name as ticket_type, t.price, e.location, ut.id, ut.hash, ut.type, ut.role, rh.status as payment_status')
            ->from('ev_user_ticket ut')
            ->join('soc_users u', 'ut.user_id = u.id', 'left')
            ->join('ev_ticket t', 'ut.ticket_id = t.id', 'left')
            ->join('ev_events e', 't.event_id = e.id', 'left')
            ->join('ev_robokassa_history rh', 'ut.payment_id = rh.id', 'left')
            ->where('ut.event_id = '.$e_id)
            ->where('ut.type = \''.$type.'\'')
            ->limit($limit, $offset);

        if($pay)
            $this->db->where('ut.pay_status', 1);

        $query = $this->db->get();
        // print_r($this->db->last_query());
        return $query->result();
    }

    function getTicket($id){
        $this->db->select('t.*, t.count, COUNT(ut.id) as t_count, e.days_count')
            ->from('ticket t')
            ->join('events e', 't.event_id = e.id', 'left')
            ->join('user_ticket ut', 'ut.ticket_id = t.id AND (ut.pay_status = 1 OR (ut.pay_status = 0 AND ut.date > '. (time() - (20*60)) .'))', 'left')
            ->join('ev_robokassa_history rh', 'ut.payment_id = rh.id', 'left')
            ->where('t.id = '.$id);

        $query = $this->db->get();
        return $query->row();
    }

    function getUserTicket($id){
        $this->db->select('t.*, t.count, COUNT(ut.id) as t_count, ut.payment_id')
            ->from('user_ticket ut')
            ->join('ticket t', 'ut.ticket_id = t.id', 'left')
            ->join('events e', 't.event_id = e.id', 'left')
            ->join('ev_robokassa_history rh', 'ut.payment_id = rh.id', 'left')
            ->where('ut.id = '.$id);

        $query = $this->db->get();
        return $query->row();
    }

    function getTicketByHash($hash, $pay = false){
        $this->db->set_dbprefix('');
        $this->db->select('u.fname, u.lname, u.birthdate, e.date, e.name, ut.role, ut.hash, t.name as ticket_type, t.price, e.location, ut.hash, ut.event_id, ut.pay_status as payment_status')
            ->from('ev_user_ticket ut')
            ->join('soc_users u', 'ut.user_id = u.id', 'left')
            ->join('ev_ticket t', 'ut.ticket_id = t.id', 'left')
            ->join('ev_events e', 'ut.event_id = e.id or t.event_id = e.id', 'left')
            ->join('ev_robokassa_history rh', 'ut.payment_id = rh.id', 'left')
            ->where('ut.hash = \''.$hash.'\'');

        if($pay)
            $this->db->where("ut.pay_status = 1");

        $query = $this->db->get();
        return $query->row();
    }

    function getUTickets($hash, $pay = false, $type = 'visitor', $limit = 35, $offset = 0){
        $this->db->set_dbprefix('');
        $this->db->select('u.id as user_id, u.fname, u.lname, u.birthdate, e.date, e.name, t.name as ticket_type, t.price, e.location, ut.id, ut.hash, ut.type, ut.role, rh.status as payment_status')
            ->from('ev_user_ticket ut')
            ->join('soc_users u', 'ut.user_id = u.id', 'left')
            ->join('ev_ticket t', 'ut.ticket_id = t.id', 'left')
            ->join('ev_events e', 't.event_id = e.id', 'left')
            ->join('ev_robokassa_history rh', 'ut.payment_id = rh.id', 'left')
            ->where('ut.type = \''.$type.'\'')
            ->limit($limit, $offset);

        if($pay)
            $this->db->where("ut.pay_status = 1");

        $query = $this->db->get();
        // dump($query->row());
        return $query->result();
    }

    function getTicketByOrder($order_id){
        $this->db->set_dbprefix('');
        $this->db->select('ut.id as t_id, ut.id as id, u.fname, u.lname, u.birthdate, u.email, e.date, e.name, t.name as ticket_type, t.price, e.location, ut.hash, rh.status as payment_status')
            ->from('ev_user_ticket ut')
            ->join('soc_users u', 'ut.user_id = u.id', 'left')
            ->join('ev_ticket t', 'ut.ticket_id = t.id', 'left')
            ->join('ev_events e', 't.event_id = e.id', 'left')
            ->join('ev_robokassa_history rh', 'ut.payment_id = rh.id', 'left')
            ->where('ut.payment_id = \''.$order_id.'\'');
            //->where('ut.date > '. (time() - 20*60));

        $query = $this->db->get();
        return $query->row();
    }

    function buy($id, $hash, $payment_id, $payed = false, $event_id = false, $type = 'visitor', $role = ''){
        $data = array(
            'ticket_id' => $id,
            'user_id' => $this->user_id,
            'date' => time(),
            'hash' => $hash,
            'type' => $type,
            'role' => $role,
            'payment_id' => $payment_id,
        );

        if($payed){
            $data['pay_status'] = 1;
        }

        if($event_id){
            $data['event_id'] = $event_id;
        }

        $result = $this->db->insert('user_ticket', $data);
        if($result){
            $result = $this->db->insert_id();
        }
        return $result;
    }

    function set_gift($ticket,$json_gift){
        $this->db->set('select_gift', $json_gift);
        $this->db->where("id",$ticket);
        $this->db->update('user_ticket');
    }
}