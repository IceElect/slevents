<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$pass = "Admin333";
$api_guid = "4A4BC3CEA73940B498E37ABED6BC4001";
$sign = hash("sha256", $pass . $api_guid . time());

function _call($url, $data){
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// curl_setopt($ch, CURLOPT_HEADER, array(
	// 	"Content-Type" => "text/json",
	// 	"Accept" => "text/json"
	// ));

	$response = curl_exec($ch);

	curl_close ($ch);

	return $response;
}

print_r(_call("https://api.digiseller.ru/api/apilogin", array('login' => 'IcedElect', 'timestamp' => time(), 'sign' => $sign)));