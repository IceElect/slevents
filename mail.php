<?php

$rest_json = file_get_contents("php://input");
$_POST = json_decode($rest_json, true);

$to = "ifearzu@gmail.com"; // емайл получателя данных из формы 
$tema = "Обратная связь"; // тема полученного емайла 
$message = "";
foreach($_POST['fields'] as $field){
	$message .= "<b>" . $field['name'] . ":</b> " . $field['value'];
}
$headers  = 'MIME-Version: 1.0' . "\r\n"; // заголовок соответствует формату плюс символ перевода строки
  $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n"; // указывает на тип посылаемого контента
mail($to, $tema, $message, $headers); //отправляет получателю на емайл значения переменных

echo json_encode(array('result' => 1, 'message' => $message));
?>